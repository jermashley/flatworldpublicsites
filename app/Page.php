<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Page extends Model
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function company() {
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }
}
