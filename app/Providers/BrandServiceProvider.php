<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class BrandServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('brand', 'App\Helpers\Brand');
    }
}
