<?php

namespace App\Console\Commands\User;

use App\Role;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class CreateCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create {first_name} {last_name} {email} {password}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a user.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            DB::beginTransaction();

            $first_name = $this->argument('first_name');
            $last_name = $this->argument('last_name');
            $email = $this->argument('email');
            $password = $this->argument('password');

            $user = User::create([
                'first_name' => $first_name,
                'last_name' => $last_name,
                'email' => $email,
                'password' => Hash::make($password),
            ]);

            // foreach ($this->argument('role') as $roleName) {
            // 	$role = Role::where('name', '=', $roleName)->firstOrFail();
            // 	$user->attachRole($role);
            // }

            $user->saveOrFail();
            $this->line('User created successfully.');
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            $this->error($e->getMessage());
        }
    }
}
