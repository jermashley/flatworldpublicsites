<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Role;
use App\Permission;
use App\Company;
use App\User;

class InstallRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'roles:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs roles.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $developer = new Role();
        $developer->name = Role::DEVELOPER;
        $developer->display_name = 'Developer';
        $developer->save();

        $superAdmin = new Role();
        $superAdmin->name = Role::SUPER_ADMIN;
        $superAdmin->display_name = 'Super Admin';
        $superAdmin->save();

        $guest = new Role();
        $guest->name = Role::GUEST;
        $guest->display_name = 'Guest';
        $guest->save();

        $admin = new Role();
        $admin->name = Role::ADMIN;
        $admin->display_name = 'Admin';
        $admin->save();

        $hr = new Role();
        $hr->name = Role::HR;
        $hr->display_name = 'Human Resources';
        $hr->save();

        $blogEditor = new Role();
        $blogEditor->name = Role::BLOGGER;
        $blogEditor->display_name = 'Blogger';
        $blogEditor->save();

        $pageAuthor = new Role();
        $pageAuthor->name = Role::WRITER;
        $pageAuthor->display_name = 'Writer';
        $pageAuthor->save();

        $users = User::all();

        foreach ($users as $user) {
            $user->attachRole($guest);
        }

        $user = User::where('email', 'jeremiah@prologuetechnology.com')->first();
        $user->attachRole($developer);
        $user->detachRole($guest);

        // User Manipulation
        $createUser = new Permission();
        $createUser->name = Permission::CREATE_USER;
        $createUser->display_name = 'Create User';
        $createUser->save();

        $updateUser = new Permission();
        $updateUser->name = Permission::UPDATE_USER;
        $updateUser->display_name = 'Update User Information';
        $updateUser->save();

        $deleteUser = new Permission();
        $deleteUser->name = Permission::DELETE_USER;
        $deleteUser->display_name = 'Delete User';
        $deleteUser->save();

        $restoreDeletedUser = new Permission();
        $restoreDeletedUser->name = Permission::RESTORE_DELETED_USER;
        $restoreDeletedUser->display_name = 'Restore Deleted User';
        $restoreDeletedUser->save();

        $updateUserCompany = new Permission();
        $updateUserCompany->name = Permission::UPDATE_USER_COMPANY;
        $updateUserCompany->display_name = 'Update User Company';
        $updateUserCompany->save();

        $updateUserRole = new Permission();
        $updateUserRole->name = Permission::UPDATE_USER_ROLE;
        $updateUserRole->display_name = 'Update User Role';
        $updateUserRole->save();

        $developer->attachPermission([
            $createUser,
            $updateUser,
            $deleteUser,
            $restoreDeletedUser,
            $updateUserCompany,
            $updateUserRole,
        ]);

        $superAdmin->attachPermission([
            $createUser,
            $updateUser,
            $deleteUser,
            $updateUserCompany,
            $updateUserRole,
        ]);

        $admin->attachPermission([
            $createUser,
            $updateUser,
        ]);
    }
}
