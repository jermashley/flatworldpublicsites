<?php

namespace App\Traits\Filters;

use App;
use Illuminate\Support\Facades\Auth;
use App\Scopes\FilterByCompanyScope;
use App\Company;
use Entrust;

/**
 * Trait FilterCompaniesTrait
 *
 * @mixin \Eloquent
 */
trait FilterByCompanyTrait
{
    /**
     * Chained boot method to add our filter to our scope for session requests
     */
    public static function bootFilterByCompanyTrait()
    {
        // Bypass for console
        if (App::runningInConsole()) {
            return;
        }
        // Bypass for devs and super admins
        if (Auth::check() && Auth::user()->hasRole(['developer', 'super.admin'])) {
            return;
        }

        if (! Auth::check()) {
            return;
        }

        $companyId = Auth::check() && Entrust::hasRole(['admin', 'blogger', 'writer', 'hr']) ? Auth::user()->company_id : Company::where('id', config('app.name'))->firstOrFail()->id;

        // Add our filter scope with our user company id
        static::addGlobalScope(new FilterByCompanyScope($companyId));
    }

    /**
     * Getter
     *
     * @return string
     */
    public function getCompanyIdColumn()
    {
        return 'company_id';
    }

    /**
     * Get the fully qualified column name
     *
     * @return string
     */
    public function getQualifiedCompanyIdColumn()
    {
        return $this->getTable().'.'.$this->getCompanyIdColumn();
    }
}