<?php

namespace App\Helpers;

/**
 * Class Brand
 *
 * @package App\Helpers
 */
class Brand
{
    /**
     * @var string
     */
    public static $viewPrefix = '.';

    /**
     * @param string $viewFile
     * @param array $data
     * @param null|string $override
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public static function view($viewFile, $data = [], $override = null)
    {

        $prefix = $override ?? config('prefix.name');

        return view($prefix . static::$viewPrefix . $viewFile, $data);
    }

    /**
     * @param string $viewName
     * @param null|string $override
     *
     * @return string
     */
    public static function partial($viewName, $override = null)
    {

        $prefix = $override ?? config('prefix.name');

        return $prefix . static::$viewPrefix . $viewName;
    }

    /**
     * @param string $name
     * @param null|string $override
     *
     * @return string
     */
    public static function asset($name, $override = null)
    {
        $prefix = $override ?? config('prefix.name');

        return asset(sprintf($name, $prefix));
    }

    /**
     * @param null|string $override
     *
     * @return \Illuminate\Config\Repository|mixed|null
     */
    public static function name($override = null)
    {
        $prefix = $override ?? config('app.name');

        return $prefix;
    }

    /**
     * @param null|string $override
     *
     * @return \Illuminate\Config\Repository|mixed|null
     */
    public static function prefix($override = null)
    {
        $prefix = $override ?? config('app.prefix');

        return $prefix;
    }
}
