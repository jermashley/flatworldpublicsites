<?php

namespace App;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    const DEVELOPER = 'developer';
    const SUPER_ADMIN = 'super.admin';
    const ADMIN = 'admin';
    const HR = 'hr';
    const BLOGGER = 'blogger';
    const WRITER = 'writer';
    const GUEST = 'guest';
}