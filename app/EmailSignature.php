<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Filters\FilterByCompanyTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmailSignature extends Model
{
    use SoftDeletes;
    use FilterByCompanyTrait;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */

    protected $dates = ['deleted_at'];

    public function company() {
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
