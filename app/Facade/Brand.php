<?php

namespace App\Facade;

use Illuminate\Support\Facades\Facade;

class Brand extends Facade
{
    /**
     * Get the binding in the IoC container
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'brand'; // the IoC binding.
    }
}
