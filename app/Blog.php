<?php

namespace App;

use App\Traits\Filters\FilterByCompanyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Blog extends Model
{

    use SoftDeletes;
    use FilterByCompanyTrait;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */

    protected $dates = ['deleted_at'];

    public function company() {
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }

    public function user() {
        return $this->belongsTo('App\User', 'created_by', 'id');
    }

    public function getRouteKeyName() {
        return 'slug';
    }

}
