<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Filters\FilterByCompanyTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContactForm extends Model
{
    use SoftDeletes;
    use FilterByCompanyTrait;

    protected $fillable = [
        'first_name',
        'last_name',
        'business',
        'type_of_business',
        'address_1',
        'address_2',
        'city',
        'state',
        'zip',
        'email',
        'telephone',
        'extension',
        'message',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }
}
