<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ReCaptchaV3 implements Rule
{

    protected $clientIp;
    protected $response;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($clientIp)
    {
        $this->clientIp = $clientIp;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $secret = config('app.recaptcha_secret');
        $recaptcha = new \ReCaptcha\ReCaptcha($secret);

        $this->response = $recaptcha->verify($value, $this->clientIp);

        return $this->response->isSuccess();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errors = $this->response->getErrorCodes();
        $message = 'We were unable to verify that you\'re not a robot. Please try again. (' . reset($errors) . ')';
        return $message;
    }
}
