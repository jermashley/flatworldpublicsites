<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManager;
use Validator;
use Entrust;
use App\Blog;
use App\Company;
use Storage;
use Image;
use Brand;
use Session;

class BlogAdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:developer|super.admin|blogger');
    }

    public function index()
    {
        // Check if developer or super admin
        if (Auth::check() && Entrust::hasRole(['developer', 'super.admin'])) {
            $blogs = Blog::withTrashed()->orderBy('created_at', 'desc')->get();
        }
        // check if Blog user
        elseif (Auth::check() && Entrust::hasRole('blogger')) {
            $blogs = Blog::withTrashed()->with(['company', 'user'])->orderBy('created_at', 'desc')->get();
        }

        // Check if normie user
        else {
            return redirect(route('blog.index'));
        }

        return view('admin.blog.index', [
            'blogs' => $blogs,
        ]);
    }

    public function show($id)
    {
        $blog = Blog::withTrashed()->find($id);

        return response()->json([
            'blog' => $blog
        ]);
    }

    public function create()
    {
        $companies = Company::get();

        return view('admin.blog.create', [
            'companies' => $companies,
        ]);
    }

    public function store(Request $request)
    {
        $blog = new Blog();

        if (Entrust::hasRole('developer', 'super.admin')) {
            $blog->company_id = $request->company_id;
        } else {
            $blog->company_id = Company::where('id', config('app.name'))->firstOrFail()->id;
        }

        $blog->published = $request->published;

        $blog->title = $request->title;
        $blog->slug = $request->slug;
        $blog->summary = $request->summary;
        $blog->content = $request->content;
        $blog->featured_image = $request->featured_image;
        $blog->created_by = $request->created_by;

        $blog->save();

        return response()->json([
            'redirect' => route('admin.blog.edit', ['blog' => $blog->id]),
            'message' => 'Saved successfully.'
        ], 200);
    }

    public function edit($slug)
    {
        if (Entrust::hasRole(['developer', 'super.admin'])) {
            $blog = Blog::withTrashed()->where('slug', $slug)->first();
        } elseif (Entrust::hasRole(('blogger'))) {
            $blog = Blog::withTrashed()->where('slug', $slug)->first();
        } else {
            $blog = Blog::where('slug', $slug)->first();
        }

        $companies = Company::get();

        return view('admin.blog.edit', [
            'blog' => $blog,
            'companies' => $companies,
        ]);
    }

    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);

        if (Entrust::hasRole('developer', 'super.admin')) {
            $blog->company_id = $request->company_id;
        } else {
            $blog->company_id = Company::where('id', config('app.name'))->firstOrFail()->id;
        }

        $blog->published = $request->published;

        $blog->title = $request->title;
        $blog->summary = $request->summary;
        $blog->content = $request->content;
        $blog->featured_image = $request->featured_image;
        $blog->created_by = $request->created_by;

        $blog->save();

        return response()->json([
            'redirect' => route('admin.blog.edit', ['blog' => $blog->id]),
            'message' => 'Saved successfully.'
        ], 200);
    }

    public function destroy($id)
    {
        $blog = Blog::find($id)->delete();

        return response()->json([
            'redirect' => route('admin.blog.index'),
        ]);
    }

    public function restore($id)
    {
        $blog = Blog::withTrashed()->find($id)->restore();

        return response()->json([
            'redirect' => route('admin.blog.index'),
        ]);
    }
}
