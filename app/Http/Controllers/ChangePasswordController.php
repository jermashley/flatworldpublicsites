<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Hash;
use Auth;
use Session;
use App\User;

class ChangePasswordController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('auth.passwords.changePassword');
    }

    public function changePassword(Request $request) {

        if (! Hash::check($request->current_password, Auth::user()->password)) {
            Session::flash('error', 'The current password does not match our records');
            return redirect(route('changePassword.index'));
        }

        $validator = $request->validate([
            'current_password' => 'required',
            'new_password' => 'required|confirmed|min:8',
        ]);

        $user = User::where('id', Auth::user()->id)
        ->first();

        $user->password = bcrypt($request->new_password);

        $user->save();

        Session::flash('success', 'Your password was successfully changed.');

        return redirect(route('userProfile.index'));

    }

    public function messages() {
        return [
            'password.required' => 'Don\'t forget to input your current password!',
            'new_password.required' => 'You came here to change your password, make sure you put a new one in!',
            'new_password.confirmed' => 'Your passwords didn\'t match. Give it another shot.',
            'new_password.min' => 'Your password needs to be at least 8 characters long.'
        ];
    }
}
