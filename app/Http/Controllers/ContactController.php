<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\StateHelper;
use App\Mail\GeneralContact;
use App\Mail\Air2PostContact;
use App\Mail\AutoResponder;
use App\ContactForm;
use App\Company;
use Mail;
use Session;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $states = ['Choose One' => ''];

        $states = array_merge($states, StateHelper::getStates());

        return view(config('app.prefix') . '.contact.index', [
            'states' => $states,
            'selectedState' => old('state', ''),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $url = 'https://www.google.com/recaptcha/api/siteverify';

        $data = [
            'secret' => config('app.recaptcha_secret'),
            'response' => $request->recaptcha,
        ];

        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data),
            ]
        ];

        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);

        if ($resultJson->success !== true && $resultJson->score >= 0.5) {
            Session::flash('warning', 'There was an error with reCaptcha. Please try again.');
            return redirect()->back()->withInput();
        }

        $companyId = Company::where('shortname', config('app.prefix'))
            ->first();

        if ($request->blood_type) {
            $contactForm = new ContactForm();

            $contactForm->company_id = $companyId->id;

            $contactForm->first_name = 'Faker';
            $contactForm->last_name = 'McFakeface';
            $contactForm->ip_address = $request->ip();
            $contactForm->attempted_honeypot = 1;

            $contactForm->save();

            Session::flash('success', ' Thank you for contacting us!  We will get back to you as soon as possible.');
            return redirect(route('contact.index'));
        } else {
            $typeOfBusinessRequired = '';

            if (config('app.prefix') == 'ram-intl') {
                $typeOfBusinessRequired = 'required';
            } else {
                $typeOfBusinessRequired = '';
            }

            $validatedData = $request->validate([
                'first_name' => 'required',
                'last_name' => 'required',
                'business' => 'required',
                'typeOfBusiness' => $typeOfBusinessRequired,
                'address_1' => 'required',
                'address_2' => 'required',
                'city' => 'required',
                'state' => 'required',
                'zip' => 'required',
                'email' => 'required',
                'telephone' => 'required',
                'extension' => '',
                'message' => 'required',
            ]);

            $contactForm = new ContactForm();

            $contactForm->fill($request->all());

            $contactForm->company_id = $companyId->id;
            $contactForm->ip_address = $request->ip();
            $contactForm->attempted_honeypot = 0;

            $contactForm->save();

            Mail::send(new GeneralContact((object) $request->all()));
            Mail::send(new AutoResponder((object) $request->all()));

            Session::flash('success', 'Thank you for contacting us!  We will get back to you as soon as possible.');

            return redirect(route('contact.index'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function a2PStoreContact(Request $request)
    {
        $secret = config('app.recaptcha_secret');
        $recaptcha = new \ReCaptcha\ReCaptcha($secret);

        $contactForm = new ContactForm();
        $companyId = Company::where('shortname', config('app.prefix'))
            ->first();

        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'business' => 'required',
            'email' => 'required',
            'g-recaptcha-response' => new ReCaptchaV3($request->ip()),
        ]);

        $contactForm->company_id = $companyId->id;

        $contactForm->first_name = $request->first_name;
        $contactForm->last_name = $request->last_name;
        $contactForm->business = $request->business;
        $contactForm->email = $request->email;

        $contactForm->product = 'Air2Post';

        $contactForm->save();

        Mail::send(new Air2PostContact((object) $request->all()));

        /**
         * Todo Make auto responder after message is submitted.
         */
        // Mail::send(new AutoResponder((object)$request->all()));

        Session::flash('success', 'Thank you for contacting us!  We will get back to you as soon as possible.');

        return redirect(route(config('app.prefix') . '.solutions.air2post'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
