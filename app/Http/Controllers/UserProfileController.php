<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\User;
use Validator;
use Entrust;
use Brand;
use Session;

class UserProfileController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        $user = User::find(Auth::user()->id);
        $roles = Auth::user()->roles;

        return view('userProfile.index', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function edit() {
        $user = User::find(Auth::user()->id);
        $roles = Auth::user()->roles;

        return view('userProfile.edit', [
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function update(Request $request) {
        $user = User::find(Auth::user()->id);

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;

        $user->save();

        Session::flash('success', 'User profile updated successfully!');

        return redirect(route('userProfile.index'));
    }
}
