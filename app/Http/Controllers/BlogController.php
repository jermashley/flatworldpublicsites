<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManager;
use Validator;
use Entrust;
use App\Blog;
use App\Company;
use Storage;
use Image;
use Brand;
use Session;

class BlogController extends Controller
{
    public function index()
    {
        // Start blog query builder
        $BLOG_QUERY = Blog::with('company');

        $blogs = $BLOG_QUERY
            ->orderBy('created_at', 'desc')
            ->where('published', 1)
            ->paginate(9);

        return view('blog.index', [
            'blogs' => $blogs,
        ]);
    }

    public function show(Blog $blog)
    {
        return view('blog.show')->withBlog($blog);
    }
}
