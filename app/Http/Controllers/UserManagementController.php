<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use Session;
use Entrust;
use App\User;
use App\Role;
use App\Company;

class UserManagementController extends Controller
{
    use EntrustUserTrait;

    public function __construct() {
        $this->middleware('auth');
        $this->middleware('role:developer|super.admin|admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('company')
        ->get();

        return view('userManagement.index', [
            'users' => $users
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::all();
        $roles = Role::all();

        return view('userManagement.create', [
            'companies' => $companies,
            'roles' => $roles,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $adminCompany = Auth::user()->company->id;
        $role = Role::find($request->role_id);
        $user = new User();

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = Hash::make(uniqid());

        if (Auth::user()->hasRole(['developer', 'super.admin'])) {
            $user->company_id = $request->company_id;
        } elseif (Auth::user()->hasRole('admin')) {
            $user->company_id = $adminCompany;
        }

        if (Auth::user()->hasRole('super.admin') && $request->role_id <= 1) {
            return redirect()->back()->with('error', 'Sorry, you don\'t have permission to set that role.');
        }

        if (Auth::user()->hasRole('admin') && $request->role_id <= 2) {
            return redirect()->back()->with('error', 'Sorry, you don\'t have permission to set that role.');
        }

        $user->save();


        $user->attachRole($role);

        Password::broker()->sendResetLink(['email' => $user->email]);

        Session::flash('success', 'Awesome! You\'ve just created a new user. We\'ve let <strong>' . $user->first_name . '</strong> know that they can set their password to login.');

        return redirect(route('user.index'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::where('id', $id)
        ->first();

        if ($user->id == Auth::user()->id) {
            Session::flash('error', 'I\'m sorry, you can\'t change your own role or company.');

            return redirect(route('userProfile.index'));
        }

        $companies = Company::all();
        $roles = Role::all();

        return view('userManagement.edit', [
            'user' => $user,
            'companies' => $companies,
            'roles' => $roles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::where('id', $id)
        ->first();

        $role = Role::find($request->role_id);

        if ($user->id == Auth::user()->id) {
            Session::flash('error', 'I\'m sorry, you can\'t change your own role or company.');

            return redirect(route('userProfile.index'));
        }

        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;

        // if (Auth::user()->hasRole('developer') && $user->hasRole('developer')) {
        //     Session::flash('error', 'Devs don\'t mess with other Devs.');

        //     return redirect()->back();
        // }

        if (Auth::user()->hasRole('super.admin') && $request->role_id <= 1) {
            Session::flash('error', 'Sorry, you don\'t have permission to set that role.');

            return redirect()->back();
        }

        if (Auth::user()->hasRole('admin') && $request->role_id <= 2) {
            Session::flash('error', 'Sorry, you don\'t have permission to set that user role.');

            return redirect()->back();
        }

        if (Auth::user()->hasRole('admin') && $request->company_ids != NULL) {
            Session::flash('error', 'Sorry, you don\'t have permission to change the user\'s company.');

            return redirect()->back();
        }

        if (Auth::user()->hasRole(['developer', 'super.admin'])) {
            $user->company_id = $request->company_id;
        }

        $user->save();

        $user->detachRole($user->roles()->first()->id);
        $user->attachRole($role);

        Session::flash('success', 'Done! We\'ve updated <strong>' . $user->first_name . '\'s</strong> information.');

        return redirect(route('user.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
