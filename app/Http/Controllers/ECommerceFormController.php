<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EcommerceForm as FormStore;
use App\Mail\eCommerceForm;
use App\Mail\EcommerceResponderForm;
use Mail;
use Session;

class ECommerceFormController extends Controller
{
    public function sendEmail(Request $request)
    {
        $request->flash();
        $request->isEmail = true;

        $form = new FormStore();
        $form->first_name = $request->firstName;
        $form->last_name = $request->lastName;
        $form->email = $request->email;
        $form->form_object = json_encode($request->all());
        $form->saveOrFail();

        // Send atuoresponder email to submitted email from form
        Mail::to($request->email)->send(new EcommerceResponderForm((object) $request->all()));

        // Send email with form contents to app contacts from env APP_CONTACTS
        Mail::to(config('prefix.contacts'))->send(new eCommerceForm((object) $request->all()));

        Session::flash('success', 'Thank you for contacting us!  We will get back to you as soon as possible.');

        return redirect(route(config('app.prefix') . '.ecommerce'));
    }
}
