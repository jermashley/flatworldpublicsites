<?php

namespace App\Http\Controllers;

use App\Company;
use Entrust;

class CompanyController extends Controller
{
  public function index() {

    if (Entrust::hasRole('developer', 'super.admin')) {
      $companies = Company::all();
    } else {
      $companies = Company::where('name', config('app.name'))->firstOrFail();
    }

    return response()->json([
      'companies' => $companies
    ]);
  }
}