<?php

namespace App\Http\Controllers;

use App\Mail\AutoResponder;
use App\Mail\GeneralContact;
use Illuminate\Http\Request;
use Mail;
use Session;

class GeneralContactController extends Controller
{
    public function sendEmail(Request $request)
    {
        $typeOfBusinessRequired = '';

        if (config('app.prefix') == 'ram-intl')  {
            $typeOfBusinessRequired = 'required';
        }

        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'business' => 'required',
            'typeOfBusiness' => $typeOfBusinessRequired,
            'address'=> 'required',
            'city' => 'required',
            'state' => 'required',
            'zip' => 'required',
            'email' => 'required',
            'telephone' => 'required',
            'extension' => '',
            'message' => 'required'
        ]);

        Mail::send(new GeneralContact((object)$request->all()));

        Mail::send(new AutoResponder((object)$request->all()));

        Session::flash('success', 'Thank you for contacting us!  We will get back to you as soon as possible.');

        return redirect(route('contact'));

    }
}
