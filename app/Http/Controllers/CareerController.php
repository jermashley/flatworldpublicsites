<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Career;
use App\Company;
use Session;

class CareerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);

        $this->middleware('role:developer|super.admin')->only('restore');

        $this->middleware('role:developer|super.admin|hr')->only(['create', 'store', 'edit', 'update', 'delete']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $careers = Career::with('company')
                ->orderBy('created_at', 'desc')
                ->get();
        } else {
            $careers = Career::with('company')
                ->where('published', 1)
                ->where('company_id', 1)
                ->orderBy('created_at', 'desc')
                ->get();
        }

        return view('careers.index', [
            'careers' => $careers,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = Company::get();

        return view('careers.create', [
            'companies' => $companies,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $career = new Career;

        $career->title = $request->title;
        $career->short_description = $request->short_description;
        $career->long_description = $request->long_description;
        $career->company_id = $request->company_id;
        $career->published = $request->published;

        $career->save();

        Session::flash('success', 'Post confirmed, Captain! Should be the newest card on the deck.');

        return redirect(route('careers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Career $career)
    {
        return view('careers.show', [
            'career' => $career,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check()) {
            $career = Career::find($id);

            return view('careers.edit', [
                'career' => $career,
            ]);
        } else {
            return view('auth.fail');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $career = Career::find($id);

        $career->title = $request->title;
        $career->short_description = $request->short_description;
        $career->long_description = $request->long_description;
        $career->company_id = $request->company_id;
        $career->published = $request->published;

        $career->save();

        Session::flash('success', 'Cool!  We all make mistakes.  Glad we could give you an edit button for this one.');

        return redirect(route('careers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        if (Auth::check()) {
            $career = Career::find($id);

            $career->delete();

            Session::flash('error', 'No worries.  We all make mistakes.  Glad we could give you a way to blot this one from the record.');

            return redirect(route('careers.index'));
        } else {
            return view('auth.fail');
        }
    }
}
