<?php

namespace App\Http\Controllers;

use App\EmailSignature;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Helpers\StateHelper;
use App\User;
use App\Company;
use Validator;
use Entrust;
use Session;
use View;

class EmailSignatureController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(Auth::user()->id);

        $signatures = EmailSignature::where('user_id', Auth::user()->id)
        ->get();

        return view('userProfile.emailSignatures.index', [
            'user' => $user,
            'signatures' => $signatures,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::find(Auth::user()->id);
        $companies = Company::all();
        $states = ['Choose One' => ''];
        $states = array_merge($states, StateHelper::getStates());

        return view('userProfile.emailSignatures.create', [
            'user' => $user,
            'companies' => $companies,
            'states' => $states,
            'selectedState' => old('state', ''),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $signature = new EmailSignature();
        $user_id = Auth::user()->id;
        $user_company_id = Auth::user()->company->id;

        $signature->user_id = $user_id;

        // If user is a Super Admin, allow setting company_id and custom address, otherwise, automatically set logged in user's company.
        if (Auth::user()->hasRole('super.admin')) {
            $signature->company_id = $request->company_id;
            $signature->address_1 = $request->address_1;
            $signature->address_2 = $request->address_2;
            $signature->city = $request->city;
            $signature->state = $request->state;
            $signature->zip = $request->zip;
        } else {
            $signature->company_id = $user_company_id;
        }

        if (isset($request->has_phone)) {
            $signature->has_phone = 1;
        }
        if (isset($request->has_fax)) {
            $signature->has_fax = 1;
        }
        if (isset($request->has_cell)) {
            $signature->has_cell = 1;
        }
        if (isset($request->has_email)) {
            $signature->has_email = 1;
        }
        $signature->phone = $request->phone;
        $signature->fax = $request->fax;
        $signature->cell = $request->cell;
        $signature->email = $request->email;

        $signature->save();

        Session::flash('success', 'Awesome! We saved that signature and it is ready for you to download.');

        return redirect(route('signatures.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EmailSignature  $emailSignature
     * @return \Illuminate\Http\Response
     */
    public function show(EmailSignature $signature)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EmailSignature  $emailSignature
     * @return \Illuminate\Http\Response
     */
    public function edit(EmailSignature $signature)
    {
        $signature = EmailSignature::where('id', $signature->id)
        ->firstOrFail();
        $user_id = Auth::user()->id;
        $user_company_id = Auth::user()->company->id;

        return view('userProfile.emailSignatures.edit', [
            'signature' => $signature,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EmailSignature  $emailSignature
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, EmailSignature $signature)
    {
        $signature = EmailSignature::where('id', $signature->id)
        ->firstOrFail();
        $user_id = Auth::user()->id;
        $user_company_id = Auth::user()->company->id;

        $signature->user_id = $user_id;

        // If user is a Super Admin, allow setting company_id and custom address, otherwise, automatically set logged in user's company.
        if (Auth::user()->hasRole('super.admin')) {
            $signature->company_id = $request->company_id;
            $signature->address_1 = $request->address_1;
            $signature->address_2 = $request->address_2;
            $signature->city = $request->city;
            $signature->state = $request->state;
            $signature->zip = $request->zip;
        } else {
            $signature->company_id = $user_company_id;
        }

        if (isset($request->has_phone)) {
            $signature->has_phone = 1;
        }
        if (isset($request->has_fax)) {
            $signature->has_fax = 1;
        }
        if (isset($request->has_cell)) {
            $signature->has_cell = 1;
        }
        if (isset($request->has_email)) {
            $signature->has_email = 1;
        }
        $signature->phone = $request->phone;
        $signature->fax = $request->fax;
        $signature->cell = $request->cell;
        $signature->email = $request->email;

        $signature->save();

        Session::flash('success', 'We\'ve updated that signature! Now download it and use it!');

        return redirect(route('signatures.index'));
    }

    public function renderHtml($id)
    {
        $signature = EmailSignature::where('id', $id)
            ->firstOrFail();

        // $view = View::make('userProfile.emailSignatures.signature', [
        //     'signature' => $signature,
        // ]);

        // $htmlString = $view->render();

        // return response()->downloadString($htmlString, 'text/html','signature.html');

        return view('userProfile.emailSignatures.signature', [
            'signature' => $signature,
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EmailSignature  $emailSignature
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $signature = EmailSignature::where('id', $id)
        ->firstOrFail();

        $signature->delete();

        Session::flash('success', 'We deleted that signature. Hope you didn\'t need it.');

        return redirect(route('signatures.index'));
    }
}
