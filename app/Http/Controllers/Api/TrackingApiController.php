<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;
use GuzzleHttp\RequestOptions;

class TrackingApiController extends Controller
{
    /**
     * Get tracking info from Pipeline.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTracking($trackingNumber)
    {
        // Save endpoint in var.
        $endpoint = 'https://kevin.pipeline.flatworldsc.com/api/v1/shipmentSearch';
        $client = new Client();

        // Create HTTP client.
        $response = $client->request('POST', $endpoint, [
            'headers' => [
                'Content-Type' => 'application/json',
                'apiKey' => config('keys.pipelineApi'),
            ],
            RequestOptions::JSON => [
                'bolNumber' => $trackingNumber,
                'globalSearch' => true
            ]
        ]);

        return response()->json([
            'response' => $response->getBody()->getContents()
        ]);
    }
}
