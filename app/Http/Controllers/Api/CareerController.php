<?php

namespace App\Http\Controllers\Api;

use App\Career;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Validator;
use Auth;

class CareerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:developer|super.admin')->only('restore');
        $this->middleware('role:developer|super.admin|hr')->only(['create', 'store', 'edit', 'update', 'delete']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $careers = Career::with('company')
            ->orderBy('created_at', 'desc')
            ->get();

        return response()->json([
            'careers' => $careers,
        ])->setStatusCode(200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'published' => 'required',
            'short_description' => 'required|max:255',
            'long_description' => 'required',
        ];

        $messages = [
            'title.required' => 'A title is required',
            'published.required' => 'Please set the published state',
            'short_description.required' => 'A description is required.',
            'short_description.max' => 'Please enter a shorter description',
            'long_description.required' => 'A long description is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'errorBag' => $validator->errors(),
                'oldInput' => $request->all(),
            ]);
        }

        try {
            DB::beginTransaction();
            $career = new Career;
            $career->fill($request->all());
            // Todo: make company_id dynamic.
            $career->company_id = 1;
            $career->created_by = Auth::user()->id;
            $career->saveOrFail();
            DB::commit();
        } catch (\Throwable $th) {
            return $th;
            DB::rollback();
        }

        return response()->json([
            'career' => $career ?? null,
        ])->setStatusCode(200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function show(Career $career)
    {
        return response()->json([
            'career' => $career,
        ])->setStatusCode(200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function edit(Career $career)
    {
        return response(json_encode([
            'career' => $career
        ]), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Career $career)
    {
        $rules = [
            'title' => 'required',
            'published' => 'required',
            'short_description' => 'required|max:255',
            'long_description' => 'required',
        ];

        $messages = [
            'title.required' => 'A title is required',
            'published.required' => 'Please set the published state',
            'short_description.required' => 'A description is required.',
            'short_description.max' => 'Please enter a shorter description',
            'long_description.required' => 'A long description is required.',
        ];


        $validator = Validator::make($request->all(), $rules, $messages);

        if ($validator->fails()) {
            return response()->json([
                'errorBag' => $validator->errors(),
            ]);
        }

        try {
            DB::beginTransaction();
            $career = Career::where('id', $request->id)->firstOrFail();
            $career->fill($request->all());
            $career->updated_by = Auth::user()->id;
            $career->save();
            DB::commit();
        } catch (\Throwable $th) {
            throw $th;
            DB::rollback();
        }

        return response(json_encode([
            'career' => $career,
            'message' => 'Successfully updated the career listing.'
        ]), 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Career  $career
     * @return \Illuminate\Http\Response
     */
    public function destroy(Career $career)
    {
        try {
            DB::beginTransaction();
            $career->delete();
            $career->save();
            DB::commit();
            $message = 'Successfully deleted the career listing.';
        } catch (\Throwable $th) {
            throw $th;
            DB::rollback();
        }

        return redirect()->back();
    }
}
