<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManager;
use Validator;
use Entrust;
use App\Blog;
use App\Company;
use Storage;
use Image;
use Brand;
use Session;

class BlogApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware('role:developer|super.admin|blogger');
    }

    public function index()
    {
        // Var for setting admin UI
        $blogPermission = false;

        // Start blog query builder
        $BLOG_QUERY = Blog::with(['company', 'user']);

        // Check if developer or super admin
        if (Auth::check() && Entrust::hasRole(['developer', 'super.admin'])) {
            $blogPermission = true;
            $blogs = $BLOG_QUERY->withTrashed()->orderBy('created_at', 'desc')->get();
        } elseif (Auth::check() && Entrust::hasRole('blogger')) {
            $blogPermission = true;
            $blogs = $BLOG_QUERY->withTrashed()->orderBy('created_at', 'desc')->get();
        } else {
            $blogs = $BLOG_QUERY->orderBy('created_at', 'desc')->paginate(9);
        }

        return view('admin.blog.index', [
            'blogs' => $blogs,
            'blogPermission' => $blogPermission,
        ]);
    }

    public function show($id)
    {
        $blog = Blog::withTrashed()->find($id);

        return response()->json([
            'blog' => $blog
        ]);
    }

    // public function create() {
    //     $companies = Company::get();

    //     return view('admin.blog.create', [
    //         'companies' => $companies,
    //     ]);
    // }

    public function store(Request $request)
    {
        $blog = new Blog();

        if (Entrust::hasRole('developer', 'super.admin')) {
            $blog->company_id = $request->company_id;
        } else {
            $blog->company_id = Company::where('shortname', config('app.prefix'))->firstOrFail()->id;
        }

        if ($request->published == 1) {
            $responseText = 'Post created and published and successfully.';
        } elseif ($request->published == 0) {
            $responseText = 'Post created and saved as draft successfully.';
        }



        $blog->published = $request->published;
        $blog->title = $request->title;
        $blog->slug = $request->slug;
        $blog->summary = $request->summary;
        $blog->content = $request->content;
        $blog->featured_image = $request->featured_image;
        $blog->created_by = $request->created_by;

        $blog->save();

        return response()->json([
            'redirect' => route('admin.blog.edit', ['blog' => $blog->slug]),
            'message' => $responseText
        ], 200);
    }

    // public function edit($id) {
    //     if (Entrust::hasRole(['developer', 'super.admin'])) {
    //         $blog = Blog::withTrashed()->find($id);
    //     } else {
    //         $blog = Blog::find($id);
    //     }


    //     $companies = Company::get();

    //     return view('admin.blog.edit', [
    //         'blog' => $blog,
    //         'companies' => $companies,
    //     ]);
    // }

    public function update(Request $request, $id)
    {
        $blog = Blog::findOrFail($id);

        if (Entrust::hasRole('developer', 'super.admin')) {
            $blog->company_id = $request->company_id;
        } else {
            $blog->company_id = Company::where('shortname', config('app.prefix'))->firstOrFail()->id;
        }

        $redirect = null;
        $responseText = '';

        if ($request->published == 1) {
            $blog->published = $request->published;
            $responseText = 'Post saved and published successfully.';
        } elseif ($request->published == 0) {
            $blog->published = $request->published;
            $responseText = 'Post saved as draft successfully.';
            $redirect = route('admin.blog.edit', ['blog' => $blog->slug]);
        } elseif ($request->published == 2) {
            $responseText = 'Post saved successfully.';
        }

        $blog->title = $request->title;
        $blog->summary = $request->summary;
        $blog->content = $request->content;
        $blog->featured_image = $request->featured_image;
        $blog->created_by = $request->created_by;

        $blog->save();

        return response()->json([
            'redirect' => $redirect,
            'message' => $responseText
        ], 200);
    }

    public function destroy($id)
    {
        $blog = Blog::find($id)->delete();

        return response()->json([
            'redirect' => route('admin.blog.index'),
        ]);
    }

    public function restore($id)
    {
        $blog = Blog::withTrashed()->find($id)->restore();

        return response()->json([
            'redirect' => route('admin.blog.index'),
        ]);
    }
}
