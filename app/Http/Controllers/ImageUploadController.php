<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ImageUpload;
use Storage;
use Session;

class ImageUploadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = ImageUpload::orderBy('created_at', 'desc')->get();

        return response()->json([
            'images' => $images,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customMessages = [
            'max' => 'Max file size is 10Mb. Please upload a smaller file.'
        ];

        $this->validate($request, [
            'file' => 'required|mimes:jpg,jpeg,png,gif,bpm|max:10000',
        ], $customMessages);

        $filename = $request->file->hashName();

        $file = Storage::disk('local')->putFile('temp-uploads', $request->file('file'));

        $optimizedImage = \Image::make(Storage::get($file))->resize(2048, null, function ($constraint) {
            $constraint->aspectRatio();
        })->encode('jpg', 75);

        $optimizedImage = $optimizedImage->stream();

        Storage::disk('s3')->put($filename, $optimizedImage->__toString(), 'public');

        $s3Url = Storage::disk('s3')->url($filename);

        Storage::delete($file);

        $image = new ImageUpload;

        $image->url = $s3Url;

        $image->name = $request->fileName;

        $image->description = $request->fileDescription;

        $image->save();

        return $s3Url;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $image = ImageUpload::find($id);

        $image->delete();

        Session::flash('error', 'That image was rubbish anyway.');

        return (redirect());
    }
}
