<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class ShipmentTrackingController extends Controller
{
	public function index()
	{
		return view(config('app.prefix').'.tracking.index');
	}

	public function getTracking(Request $request)
	{
		$bol = $request->input('bol');
		$pro = $request->input('pro');

		$client = new Client();

		$result = $client->request('GET', 'http://pipeline.flatworldsc.com/tracking.php',[
			'query' => [
				'bol_num' => $bol,
				'pro_num' => $pro
			],
		]);

		if ($result->getStatusCode() > 200)
		{
			$request->session()->flash('error', 'Looks like there was an error fetching that tracking information.  Please try again.');

			return view(config('app.prefix').'.tracking.index');
		}

		$html = $result->getBody(true)->getContents();

		// dd($html);

		return view(config('app.prefix').'.tracking.index')->with('html', $html);
	}
}