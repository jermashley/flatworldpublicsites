<?php

namespace App\Http\Controllers;

use App\Mail\AutoResponder;
use App\Mail\Air2PostContact;
use Illuminate\Http\Request;
use Mail;
use Session;

class Air2PostContactController extends Controller
{
    public function sendEmail(Request $request)
    {
        $validatedData = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'business' => 'required',
            'email' => 'required',
        ]);

        Mail::send(new Air2PostContact((object)$request->all()));

        /**
         * Todo Make auto responder after message is submitted.
         */
        // Mail::send(new AutoResponder((object)$request->all()));

        Session::flash('success', 'Thank you for contacting us!  We will get back to you as soon as possible.');

        return redirect(route(config('app.prefix').'.solutions.air2post'));

    }
}
