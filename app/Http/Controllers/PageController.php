<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManager;
use Validator;
use Entrust;
use App\Page;
use App\Company;
use App\ImageUpload;
use Brand;
use Session;
use Storage;
use Image;

class PageController extends Controller
{

    public function __construct() {

        $this->middleware('auth')->except(['index', 'show']);

        $this->middleware('role:developer|super.admin')->only('restore');

        $this->middleware('role:developer|super.admin|page.admin')->only('delete');

        $this->middleware('role:developer|super.admin|page.admin|page.editor|page.author')->only(['create', 'store', 'edit', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Var for setting admin UI
        $pagePermission = false;

        // Start page query builder
        $PAGE_QUERY = Page::with('company');

        // Check if developer or super admin
        if (Auth::check() && Entrust::hasRole(['developer', 'super.admin'])) {

            $pagePermission = true;
            $pages = $PAGE_QUERY->withTrashed()->orderBy('created_at', 'desc')->paginate(9);

        } elseif (Auth::check() && Entrust::hasRole(['page.admin', 'page.editor', 'page.author'])) {

            $pagePermission = true;
            $pages = $PAGE_QUERY->orderBy('created_at', 'desc')->paginate(9);

        } else {

            $pages = $PAGE_QUERY->orderBy('created_at', 'desc')->paginate(9);

        }

        // dd($pages);

        return view('pages.index', [
            'pages' => $pages,
            'pagePermission' => $pagePermission
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        if (Auth::check()) {
            $pages = Page::with('company')
            ->get();

            $companies = Company::get();

            return view('pages.create', [
                'pages' => $pages,
                'companies' => $companies,
            ]);
        } else {
            return view('auth.fail');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validateData = Validator::make($request->all(), [
            'featured_image' => 'mimes:jpg,jpeg,png,gif,bpm|dimensions:max_width=2048,max_height=2048',
            'images[]' => 'mimes:jpg,jpeg,png,gif,bpm|dimensions:max_width=2048,max_height=2048',
        ]);

        if ($validateData->fails())
        {
            return redirect(route('pages.edit', ['page' => $page->id]))
            ->withErrors($validateData)
            ->withInput();
        } else {

            $page = new Page;

            $page->title = $request->title;
            $page->summary = $request->summary;
            $page->content = $request->content;
            $page->company_shortname = $request->company_shortname;
            $page->published = $request->published;

            $file = $request->file('featured_image');
            $featured_image_filename = $file->hashName();
            $optimizedImage = Image::make($file)->resize(1024, null, function($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 75);
            $optimizedImage =  $optimizedImage->stream();
            $s3 = Storage::disk('s3')->put($featured_image_filename, $optimizedImage->__toString(), 'public');
            $page->featured_image = Storage::disk('s3')->url($featured_image_filename);

            $page->save();


            $page = Page::orderBy('created_at', 'desc')
            ->first();

            $uploaded_files = $request->images;

            foreach ($uploaded_files as $image) {

                $filename = $image->hashName();

                $optimizedImage = Image::make($image)->resize(1024, null, function($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 75);

                $optimizedImage =  $optimizedImage->stream();

                $s3 = Storage::disk('s3')->put($filename, $optimizedImage->__toString(), 'public');

                $gallery_image = new ImageUpload;
                $gallery_image->page_id = $page->id;
                $gallery_image->url = Storage::disk('s3')->url($filename);

                $gallery_image->save();

            }


            Session::flash('success', 'New page confirmed, Captain! Should be the newest card on the deck.');

            return redirect(route('pages.index'));

        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (Auth::check()) {
            $page = Page::find($id);
        } else {
            $page = Page::with('company')
            ->where('company_shortname', Brand::prefix())
            ->where('id', $id)
            ->first();
        }

        $gallery_images = ImageUpload::where('page_id', $id)
        ->get();

        return view('pages.show', [
            'page' => $page,
            'gallery_images' => $gallery_images
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::check())
        {
            $page = Page::find($id);

            $images = ImageUpload::where('page_id', $id)
            ->get();

            $companies = Company::get();
        } else {
            return view('auth.fail');
        }

        return view('pages.edit', [
            'page' => $page,
            'images' => $images,
            'companies' => $companies,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validateData = Validator::make($request->all(), [
            'featured_image' => 'mimes:jpg,jpeg,png,gif,bpm|dimensions:max_width=2048,max_height=2048',
            'images[]' => 'mimes:jpg,jpeg,png,gif,bpm|dimensions:max_width=2048,max_height=2048',
        ]);

        if ($validateData->fails())
        {
            return redirect(route('pages.edit', ['page' => $page->id]))
            ->withErrors($validateData)
            ->withInput();
        }
        else
        {
            $page = Page::find($id);
            // dd($page);

            $page->title = $request->title;

            if ($request->has('featured_image'))
            {

                $file = $request->file('featured_image');
                $filename = $file->hashName();

                $optimizedImage = Image::make($file)->resize(1024, null, function($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 75);

                $optimizedImage =  $optimizedImage->stream();

                $s3 = Storage::disk('s3')->put($filename, $optimizedImage->__toString(), 'public');

                $page->featured_image = Storage::disk('s3')->url($filename);

                $page->summary = $request->summary;
                $page->content = $request->content;
                $page->company_shortname = $request->company_shortname;
                $page->published = $request->published;

                $page->save();

            }
            else
            {

                $page->summary = $request->summary;
                $page->content = $request->content;
                $page->company_shortname = $request->company_shortname;
                $page->published = $request->published;

                $page->save();
            }

            if ($request->has('images'))
            {

                $uploaded_files = $request->images;
                // dd($uploaded_files);

                foreach ($uploaded_files as $image)
                {

                    $filename = $image->hashName();

                    $optimizedImage = Image::make($image)->resize(1024, null, function($constraint) {
                        $constraint->aspectRatio();
                    })->encode('jpg', 75);

                    $optimizedImage =  $optimizedImage->stream();

                    $s3 = Storage::disk('s3')->put($filename, $optimizedImage->__toString(), 'public');

                    $gallery_image = new ImageUpload;
                    $gallery_image->page_id = $page->id;
                    $gallery_image->url = Storage::disk('s3')->url($filename);

                    $gallery_image->save();

                }
            }

            Session::flash('success', 'Cool!  We all make mistakes.  Glad we could give you an edit button for this one.');

            return redirect(route('pages.show', ['page' => $page->id]));

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $page  = Page::find($id);

        $page->delete();

        Session::flash('error', 'Well, that page is gone forever.  Maybe...');

        return(redirect(route('pages.index')));
    }

    public function restore($id) {
        $page = Page::withTrashed()->find($id)->restore();

        Session::flash('success', 'There is great power with the ability to undo deletes. Use it wisely!');

        return redirect(route('pages.index'));
    }
}
