<?php

namespace App;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{

    // User manipulation
    const CREATE_USER = 'create.user';
    const UPDATE_USER = 'update.user';
    const DELETE_USER = 'delete.user';
    const RESTORE_DELETED_USER = 'restore.user.deleted';
    const UPDATE_USER_COMPANY = 'update.user.company';
    const UPDATE_USER_ROLE = 'update.user.role';

    // Content manipulation
    const CREATE_BLOG_POST = 'create.blog.post';
    const PUBLISH_BLOG_POST = 'publish.blog.post';
    const UPDATE_BLOG_POST = 'update.blog.post';
    const DELETE_BLOG_POST = 'delete.blog.post';
    const RESTORE_DELETED_BLOG_POST = 'restore.deleted.blog.post';

    // Career manipulation
    const CREATE_CAREER_LISTING = 'create.career.listing';
    const PUBLISH_CAREER_LISTING = 'publish.career.listing';
    const UPDATE_CAREER_LISTING = 'update.career.listing';
    const DELETE_CAREER_LISTING = 'delete.career.listing';
    const RESTORE_DELETED_CAREER_LISTING = 'restore.deleted.career.listing';

    // Site Pages manipulation
    const CREATE_SITE_PAGE = 'create.site.page';
    const PUBLISH_SITE_PAGE = 'publish.site.page';
    const UPDATE_SITE_PAGE = 'update.site.page';
    const DELETE_SITE_PAGE = 'delete.site.page';
    const RESTORE_DELETED_SITE_PAGE = 'restore.deleted.site.page';

}