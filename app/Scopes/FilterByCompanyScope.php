<?php

namespace App\Scopes;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class FilterByCompanyScope implements Scope
{
    /**
     * @var int|null
     */
    private $companyId;

    /**
     * FilterCompaniesScope constructor.
     *
     * @param null $companyId
     */
    public function __construct($companyId = null)
    {
        $this->companyId = $companyId;
    }

    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model|\App\Traits\Filters\FilterByCompanyTrait $model
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        // Consider null a default user
        if (!empty($this->companyId)) {
            $builder->where($model->getQualifiedCompanyIdColumn(), '=', $this->companyId);
        }
    }
}