<?php

namespace App;

use App\Traits\Filters\FilterByCompanyTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Career extends Model
{

    use SoftDeletes;
    use FilterByCompanyTrait;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */

    protected $dates = ['deleted_at'];

    protected $fillable = ['company_id', 'published', 'title', 'short_description', 'long_description', 'created_by', 'updated_by'];

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id', 'id');
    }
}
