<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class eCommerceForm extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var array
     */
    public $form;


    /**
     * Create a new message instance.
     *
     * @param object $form
     * @return void
     */
    public function __construct($form)
    {
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $form = $this->form;
        return $this->from('ecommerce@flatworldgs.com')->subject('E Commerce Request – ' . $form->firstName . ' ' . $form->lastName . ' - ' . $form->companyName)->replyTo($form->email)->markdown('emails.ecommerce.form');
    }
}
