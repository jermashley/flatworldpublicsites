<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Air2PostContact extends Mailable
{
    use Queueable, SerializesModels;

    public $form;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form)
    {
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->from('air2post@flatworldgs.com')
            ->to('sales@air2post.com')
            ->bcc('keving@prologuetechnology.com')
            ->subject('New Message about Air2Post')
            ->markdown('emails.contact.air2post');
    }
}
