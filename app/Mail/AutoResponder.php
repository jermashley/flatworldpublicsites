<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AutoResponder extends Mailable
{
    use Queueable, SerializesModels;

    public $form;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($form)
    {
        $this->form = $form;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $typeOfBusiness = '';

        if (config('app.env') === 'local' || config('app.env') === 'staging') {
            $from = config('prefix.contacts');
            $destination = config('prefix.contacts');
            $bcc = config('prefix.contacts');
        }

        if (config('app.env') == 'production') {
            $from = config('prefix.contacts');
            $destination = config('prefix.contacts');
            $bcc = 'kevin@prologuetechnology.com';
            if (config('app.prefix') == 'ram-intl' && $this->form->typeOfBusiness == 'Importer') {
                $destination = 'imports@flatworldgs.com';
                $typeOfBusiness = 'Importer';
            }
            if (config('app.prefix') == 'ram-intl' && $this->form->typeOfBusiness == 'eCommerce') {
                $destination = ['ecom@flatworldgs.com, jeagen@flatworldgs.com'];
                $typeOfBusiness = 'eCommerce';
            }
        }

        return $this->to($this->form->email)
            ->from($from, config('app.name'))
            ->subject('Thank you for contacting ' . config('app.name'))
            ->markdown('emails.contact.generalResponse');
    }
}
