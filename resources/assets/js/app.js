/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

const user = document.head.querySelector('meta[name="user"]');

window.Vue = require("vue");
import VeeValidate from "vee-validate";

require("./components");
require("./flat-world-theme");

Vue.use(VeeValidate);

const app = new Vue({
  el: "#app",
});
