window.mobileNavigationToggle = function() {
    let mobileNavigationButton = '#mobileNavigationButton';

    $(mobileNavigationButton).on('click', function() {
        console.log('Expand the nav!');
        $('nav').toggleClass('expanded');
        $('body').toggleClass('nav__overflow');
    });
}

window.overflowMenuHandler = function() {

    let overflowParent = '.hasOverflow';
    let overflowChild = '.navigation-overflow';

    $(overflowParent).on('click', function(e) {
        let nav = $('nav').get(0);
        let footer = $('footer').get(0);
        let clickedEl = $(this).get(0);

        if ($.contains(nav, clickedEl) == true) {
            if($(this).find(overflowChild).is(':hidden')) {
                $(this).closest('nav').find(overflowChild).hide(250);
                $(this).closest('nav').find(overflowParent).removeClass('activeOverflow');
                $(this).addClass('activeOverflow');
                $(this).find(overflowChild).show(350);
            } else if ($(this).find(overflowChild).is(':visible')) {
                $(this).removeClass('activeOverflow');
                $(this).find(overflowChild).hide(250);
            }

            $(document).keyup(function(e){
                if(e.keyCode === 27) {
                    $(overflowParent).removeClass('activeOverflow');
                    $(overflowParent).find(overflowChild).hide(250);
                }
            })

            ;$(document).on('click', function(e) {
                if(!$(e.target).closest(overflowParent).length) {
                    e.stopPropagation();
                    $('.activeOverflow').find(overflowChild).hide(250);
                    $('.activeOverflow').removeClass('activeOverflow');
                }
            });
        }
    });
}

mobileNavigationToggle();
overflowMenuHandler();
