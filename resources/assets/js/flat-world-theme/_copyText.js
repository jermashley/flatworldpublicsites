window.selectText = function(node) {
	node = document.getElementById(node)

	if (document.body.createTextRange) {
		const range = document.body.createTextRange()
		range.moveToElementText(node)
		range.select()
	} else if (window.getSelection) {
		const selection = window.getSelection()
		const range = document.createRange()
		range.selectNodeContents(node)
		selection.removeAllRanges()
		selection.addRange(range)
	} else {
		console.warn("Could not select text in node: Unsupported browser.")
	}
}

window.changeText = function() {
	$('button[data-action="clipboard"]').on('click', function() {
		let replace = $(this);
		replace.empty().html('<i class="fas fa-thumbs-up fa-fw mr-2"></i> Copied!');
		setTimeout(function(){
			replace.empty().html('<i class="fas fa-copy fa-fw mr-2"></i> Copy to Clipboard');
		}, 1500);
	});
}
changeText();