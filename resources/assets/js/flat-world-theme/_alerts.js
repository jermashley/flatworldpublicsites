import anime from 'animejs';

const alertAnimateIn = () => {

    let alert = document.querySelector('.alert');

    anime({
        targets: alert,
        delay: 1750,
        scale: [.85, 1],
        translateY: [-24, 0],
        opacity: 1,
        duration: 750,
        easing: 'easeOutCirc',
        complete: function() {
            anime({
                targets: alert,
                delay: 15000,
                scale: .85,
                translateY: [0, 24],
                opacity: 0,
                duration: 1000,
                easing: 'easeOutCirc',
                complete: function() {
                    if (alert != null) {
                        alert.remove();
                    }
                }
            })
        }
    })
}

alertAnimateIn();

const closeAlert = () => {
    let alert = $('.alert');
    let toggle = alert.find('.dismiss');

    toggle.on('click', function() {
        anime({
            targets: '.alert',
            scale: .85,
            opacity: 0,
            duration: 1000,
            easing: 'easeOutCirc',
        });
        setTimeout( () => {
            $(this).closest('.alert').remove();
        }, 1000);
    });
}

closeAlert();