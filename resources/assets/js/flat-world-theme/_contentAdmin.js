window.manageContent = function() {

    $('*[data-toggle="delete-post"]').on('click', function() {

        $(this).siblings('*[data-toggle="confirm-delete"]').show(250);
    });

    $('.confirmNo').on('click', function() {
        $(this).parent('*[data-toggle="confirm-delete"]').hide(250);
    });

    $('.adminControls').mouseleave(function() {
        if ($('*[data-toggle="confirm-delete"]').is(':visible'))
        {
            $(this).find('*[data-toggle="confirm-delete"]').hide(250);
        }
    });

}

manageContent();