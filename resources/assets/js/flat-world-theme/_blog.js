// import anime from 'animejs';

const dimOtherCards = () => {
	$('.card').on('mouseenter', function() {
		$(this).siblings().css('opacity', .65);
	});
	$('.card').on('mouseleave', function() {
		$(this).siblings().css('opacity', 1);
	});
}

dimOtherCards();