// Passport Components

// Vue.component(
//   'passport-clients',
//   require('./components/passport/Clients.vue').default
// )

// Vue.component(
//   'passport-authorized-clients',
//   require('./components/passport/AuthorizedClients.vue').default
// )

// Vue.component(
//   'passport-personal-access-tokens',
//   require('./components/passport/PersonalAccessTokens.vue').default
// )

// Custom Components

Vue.component("fw-button", require("./components/ButtonComponent.vue").default);

Vue.component("fw-link", require("./components/LinkComponent.vue").default);

Vue.component(
  "fw-navigation",
  require("./components/navigation/NavigationComponent.vue").default
);

// Vue.component(
//   "fw-nav-logo",
//   require("./components/navigation/NavigationLogo.vue").default
// );

Vue.component(
  "fw-nav-link",
  require("./components/navigation/NavigationLink.vue").default
);

Vue.component(
  "fw-card-general",
  require("./components/cards/generalCard.vue").default
);

Vue.component(
  "fw-card-blog",
  require("./components/cards/BlogCard.vue").default
);

Vue.component(
  "fw-card-blog-admin",
  require("./components/cards/BlogAdminCard.vue").default
);

Vue.component(
  "fw-card-document",
  require("./components/cards/DocumentCard.vue").default
);

// Vue.component(
//   "image-gallery",
//   require("./components/imageGallery/ImageGallery.vue").default
// );

Vue.component(
  "image-thumbnail",
  require("./components/imageGallery/ImageThumbnail.vue").default
);

// Vue.component(
//   "general-modal",
//   require("./components/modals/GeneralModal.vue").default
// );

// Vue.component(
//   "tracking-modal",
//   require("./components/modals/TrackingModal.vue").default
// );

// Vue.component(
//   "image-picker",
//   require("./components/inputs/ImagePicker.vue").default
// );

// Vue.component(
//   "image-upload",
//   require("./components/inputs/ImageUpload.vue").default
// );

Vue.component(
  "blog-create",
  require("./components/blog/BlogCreate.vue").default
);

Vue.component(
  "error-alert",
  require("./components/alerts/ErrorAlert.vue").default
);

Vue.component(
  "general-alert",
  require("./components/alerts/GeneralAlert.vue").default
);

Vue.component(
  "feature-card",
  require("./components/cards/FeatureCard.vue").default
);

Vue.component(
  "fw-font-awesome-icon",
  require("./components/FontAwesomeIcon.vue").default
);

// Tracking card
// This should be the same card as the domestic tracking card on Pipeline, but removing some details such as origin and destination address.
// Vue.component(
//   "shipment-tracking",
//   require("./components/tracking/ShipmentTracking.vue").default
// );

// Vue.component(
//   "domestic-tracking-card",
//   require("./components/cards/DomesticTrackingCard.vue").default
// );

Vue.component(
  "e-commerce-form",
  require("./components/forms/eCommerceForm.vue").default
);

Vue.component(
  "career-admin",
  require("./components/CareerAdmin.vue").default
)
