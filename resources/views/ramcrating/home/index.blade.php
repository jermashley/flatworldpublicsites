@extends('app.updated-layout')

@section('hero')

    <div id="hero">

        <div class="hero content">

            <div class="logo">

                <img src="/images/common/gs-full-logo-light.png" alt="">

            </div>

            <div class="snippet cta">

                <span>Crating Innovation</span>

            </div>

        </div>

    </div>

@endsection

@section('content')

    <div class="content__featureWall" id="culture">

        <header class="featureWall__header">

            <span>Our Culture</span>
            <span></span>

        </header>

        <div class="featureWall__blockHalf">

            <h2 class="mb-4">Quality Products Reflect a Culture of Excellence</h2>

            <p class="text paragraph general mb-4">At Flat World Global Solutions, our Culture of Excellence is built on creating an environment that makes our employees proud to be part of our family. What we do matters, so we pride ourselves in our hard work, responsive interactions with clients and team members, and transparency in all our dealings.</p>

            <p class="text paragraph general mb-4">Our team of custom crating professionals literally grew up in this business, and know how important it is to get your cargo to its destination safely and intact. Sometimes that may require new solutions, or an innovative approach to building the crate you need.</p>

            <p class="text paragraph general mb-4">And that’s what we’re here for. We’re proud to work with you for all your custom crating needs, and look forward to creating a partnership based on excellence, innovation, and service.</p>

        </div>

        <div class="featureWall__blockHalf">

            <div class="block__image">

                <img src="{{ Brand::asset('images/%s/crate-1.png') }}" alt="">

            </div>

        </div>

    </div>

    <div class="content__featureWall" id="materials">

        <header class="featureWall__header">

            <span>Quality Materials</span>
            <span></span>

        </header>

        <div class="featureWall__blockHalf">

            <h2 class="mb-4">Industry-leading Materials and Process</h2>

            <p class="text paragraph general mb-4">Whether our customers are shipping fine art or heavy industrial equipment, one thing is certain: what goes inside of our custom crates is valuable—sometimes even irreplaceable—and needs to be protected by crates manufactured with high quality material that meets domestic and international shipping standards, including <strong>ISPM-15</strong>.</p>

            <p class="text paragraph general mb-4">We’re proud you’ve chosen to work with Flat World Global Solutions, and look forward to being your custom crating partner. That partnership requires us to deliver on our promise to every customer:</p>

            <p class="text paragraph general mb-4">Your shipment will arrive safely, protected by a custom crate made from the highest quality material, built by crating experts who truly care about the cargo you ship in our crates.</p>

        </div>

        <div class="featureWall__blockHalf">

            <div class="block__image">

                <img src="{{ Brand::asset('images/%s/crate-2.png') }}" alt="">

            </div>

        </div>

    </div>

    <div class="content__featureWall" id="service">

        <header class="featureWall__header">

            <span>Exceptional Service</span>
            <span></span>

        </header>

        <div class="featureWall__blockHalf">

            <h2 class="mb-4">We Don't Build Crates, We Build Relationships</h2>

            <p class="text paragraph general mb-4">We believe exceptional service means telling our customers what they need to know, rather than what they want to hear. We believe exceptional service means you talk to a real human being on the phone, ready to answer your questions and deliver solutions. We believe exceptional service means you work with custom crating experts, like Steve Raetz, Jr., our Director of Custom Crating.</p>

            <p class="text paragraph general mb-4">In fact, Steve’s customers know him as “Crate Boy”, a superhero when it comes designing custom crates.</p>

            <p class="text paragraph general mb-4">It isn’t hard to imagine why our customers value working with Steve. He literally grew up in the crating business. His father owned a St. Louis-based crating company, and Steve worked alongside his dad to learn the crating business for seven years.</p>

            <p class="text paragraph general mb-4"><em>That’s what excellence means to us: real people, with real expertise, providing heroic customer service.</em></p>

        </div>

        <div class="featureWall__blockHalf">

            <div class="block__image">

                <img src="{{ Brand::asset('images/%s/crate-3.png') }}" alt="">

            </div>

        </div>

    </div>

    <fw-link href="{{ route('contact.index') }}" target="_self" design="outline" state="info" size="small" icon="pen" style="width: 100%;">
        Get In Touch
    </fw-link>

    <div class="d-flex flex-row flex-wrap justify-content-around" style="margin: 48px 0 0;">
        <fw-card-general
            title="Bob B. (Warehouse Manager)"
            image="{{ Brand::asset('images/businessman.jpg') }}"
            image-alt="Less-than-truckload art."
            circle-image
        >
        <span>Flat World Global Solutions has always given us the confidence to ship internationally, knowing our products will always arrive safely and intact.</span>
        </fw-card-general>

        <fw-card-general
            title="Susan A. (Technology Distribution)"
            image="{{ Brand::asset('images/businesswoman.jpg') }}"
            image-alt="Less-than-truckload art."
            circle-image
        >
        <span>We know that we can rely on Flat World Global Solutions to keep our products safe while being transported. They have exceeded all of our expectations.</span>
        </fw-card-general>

    </div>

@endsection
