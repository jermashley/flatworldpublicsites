@extends('app.updated-layout')

@section('title', 'Contact | ')

@section('content')

    <h1 class="page__title my-1">Contact</h1>

    <h3 class="page__subTitle my-1">Would you prefer speaking with someone on the phone?</h3>

    <h3 class="page__subTitle my-1">Give us a call at <a href="tel:+13144273000" class="link">(314) 427-3000</a></h3>

    @include ('partials.contactForm')


    <div class="mt-5 contact" style="display: flex; flex-flow: row; justify-content: center;">

        <fw-card-general
            title="Flat World Global Solutions"
            href="https://www.google.com/maps/place/Ram+Custom+Crating/@38.7635235,-90.3475159,17z/data=!3m1!4b1!4m5!3m4!1s0x87df36f19490d7db:0x142a06385779d34c!8m2!3d38.7635193!4d-90.3453272"
            button-icon="map-marker-alt"
            button-text="Get Directions"
        >
            <div class="contactInfo">
                <span>
                    <fw-font-awesome-icon icon="phone"></fw-font-awesome-icon>
                    {{-- <i class="fa fa-phone mr-2"></i> --}}
                    <a href="tel: +13144273000" class="link">(314) 427-3000</a>
                </span>
            </div>

            <address>
                <span>8949 Frost Ave</span>
                <span>Berkeley, MO</span>
                <span>63134</span>
            </address>
        </fw-card-general>

    </div>

@endsection
