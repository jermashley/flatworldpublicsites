@extends('app.updated-layout')

@section('title', $user->first_name . ' ' . $user->last_name . ' | ')

@section('content')

	<h1 class="page__title my-1">Hi {{ $user->first_name }}!</h1>

    <h3 class="page__subTitle my-1">
		Here's where you can keep your personal info up-to-date.
	</h3>


    <div class="d-flex flex-row flex-wrap justify-content-around mt-5 mb-5">

		@component ('components.cards.general', [
			'cardClasses' => 'large header mx-2 mb-5',
			'hasHero' => true,
			'hasShadow' => true,
			'hasContent' =>true,
			'hasFooter' => true,
			'hasActions' => true,
			'heroImageSource' => 'https://images.unsplash.com/photo-1501254212636-6c9a740bc2d4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b7ad6d3dee7bb70bdabb5d2169c7fa74&auto=format&fit=crop&w=562&q=80',
			'heroImageClasses' => 'cover center blurred',
			'headingClasses' => 'text heading h5 font-weight-800 center uppercase',
			'textClasses' => 'userProfile',
			'actions' => [
				[
					'text' => 'Update',
					'classes' => 'btn btn__primary',
					'link' => route('userProfile.edit')
				],
			],
		])
		@slot('insetText')
			<span class="text white center uppercase heading h5 font-weight-900">
					@foreach ($roles as $role)
					{{ $role->display_name }}{{$loop->last ? '' : ', '}}
					@endforeach
			</span>
			<span class="text white center heading h3 font-weight-300">
				{{ $user->company->name }}
			</span>
		@endslot

		@slot('cardText')
			@component ('components.userProfileGrid', [
				'userProfile' => [
					[
						'rowTitle' => 'Name',
						'rowValue' => $user->first_name . ' ' . $user->last_name,
					],
					[
						'rowTitle' => 'Email',
						'rowValue' => $user->email,
					]
				],
			])
			@endcomponent

			<div class="d-flex flex-row justify-content-center align-items-center" style="margin:2rem 0;">
				<fw-link href="{{ route('changePassword.index') }}" target="_self" design="text" state="dark" size="small" icon="lock">
					Change Password
				</fw-link>
			</div>

			<hr style="border-color: rgba(0,0,0,.1);
			border-width: 1px;
			border-bottom: none;
			width: calc(100% - 3rem);">
		@endslot
		@endcomponent

    </div>

@endsection