@extends('app.updated-layout')

@section('title', $user->first_name . ' ' . $user->last_name . ' | ')

@section('content')

	<h1 class="page__title my-1">Hi {{ $user->first_name }}!</h1>

    <h3 class="page__subTitle my-1">
		Here's where you can keep your personal info up-to-date.
	</h3>


    <div class="d-flex flex-row flex-wrap justify-content-around mt-5 mb-5">

		@component ('components.cards.general', [
			'cardClasses' => 'large header mx-2 mb-5',
			'hasHero' => true,
			'hasShadow' => true,
			'hasContent' =>true,
			'hasFooter' => true,
			'heroImageSource' => 'https://images.unsplash.com/photo-1501254212636-6c9a740bc2d4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b7ad6d3dee7bb70bdabb5d2169c7fa74&auto=format&fit=crop&w=562&q=80',
			'heroImageClasses' => 'cover center blurred',
			'headingClasses' => 'text heading h5 font-weight-800 center uppercase',
			'textClasses' => 'userProfile',
		])
		@slot('insetText')
			<span class="text white center uppercase heading h5 font-weight-900">
					@foreach ($roles as $role)
					{{ $role->display_name }}{{$loop->last ? '' : ', '}}
					@endforeach
			</span>
			<span class="text white center heading h3 font-weight-300">
				{{ $user->company->name }}
			</span>
		@endslot

		@slot('cardText')
			<form action="{{ route('userProfile.update') }}" method="POST">
				@method('PATCH')
				@csrf

				<div class="form-group">
					<label for="first_name" @if ($errors->has('first_name')) class="error" @endif>
						<span class="label-text">First Name</span>
						<input type="text" name="first_name" id="first_name" value="{{ $user->first_name }}" placeholder="John" required>

						@if ($errors->has('first_name'))
							<span class="error">
								<strong>{{ $errors->first('first_name') }}</strong>
							</span>
						@endif
					</label>
				</div>

				<div class="form-group">
					<label for="last_name" @if ($errors->has('last_name')) class="error" @endif>
						<span class="label-text">Last Name</span>
						<input type="text" name="last_name" id="last_name" value="{{ $user->last_name }}" placeholder="Doe" required>

						@if ($errors->has('last_name'))
							<span class="error">
								<strong>{{ $errors->first('last_name') }}</strong>
							</span>
						@endif
					</label>
				</div>

				<div class="form-group">
					<label for="email" @if ($errors->has('email')) class="error" @endif>
						<span class="label-text">E-mail</span>
						<input type="text" name="email" id="last_name" value="{{ $user->email }}" placeholder="Doe" required>

						@if ($errors->has('email'))
							<span class="error">
								<strong>{{ $errors->first('email') }}</strong>
							</span>
						@endif
					</label>
				</div>

				<hr style="border-color: rgba(0,0,0,.1);
				border-width: 1px;
				border-bottom: none;
				width: calc(100% - 3rem);">

				<div class="d-flex flex-row justify-content-center align-items-center mt-3">
					<fw-link href="{{ route('changePassword.index') }}" target="_self" design="text" state="dark" size="small" icon="lock">
						Change Password
					</fw-link>
				</div>

				<hr style="border-color: rgba(0,0,0,.1);
				border-width: 1px;
				border-bottom: none;
				width: calc(100% - 3rem);">

				<div class="d-flex flex-row justify-content-center align-items-center mt-3">
					<fw-link href="{{ route('userProfile.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
						Cancel
					</fw-link>

					<fw-button design="solid" state="info" size="small" icon="paper-plane" type="save" role="button">
						Save
					</fw-button>
				</div>
			</form>
		@endslot
		@endcomponent

    </div>

@endsection