<!DOCTYPE html>
<html lang="en" id="copy">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
</head>
<body>
	<table
		id="signature_{{ $signature->id }}"
		name="signature"
		style="display: inline-table; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; color: rgba(61, 71, 77, 1); margin: 0;">
		<tbody>
			<tr>
				<td name="user-name">
					<span style="font-size: 16px; font-weight: 400;">{{ $signature->user->first_name}} {{ $signature->user->last_name}}</span>
				</td>
			</tr>
			<tr>
				<td name="user-title">
					<span style="font-size: 11px; font-weight: 900; text-transform: uppercase;">{{ $signature->user->title }}</span>
				</td>
			</tr>
			<tr>
				<td name="user-company">
					<span style="display: inline-block; box-sizing: border-box; padding: 16px 0 0; font-size: 13px; font-weight: 900;">{{ $signature->company->name }}</span>
				</td>
			</tr>
			<tr>
				<td name="user-address">
					<span style="display: inline-block; box-sizing: border-box; padding: 2px 0 0; font-size: 13px; font-weight: 500;">@if (empty($signature->address_1)){{ $signature->company->address_1 }}@else{{ $signature->address_1 }}@endif</span>
				</td>
			</tr>
			<tr>
				<td name="user-address">
					<span style="font-size: 13px; font-weight: 500;">@if (empty($signature->address_2)){{ $signature->company->address_2 }}@else{{ $signature->address_2 }}@endif</span>
				</td>
			</tr>
			<tr>
				<td name="user-address">
					<span style="font-size: 13px; font-weight: 500;">@if (empty($signature->city)){{ $signature->company->city }}@else{{ $signature->city }}@endif, @if (empty($signature->state)){{ $signature->company->state }}@else{{ $signature->state }}@endif @if (empty($signature->zip)){{ $signature->company->zip }}@else{{ $signature->zip }}@endif</span>
				</td>
			</tr>

			@if ($signature->has_phone === 1)
			<tr>
				<td>
					<table style="display: inline-block; box-sizing: border-box; margin: 16px 0 0; padding: 0; font-size: 13px; font-weight: 500; height: 20px;">
						<tbody>
							<tr>
								<td>
									<table style="margin: 0 4px 0 0; font-size: 13px;">
										<tbody>
											<tr>
												<td style="vertical-align: middle;">
													<img src="data:image/svg+xml,%3Csvg color='%233D474D' class='svg-inline--fa fa-phone fa-w-16 fa-fw' aria-hidden='true' data-prefix='fas' data-icon='phone' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' width='13' height='13' data-fa-i2svg=''%3E%3Cpath fill='currentColor' d='M493.4 24.6l-104-24c-11.3-2.6-22.9 3.3-27.5 13.9l-48 112c-4.2 9.8-1.4 21.3 6.9 28l60.6 49.6c-36 76.7-98.9 140.5-177.2 177.2l-49.6-60.6c-6.8-8.3-18.2-11.1-28-6.9l-112 48C3.9 366.5-2 378.1.6 389.4l24 104C27.1 504.2 36.7 512 48 512c256.1 0 464-207.5 464-464 0-11.2-7.7-20.9-18.6-23.4z'%3E%3C/path%3E%3C/svg%3E" alt="">
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td>
									<a href="tel:@if (empty($signature->phone)){{ $signature->company->phone }} @else {{ $signature->phone }} @endif" style="color: rgba(52, 139, 183, 1); text-decoration: none; border: none;">@if (empty($signature->phone)){{ $signature->company->phone }}@else{{ $signature->phone }}@endif
									</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			@endif

			@if ($signature->has_cell === 1)
			<tr>
				<td>
					<table style="display: inline-block; box-sizing: border-box; padding: 0; font-size: 13px; font-weight: 500; height: 20px;">
						<tbody>
							<tr>
								<td>
									<table style="margin: 0 4px 0 0; font-size: 13px;">
										<tbody>
											<tr>
												<td style="vertical-align: middle;">
													<img src="data:image/svg+xml,%3Csvg color='%233D474D' class='svg-inline--fa fa-mobile-alt fa-w-10 fa-fw' aria-hidden='true' data-prefix='fas' data-icon='mobile-alt' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 320 512' width='13' height='13' data-fa-i2svg=''%3E%3Cpath fill='currentColor' d='M272 0H48C21.5 0 0 21.5 0 48v416c0 26.5 21.5 48 48 48h224c26.5 0 48-21.5 48-48V48c0-26.5-21.5-48-48-48zM160 480c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm112-108c0 6.6-5.4 12-12 12H60c-6.6 0-12-5.4-12-12V60c0-6.6 5.4-12 12-12h200c6.6 0 12 5.4 12 12v312z'%3E%3C/path%3E%3C/svg%3E" alt="">
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td>
									<a href="tel:+16362651460" style="color: rgba(52, 139, 183, 1); text-decoration: none; border: none;">{{ $signature->cell }}</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			@endif

			@if ($signature->has_fax === 1)
			<tr>
				<td>
					<table style="display: inline-block; box-sizing: border-box; padding: 0; font-size: 13px; font-weight: 500; height: 20px;">
						<tbody>
							<tr>
								<td>
									<table style="margin: 0 4px 0 0; font-size: 13px;">
										<tbody>
											<tr>
												<td style="vertical-align: middle;">
													<img src="data:image/svg+xml,%3Csvg color='%233D474D' class='svg-inline--fa fa-fax fa-w-16 fa-fw' aria-hidden='true' data-prefix='fas' data-icon='fax' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' width='13' height='13' data-fa-i2svg=''%3E%3Cpath fill='currentColor' d='M128 144v320c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V144c0-26.51 21.49-48 48-48h32c26.51 0 48 21.49 48 48zm384 64v256c0 26.51-21.49 48-48 48H192c-26.51 0-48-21.49-48-48V40c0-22.091 17.909-40 40-40h207.432a39.996 39.996 0 0 1 28.284 11.716l48.569 48.569A39.999 39.999 0 0 1 480 88.568v74.174c18.641 6.591 32 24.36 32 45.258zm-320-16h240V96h-24c-13.203 0-24-10.797-24-24V48H192v144zm96 204c0-6.627-5.373-12-12-12h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40zm0-128c0-6.627-5.373-12-12-12h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40zm128 128c0-6.627-5.373-12-12-12h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40zm0-128c0-6.627-5.373-12-12-12h-40c-6.627 0-12 5.373-12 12v40c0 6.627 5.373 12 12 12h40c6.627 0 12-5.373 12-12v-40z'%3E%3C/path%3E%3C/svg%3E" alt="">
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td>
									@if (empty($signature->fax))
									{{ $signature->company->fax }}
									@else
									{{ $signature->fax }}
									@endif
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			@endif

			@if ($signature->has_email === 1)
			<tr>
				<td>
					<table style="display: inline-block; box-sizing: border-box; padding: 0; font-size: 13px; font-weight: 500; height: 20px;">
						<tbody>
							<tr>
								<td>
									<table style="margin: 0 4px 0 0; font-size: 13px;">
										<tbody>
											<tr>
												<td style="vertical-align: middle;">
													<img src="data:image/svg+xml,%3Csvg color='%233D474D' class='svg-inline--fa fa-envelope fa-w-16 fa-fw' aria-hidden='true' data-prefix='fas' data-icon='envelope' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 512 512' width='13' height='13' data-fa-i2svg=''%3E%3Cpath fill='currentColor' d='M502.3 190.8c3.9-3.1 9.7-.2 9.7 4.7V400c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V195.6c0-5 5.7-7.8 9.7-4.7 22.4 17.4 52.1 39.5 154.1 113.6 21.1 15.4 56.7 47.8 92.2 47.6 35.7.3 72-32.8 92.3-47.6 102-74.1 131.6-96.3 154-113.7zM256 320c23.2.4 56.6-29.2 73.4-41.4 132.7-96.3 142.8-104.7 173.4-128.7 5.8-4.5 9.2-11.5 9.2-18.9v-19c0-26.5-21.5-48-48-48H48C21.5 64 0 85.5 0 112v19c0 7.4 3.4 14.3 9.2 18.9 30.6 23.9 40.7 32.4 173.4 128.7 16.8 12.2 50.2 41.8 73.4 41.4z'%3E%3C/path%3E%3C/svg%3E" alt="">
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td>
									<a href="mailto:{{ $signature->email }}" style="color: rgba(52, 139, 183, 1); text-decoration: none; border: none;">{{ $signature->email }}</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
			@endif

			<tr>
				<td>
					<table style="display: inline-block; box-sizing: border-box; padding: 12px 0 0; font-size: 13px; font-weight: 500; height: 20px;">
						<tbody>
							<tr>
								<td>
									<table style="margin: 0 4px 0 0; font-size: 13px;">
										<tbody>
											<tr>
												<td style="vertical-align: middle;">
													<img src="data:image/svg+xml,%3Csvg color='%233D474D' class='svg-inline--fa fa-globe fa-w-16 fa-fw' aria-hidden='true' data-prefix='fas' data-icon='globe' role='img' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 496 512' width='13' height='13' data-fa-i2svg=''%3E%3Cpath fill='currentColor' d='M336.5 160C322 70.7 287.8 8 248 8s-74 62.7-88.5 152h177zM152 256c0 22.2 1.2 43.5 3.3 64h185.3c2.1-20.5 3.3-41.8 3.3-64s-1.2-43.5-3.3-64H155.3c-2.1 20.5-3.3 41.8-3.3 64zm324.7-96c-28.6-67.9-86.5-120.4-158-141.6 24.4 33.8 41.2 84.7 50 141.6h108zM177.2 18.4C105.8 39.6 47.8 92.1 19.3 160h108c8.7-56.9 25.5-107.8 49.9-141.6zM487.4 192H372.7c2.1 21 3.3 42.5 3.3 64s-1.2 43-3.3 64h114.6c5.5-20.5 8.6-41.8 8.6-64s-3.1-43.5-8.5-64zM120 256c0-21.5 1.2-43 3.3-64H8.6C3.2 212.5 0 233.8 0 256s3.2 43.5 8.6 64h114.6c-2-21-3.2-42.5-3.2-64zm39.5 96c14.5 89.3 48.7 152 88.5 152s74-62.7 88.5-152h-177zm159.3 141.6c71.4-21.2 129.4-73.7 158-141.6h-108c-8.8 56.9-25.6 107.8-50 141.6zM19.3 352c28.6 67.9 86.5 120.4 158 141.6-24.4-33.8-41.2-84.7-50-141.6h-108z'%3E%3C/path%3E%3C/svg%3E" alt="">
												</td>
											</tr>
										</tbody>
									</table>
								</td>
								<td>
									<a href="http://{{ $signature->website }}" style="color: rgba(52, 139, 183, 1); text-decoration: none; border: none;">{{ $signature->company->website }}</a>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>
</body>
</html>