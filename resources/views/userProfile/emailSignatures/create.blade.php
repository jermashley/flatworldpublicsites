@extends('app.updated-layout')

@section('title', $user->first_name . '\'s Signatures' . ' | ')

@section('content')

<div class="signature-card">
	<form action="{{ route('signatures.store') }}" method="post">
		@method('POST')
		@csrf

		<h2 class="mb-3">General Information</h2>

		<div class="form-group">
			<label for="first_name" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">First Name</div>
				<input type="text" name="first_name" id="first_name" value="{{ Auth::user()->first_name }}" placeholder="Pam" disabled>

				@if ($errors->has('first_name'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>

			<label for="last_name" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Last Name</div>
				<input type="text" name="last_name" id="last_name" value="{{ Auth::user()->last_name }}" placeholder="Halpert" disabled>

				@if ($errors->has('last_name'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label for="title" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Title</div>
				<input type="text" name="title" id="title" value="{{ Auth::user()->title }}" placeholder="Office Manager" disabled>

				@if ($errors->has('title'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		@role('super.admin')
		<div class="form-group">
			<label for="company" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Company</div>
				<select name="company" id="company">
					<option value="" disabled selected>Choose One</option>
					@foreach ($companies as $company)
					<option value="{{ $company->id }}">{{ $company->name }}</option>
					@endforeach
				</select>

				@if ($errors->has('company'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>

			<label class="inputGroup checkbox" for="custom_address">
				<div class="label-text">Use Custom Address</div>
				<input type="checkbox" name="custom_address">
			</label>
		</div>
		@endrole

		@role('super.admin')
		<h2 class="mt-4 mb-3">Custom Address</h2>

		<div class="form-group">
			<label for="address_1" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Address Line 1</div>
				<input type="text" name="address_1" id="address_1" value="{{ old('address_1') }}" placeholder="1725 Slough Avenue">

				@if ($errors->has('address_1'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label for="address_2" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Address Line 2</div>
				<input type="text" name="address_2" id="address_2" value="{{ old('address_2') }}" placeholder="Suite 200">

				@if ($errors->has('address_2'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label for="city" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">City</div>
				<input type="text" name="city" id="city" value="{{ old('city') }}" placeholder="Scranton">

				@if ($errors->has('city'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>

			<label for="state" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">State</div>
				<select name="state" id="state">
					@foreach ($states as $abbr => $state)
					<option value="{{ $state }}"
					@if ($selectedState === $state) selected @endif
					>{{ $abbr }}</option>
					@endforeach
				</select>

				@if ($errors->has('state'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>

			<label for="zip" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Zip Code</div>
				<input type="text" name="zip" id="zip" value="{{ old('zip') }}" placeholder="18501">

				@if ($errors->has('zip'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>
		@endrole

		<h2 class="mt-4 mb-3">Contact Information</h2>

		<div class="form-group">
			<label class="checkbox" for="has_phone" style="width: fit-content !important;">
				<input type="checkbox" name="has_phone">
			</label>

			<label for="phone" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Office Phone</div>
				<input type="tel" name="phone" id="phone" value="{{ Auth::user()->company->phone }}" placeholder="(213) 570-4649">

				@if ($errors->has('phone'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label class="checkbox" for="has_fax" style="width: fit-content !important;">
				<input type="checkbox" name="has_fax">
			</label>

			<label for="fax" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Fax Line</div>
				<input type="tel" name="fax" id="fax" value="{{ Auth::user()->company->fax }}" placeholder="(213) 570-4648">

				@if ($errors->has('fax'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label class="checkbox" for="has_cell" style="width: fit-content !important;">
					<span class="label-text">&nbsp;</span>
				<input type="checkbox" name="has_cell">
			</label>

			<label for="cell" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">Cell Phone</div>
				<input type="tel" name="cell" id="cell" value="{{ old('cell') }}" placeholder="(213) 804-6744">
			</label>
		</div>

		<div class="form-group">
			<label class="" for="has_email" style="width: fit-content !important;">
				<span class="label-text">&nbsp;</span>
				<input type="checkbox" name="has_email">
			</label>

			<label for="email" @if ($errors->has('business')) class="error" @endif>
				<div class="label-text">E-Mail</div>
				<input type="email" name="email" id="email" value="{{ Auth::user()->email }}" placeholder="pam.halpert@dundermifflin.com">
			</label>
		</div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-link href="{{ route('signatures.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
                Cancel
            </fw-link>

            <fw-button design="solid" state="info" size="small" icon="save" type="submit" role="button">
				Save Signature
            </fw-button>
        </div>
	</form>
</div>

@endsection