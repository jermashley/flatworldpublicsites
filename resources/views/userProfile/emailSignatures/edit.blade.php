@extends ('app.updated-layout')

@section ('title', 'Edit Your Signature | ')

@section ('content')

<div class="signature-card">
	<form action="{{ route('signatures.update') }}" method="post">
		@method('PATCH')
		@csrf

		<h2 class="mb-3">General Information</h2>

		<div class="form-group">
			<label class="inputGroup @if ($errors->has('first_name')) error @endif" for="first_name">
				<div class="labelText">First Name</div>
				<input type="text" name="first_name" id="first_name" value="{{ Auth::user()->first_name }}" placeholder="" disabled>

				@if ($errors->has('first_name'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>

			<label class="inputGroup @if ($errors->has('last_name')) error @endif" for="last_name">
				<div class="labelText">Last Name</div>
				<input type="text" name="last_name" id="last_name" value="{{ Auth::user()->last_name }}" placeholder="" disabled>

				@if ($errors->has('last_name'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label class="inputGroup @if ($errors->has('title')) error @endif" for="title">
				<div class="labelText">Title</div>
				<input type="text" name="title" id="title" value="{{ Auth::user()->title }}" placeholder="" disabled>

				@if ($errors->has('title'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		@role('super.admin')
		<div class="form-group">
			<label class="inputGroup @if ($errors->has('company')) error @endif" for="company">
				<div class="labelText">Company</div>
				<select name="company" id="company">
					<option value="" disabled selected>Choose One</option>
					@foreach ($companies as $company)
					<option value="{{ $company->id }}">{{ $company->name }}</option>
					@endforeach
				</select>

				@if ($errors->has('company'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>

			<label class="inputGroup checkbox" for="custom_address">
				<div class="labelText">Use Custom Address</div>
				<input type="checkbox" name="custom_address">
			</label>
		</div>
		@endrole

		@role('super.admin')
		<h2 class="mt-4 mb-3">Custom Address</h2>

		<div class="form-group">
			<label class="inputGroup @if ($errors->has('address_1')) error @endif" for="address_1">
				<div class="labelText">Address Line 1</div>
				<input type="text" name="address_1" id="address_1" value="{{ $signature->address_1 }}" placeholder="Address Line 1">

				@if ($errors->has('address_1'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label class="inputGroup @if ($errors->has('address_2')) error @endif" for="address_2">
				<div class="labelText">Address Line 2</div>
				<input type="text" name="address_2" 	id="address_2" value="{{ $signature->address_2 }}" placeholder="Address Line 2">

				@if ($errors->has('address_2'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label class="inputGroup @if ($errors->has('city')) error @endif" for="city">
				<div class="labelText">City</div>
				<input type="text" name="city" id="city" value="{{ $signature->address_2 }}" placeholder="City">

				@if ($errors->has('city'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>

			<label class="inputGroup @if ($errors->has('state')) error @endif chars-8" for="state">
				<div class="labelText">State</div>
				<select name="state" id="state">
					@foreach ($states as $abbr => $state)
					<option value="{{ $state }}"
					@if ($signature->state === $state) selected @endif
					>{{ $abbr }}</option>
					@endforeach
				</select>

				@if ($errors->has('state'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>

			<label class="inputGroup @if ($errors->has('zip')) error @endif" for="zip">
				<div class="labelText">Zip Code</div>
				<input type="text" name="zip" id="zip" value="{{ $signature->zip }}" placeholder="Zip Code">

				@if ($errors->has('zip'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>
		@endrole

		<h2 class="mt-4 mb-3">Contact Information</h2>

		<div class="form-group">
			<label class="inputGroup checkbox" for="has_phone" style="width: fit-content !important;">
				<input type="checkbox" name="has_phone"
				@if ($signature->has_phone === 1)
				checked
				@endif
				>
			</label>

			<label class="inputGroup @if ($errors->has('phone')) error @endif" for="phone">
				<div class="labelText">Office Phone</div>
				<input type="tel" name="phone" id="phone" value="@if ($signature->has_phone === 1) {{ $signature->phone }} @else {{ Auth::user()->company->phone }} @endif" placeholder="Office Phone">

				@if ($errors->has('phone'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label class="inputGroup checkbox" for="has_fax" style="width: fit-content !important;">
				<input type="checkbox" name="has_fax"
				@if ($signature->has_fax === 1)
				checked
				@endif
				>
			</label>

			<label class="inputGroup @if ($errors->has('fax')) error @endif" for="fax">
				<div class="labelText">Fax Line</div>
				<input type="tel" name="fax" id="fax" value="@if ($signature->has_fax === 1) {{ $signature->fax }} @else {{ Auth::user()->company->fax }} @endif" placeholder="Fax Line">

				@if ($errors->has('fax'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label class="inputGroup checkbox" for="has_cell" style="width: fit-content !important;">
				<input type="checkbox" name="has_cell"
				@if ($signature->has_cell === 1)
				checked
				@endif
				>
			</label>

			<label class="inputGroup @if ($errors->has('cell')) error @endif" for="cell">
				<div class="labelText">Cell Phone</div>
				<input type="tel" name="cell" id="cell" value="{{ $signature->cell }}" placeholder="Cell Phone">

				@if ($errors->has('cell'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

		<div class="form-group">
			<label class="inputGroup checkbox" for="has_email" style="width: fit-content !important;">
				<input type="checkbox" name="has_email"
				@if ($signature->has_email === 1)
				checked
				@endif
				>
			</label>

			<label class="inputGroup @if ($errors->has('email')) error @endif" for="email">
				<div class="labelText">E-Mail</div>
				<input type="email" name="email" id="email" value="{{ $signature->email }}" placeholder="E-Mail Address">

				@if ($errors->has('email'))
					<span class="error">
						<strong>
							<i class="fas fa-exclamation-circle fa-fw"></i>
						</strong>
					</span>
				@endif
			</label>
		</div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-link href="{{ route('signatures.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
                Cancel
            </fw-link>

            <fw-button design="solid" state="info" size="small" icon="save" type="submit" role="button">
				Save Signature
            </fw-button>
        </div>
	</form>
</div>

@endsection