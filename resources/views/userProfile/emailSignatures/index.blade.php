@extends('app.updated-layout')

@section('title', $user->first_name . '\'s Signatures' . ' | ')

@section('content')
<div style="display: flex; flex-flow: row wrap; justify-content: flex-start; align-items: stretch; align-content: flex-start;">
	<div class="signature-card" style="justify-content: center; align-self: stretch;">
		<a href="{{ route('signatures.create') }}" class="new-signature" style="display: flex; flex-flow: row nowrap; justify-content: center; align-items: center; writing-mode: vertical-rl; font-size: 18px; font-weight: 800; text-transform: uppercase; text-align: center;">
			<i class="fas fa-plus fa-fw" style="margin: 12px;"></i> new signature
		</a>
	</div>
	@foreach ($signatures as $signature)
	<div class="signature-card">
		<table
			id="signature_{{ $signature->id }}"
			name="signature"
			style="display: inline-table; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol'; color: rgba(61, 71, 77, 1); margin: 0;">
			<tbody>
				<tr>
					<td name="user-name">
						<span style="font-size: 16px; font-weight: 400;">{{ $signature->user->first_name}} {{ $signature->user->last_name}}</span>
					</td>
				</tr>
				<tr>
					<td name="user-title">
						<span style="font-size: 11px; font-weight: 900; text-transform: uppercase;">{{ $signature->user->title }}</span>
					</td>
				</tr>
				<tr>
					<td name="user-company">
						<span style="display: inline-block; box-sizing: border-box; padding: 16px 0 0; font-size: 13px; font-weight: 900;">{{ $signature->company->name }}</span>
					</td>
				</tr>
				<tr>
					<td>
						<table style="margin: 0 0 24px;">
							<tr>
								<td name="user-address">
									<span style="display: inline-block; box-sizing: border-box; padding: 2px 0 0; font-size: 13px; font-weight: 500;">@if (empty($signature->address_1)){{ $signature->company->address_1 }}@else{{ $signature->address_1 }}@endif</span>
								</td>
							</tr>
							<tr>
								<td name="user-address">
									<span style="font-size: 13px; font-weight: 500;">@if (empty($signature->address_2)){{ $signature->company->address_2 }}@else{{ $signature->address_2 }}@endif</span>
								</td>
							</tr>
							<tr>
								<td name="user-address">
									<span style="font-size: 13px; font-weight: 500;">@if (empty($signature->city)){{ $signature->company->city }}@else{{ $signature->city }}@endif, @if (empty($signature->state)){{ $signature->company->state }}@else{{ $signature->state }}@endif @if (empty($signature->zip)){{ $signature->company->zip }}@else{{ $signature->zip }}@endif</span>
								</td>
							</tr>
						</table>
					</td>
				</tr>

				@if ($signature->has_phone === 1)
				<tr>
					<td>
						<table style="display: inline-block; box-sizing: border-box; margin: 0; padding: 0; font-size: 13px; font-weight: 500;">
							<tbody>
								<tr>
									<td>
										<table style="margin: 0 4px 0 0; font-size: 13px;"><tbody><tr><td style="vertical-align: middle;"><img src="{{ asset('images/svg/phone-icon.svg') }}" alt="Phone Icon" width="13" height="14"></td></tr></tbody></table>
									</td>
									<td><a href="tel:@if (empty($signature->phone)){{ $signature->company->phone }} @else {{ $signature->phone }} @endif" style="color: rgba(52, 139, 183, 1); text-decoration: none; border: none;">@if (empty($signature->phone)){{ $signature->company->phone }}@else{{ $signature->phone }}@endif
										</a></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				@endif

				@if ($signature->has_cell === 1)
				<tr>
					<td>
						<table style="display: inline-block; box-sizing: border-box; padding: 0; font-size: 13px; font-weight: 500;">
							<tbody>
								<tr>
									<td>
										<table style="margin: 0 4px 0 0; font-size: 13px;"><tbody><tr><td style="vertical-align: middle;"><img src="{{ asset('images/svg/cell-icon.svg') }}" alt="Cell Phone Icon" width="13" height="14"></td></tr></tbody></table>
									</td>
									<td><a href="tel:+16362651460" style="color: rgba(52, 139, 183, 1); text-decoration: none; border: none;">{{ $signature->cell }}</a></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				@endif

				@if ($signature->has_fax === 1)
				<tr>
					<td>
						<table style="display: inline-block; box-sizing: border-box; padding: 0; font-size: 13px; font-weight: 500;">
							<tbody>
								<tr>
									<td>
										<table style="margin: 0 4px 0 0; font-size: 13px;"><tbody><tr><td style="vertical-align: middle;"><img src="{{ asset('images/svg/fax-icon.svg') }}" alt="Fax Icon" width="13" height="14"></td></tr></tbody></table>
									</td>
									<td>@if (empty($signature->fax))
										{{ $signature->company->fax }}
										@else
										{{ $signature->fax }}
										@endif</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				@endif

				@if ($signature->has_email === 1)
				<tr>
					<td>
						<table style="display: inline-block; box-sizing: border-box; padding: 0; font-size: 13px; font-weight: 500;">
							<tbody>
								<tr>
									<td>
										<table style="margin: 0 4px 0 0; font-size: 13px;"><tbody><tr><td style="vertical-align: middle;"><img src="{{ asset('images/svg/email-icon.svg') }}" alt="Email Icon" width="13" height="10"></td></tr></tbody></table>
									</td>
									<td><a href="mailto:{{ $signature->email }}" style="color: rgba(52, 139, 183, 1); text-decoration: none; border: none;">{{ $signature->email }}</a></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				@endif

				<tr>
					<td>
						<table style="display: inline-block; box-sizing: border-box; padding: 0; font-size: 13px; font-weight: 500;">
							<tbody>
								<tr>
									<td>
										<table style="margin: 0 4px 0 0; font-size: 13px;"><tbody><tr><td style="vertical-align: middle;" align="middle"><img src="{{ asset('images/svg/globe-icon.svg') }}" alt="Globe (Internet) Icon" width="13" height="14"></td></tr></tbody></table>
									</td>
									<td><a href="//{{ $signature->company->website }}" style="color: rgba(52, 139, 183, 1); text-decoration: none; border: none;">{{ $signature->company->website }}</a></td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</tbody>
		</table>

		<div style="flex-grow: 1;"></div>

		<fw-button design="text" state="info" size="small" icon="copy" type="button" role="button" value="copy" onclick="selectText('signature_{{ $signature->id }}'); document.execCommand('copy');" data-action="clipboard">
			Copy To Clipboard
		</fw-button>

		<form action="{{ route('signatures.destroy', ['signature' => $signature->id]) }}" method="POST" class="delete-signature-card">
			@method('DELETE')
			@csrf
			<input type="hidden" name="delete_signature_{{ $signature->id }}">
			<fw-button design="text" state="critical" size="small" icon="trash-alt" type="submit" role="buttom" value="delete" />
			</button>
		</form>
	</div>
	@endforeach
</div>
@endsection