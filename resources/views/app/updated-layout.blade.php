<!DOCTYPE html>
<html lang="en">

<head>
    @include(Brand::partial('home.meta'))
    <meta name="csrf-token" content="{{ csrf_token() }}">
    @yield('meta')
    <title>@yield('title'){{ Brand::name() }}</title>

    <link rel="shortcut icon" href="{{ asset('favicon.png') }}">
    <link rel="stylesheet" href="{{ mix('css/app.css')}}">

    @if (Route::is('ecommerce') && config('app.env' === 'production'))
        <script src="https://cdn.lr-ingest.io/LogRocket.min.js" crossorigin="anonymous"></script>
        <script>window.LogRocket && window.LogRocket.init('hzfmtb/ram-intl');</script>
    @endif

    @yield('custom-css')
    @yield('head-scripts')
</head>

<body>
    <div id="app">
        <fw-navigation
            logo="@if(Route::is(['home', config('app.prefix').'.solutions.air2post']))
            /images/common/gs-header-logo-light.png
            @else
            /images/common/gs-header-logo-dark.png
            @endif"
            theme="@if (Route::is(['home', config('app.prefix').'.solutions.air2post']))
            light
            @else
            dark
            @endif"
        >
            <template slot="links">
                @include('partials.navigation')
            </template>
        </fw-navigation>

        @if(Route::is('home'))
        <header>

            <div class="header__coverImage">
                <img src="{{ Brand::asset('images/common/%s-hero-background.jpg') }}" class="" alt="">
            </div>

            @section('hero')
            @show
        </header>
        @endif

        @section('page-hero') @show

        <main id="content">
            @if (Session::has('errors'))
                <error-alert message-bag="{{ Session::has('errors') ? json_encode(Session::get('errors')->messages()) : json_encode("{}") }}" :active="{{ Session::has('errors') ? 'true' : 'false' }}"></error-alert>
            @endif

            @if (Session::has('info'))
                <general-alert state="info" icon="info" message="{{ Session::get('info') }}" :active="true"></general-alert>
            @endif

            @if (Session::has('success'))
                <general-alert state="success" icon="thumbs-up" message="{{ Session::get('success') }}" :active="true"></general-alert>
            @endif

            @if (Session::has('warning'))
                <general-alert state="warning" icon="exclamation-triangle" message="{{ Session::get('warning') }}" :active="true"></general-alert>
            @endif

            @yield('content')
        </main>

        <footer>

            <div class="footer-content">

                <span>{{ Brand::name() }}</span>
                <span>All Rights Reserved &copy; {{ date('Y') }}</span>

            </div>

            <div class="footer-links">
                @include ('partials.footerLinks')
            </div>

        </footer>

    </div>

    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
                'user' => Auth::user()
            ]); ?>
    </script>

    <script crossorigin="anonymous" src="https://polyfill.io/v3/polyfill.min.js?features=es2015%2Ces2016%2Ces2017%2Ces5%2Ces6%2Ces7"
        async></script>
    <script src="{{ mix('js/app.js') }}" async></script>

    @yield('footer-scripts')
    @include(Brand::partial('home.tracking'))

</body>

</html>
