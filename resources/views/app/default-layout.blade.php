<!DOCTYPE html>

<html lang="en">

	<head>

		@include(Brand::partial('home.meta'))
		<meta name="csrf-token" content="{{ csrf_token() }}">

		<title>@yield('title'){{ Brand::name() }}</title>

		<link rel="shortcut icon" href="{{ asset('favicon.png') }}">
		<link rel="stylesheet" href="{{ mix('css/app.css')}}">
		@yield('custom-css')
		@yield('head-scripts')

		@if (config('app.env') === 'local')
		<script src="http://localhost:8098"></script>
		@endif

	</head>

	<body>

		<div id="app">

			<fw-navigation
				@if(!Route::is('home'))
					logo="{{ Brand::asset('images/common/%s-header-logo-light.png') }}"
				@endif

        theme="
        @if(Route::is('home'))
          light
        @else
          dark
        @endif"
			>
				<template slot="links">
					@include('partials.navigation')
				</template>
			</fw-navigation>

			<header>

				<div class="header__coverImage">
					<img src="{{ Brand::asset('images/common/%s-hero-background.jpg') }}" class="" alt="">
				</div>

				@section('hero')

				@show

			</header>

			<div id="content">

				@if (Session::has('info'))
				@component ('components.alerts.banner', [
					'state' => 'info',
					'message' => Session::get('info'),
				])
				@endcomponent
				@endif

				@if (Session::has('success'))
				@component ('components.alerts.banner', [
					'state' => 'success',
					'message' => Session::get('success'),
				])
				@endcomponent
				@endif

				@if (Session::has('error'))
				@component ('components.alerts.banner', [
					'state' => 'error',
					'message' => Session::get('error'),
				])
				@endcomponent
				@endif

				@if (! empty($errors->all()))
				@component ('components.alerts.banner', [
					'state' => 'error',
				])
				@endcomponent
				@endif

				@yield('content')

			</div>

			<footer>

				<div class="footer-content">

					<span>{{ Brand::name() }}</span>
					<span>All Rights Reserved &copy; {{ date('Y') }}</span>

				</div>

				<div class="footer-links">
					@include ('partials.footerLinks')
				</div>

			</footer>

		</div>

		<script>
			window.Laravel = <?php echo json_encode([
				'csrfToken' => csrf_token(),
				'user' => Auth::user()
			]); ?>
		</script>

		<script src="{{ mix('js/app.js') }}"></script>

		@yield('footer-scripts')

		@include(Brand::partial('home.tracking'))

	</body>

</html>
