@extends('app.updated-layout')

@section('title', 'Terms and Conditions of Service | ')

@section('content')

	<h1 class="page__title my-1">Terms and Conditions of Service</h1>

	<div class="markdown">
		<p style="width: 95%; margin-right: auto; margin-left: auto; margin-bottom: 4em; font-style: italic;">These terms and conditions of service constitute a legally binding contract between the "Company" and the "Customer". In the event the Company renders services and issues a document containing Terms and Conditions governing such services, the terms and conditions set forth in such other document(s) shall govern those services.</p>

		<h3>1. Definitions</h3>

		<ol type="a">
		<li>"Company" shall mean RAM International, its subsidiaries, related companies, agents and/or representatives;</li>

		<li>"Customer" shall mean th e person for which the Company is rendering service, as well as its principals, agents and/or representatives, including, but not limited to, shippers, importers, exporters, carriers, secured parties, warehousemen, buyers and/or sellers, shipper's agents, insurers and underwriters, break - bulk agents, consignees, etc. It is the responsibility of the Customer to provide notice and copy(s) of these terms and conditions of service to all such agents or representatives;</li>

		<li>"Documentation" shall mean all information received directly or indirectly from  Customer, whether in paper or electronic form;</li>

		<li>"Ocean Transportation Intermediaries" ("OTI") shall include an "ocean freight forwarder" and a "non - vessel operating carrier";</li>

		<li>"Third parties" shall include, but not be limited to, the following: "carriers, truckmen, cartmen, lightermen, forwarders, OTIs, customs brokers, agents, warehousemen and others to which the goods are entrusted for transportation, cartage, handling and/or delivery and/or storage or otherwise".</li>

		</ol>

		<h3>2. Company as agent</h3>

		<p style="padding-left: 1.5em;">The Company acts as the "agent" of the Customer for the purpose of performing duties in connection with the entry and release of goods, post entry services, the securing of export licenses, the filing of export and security documentation on behalf of the Customer and other dealings with government agencies, or for arranging for transportation services or other logistics services in any capacity other than as a carrier.</p>

		<h3>3. Limitation of Action</h3>

		<ol type="a">

		<li>Unless subject to a specific statute or international convention, all claims against the Company for a potential or actual loss, must be made in writing and received by the Company, within 180 days of the event giving rise to claim; the failure to give the Company timely notice shall be a complete defense to any suit or action commenced by Customer.</li>

		<li>All suits against Company must be filed and properly served on Company as follows:</li>

		<ol type="i">

			<li>For claims arising out of ocean transportation, within 180 days of the e vent giving rise to claim;</li>

			<li>For claims arising out of air transportation, within 180 days of the event giving rise to claim;</li>

			<li>For claims arising out of the preparation and/or submission of an import customs entry(s), within 180 days from the date of liquidation of the entry(s);</li>

			<li>For any and all other claims of any other type, within 180 days from the date of the loss or damage.</li>

		</ol>

		</ol>

		<h3>4. No Liability for Selection or Services of Third Parties and/or Routes</h3>

		<p style="padding-left: 1.5em;">Unless services are performed by persons or firms engaged pursuant to express written instructions from the Customer, Company shall use reasonable care in its selection of third parties, or in selecting the means, route and procedure to be followed in the handling, transportation, clearance an d delivery of the shipment; advice by the Company that a particular person or firm has been selected to render services with respect to the goods, shall not be construed to mean that the Company warrants or represents that such person or firm will render such services nor does Company assume responsibility or liability for any actions(s) and/or inaction(s) of such third parties and/or its agents, and shall not be liable for any delay or loss of any kind, which occurs while a shipment is in the custody or control of a third party or the agent of a third party; all claims in connection with the Act of a third party shall be brought solely against such party and/or its agents; in connection with any such claim, the Company shall reasonably cooperate with the Customer, which shall be liable for any charges or costs incurred by the Company.</p>

		<h3>5. Quotations Not Binding</h3>

		<p style="padding-left: 1.5em;">Quotations as to fees, rates of duty, freight charges, insurance premiums or other charges given by the Company to the Customer are for informational purposes only and are subject to change without notice; no quotation shall be binding upon the Company unless the Company in writing agrees to undertake the handling or transportation of the shipment at a specific rate or amount set forth in the quotation and payment arrangements are agreed to between the Company and the Customer.</p>

		<h3>6. Reliance On Information Furnished</h3>

		<ol type="a">

		<li>Customer acknowledges that it is required to review all documents and declarations prepared and/or filed with U.S. Customs &amp; Border Protect ion, other government agency (s) and/or third parties, and will immediately advise the Company of any errors, discrepancies, incorrect statements, or omissions on any declaration or other submission filed on Customer’s behalf;</li>

		<li>In preparing and submitting customs entries, export declarations, applications, security filings, documentation and/or other required data, the Company relies on the correctness of all documentation, whether in written or electronic format, and all information furnished by Customer; Customer shall use reasonable care to ensure the correctness of all such information and shall indemnify and hold the Company harmless from any and all claims asserted and/or liability or losses suffered by reason of the Customer's failure to disclose information or any incorrect, incomplete or false statement by the Customer or its agent, representative or contractor upon which the Company reasonably relied. The Customer agrees that the Customer has an affirmative non - delegable duty to disclose any and all information required to import, export or enter the goods.</li>

		<li>Customer acknowledges that it is required to provide verified weights obtained on calibrated, certified equipment of all cargo that is to be tendered to steamship lines and represents that Company is entitled to rely on the accuracy of such weights and to counter - sign or endorse it as agent of Customer in order to provide the certified weight to the steamship lines. The Customer agrees that it shall indemnify and hold the Co mpany harmless from any and all claims, losses, penalties or other costs resulting from any incorrect or questionable statements of the weight provided by the Customer or its agent or contractor on which the Company relies.</li>

		</ol>

		<h3>7. Declaring Higher Value to Third Parties</h3>

		<p style="padding-left: 1.5rem;">Third parties to whom the goods are entrusted may limit liability for loss or damage; the Company will request excess valuation coverage only upon specific written instructions from the Customer, which must agree to pay any charges therefore; in the absence of written instructions or the refusal of the third party to agree to a higher declared value, at Company's discretion, the goods may be tendered to the third party, subject to the terms of the third party's limitations of liability and/or terms and conditions of service.</p>

		<h3>8. Insurance</h3>

		<p style="padding-left: 1.5em;">Unless requested to do so in writing and confirmed to Customer in writing, Company is under no obligation to procure insurance on Customer's behalf; in all cases, Customer shall pay all premiums and costs in connection with procuring requested insurance.</p>

		<h3>9. Disclaimers; Limitation of Liability</h3>

		<ol type="a">

		<li>Except as specifically set forth herein, Company makes no express or implied warranties in connection with its services;</li>

		<li>In connection with all services performed by the Company, Customer may obtain additional liability coverage, up to the actual or declared value of the shipment or transaction, by requesting such coverage and agreeing to make payment therefor, which request must be confirmed in writing by the Company prior to rendering services for the covered transaction(s).</li>

		<li>In the absence of additional coverage under (b) above, the Company's liability shall be limited to the following:</li>

			<ol type="i">

			<li>where the claim arises from activities other than those relating to customs business, $500.00 per shipment or transaction, or</li>

			<li>where the claim arises from activities relating to "Customs business," the amount of $50.00 or the brokerage fees paid to Company for the entry, whichever is less;</li>

			</ol>

		<li>In no event shall Company be liable or responsible for consequential, indirect, incidental, statutory or punitive damages, even if it has been put on notice of the possibility of such damages, or for the acts of third parties.</li>

		</ol>

		<h3>10. Advancing Money</h3>

		<p style="padding-left: 1.5em;">All charges must be paid by Customer in advance unless the Company agrees in writing to extend credit to customer; the granting of credit to a Customer in connection with a particular transaction shall not be considered a waiver of this provision by the Company.</p>

		<h3>11. Indemnification/Hold Harmless</h3>

		<p style="padding-left: 1.5em;">The Customer agrees to indemnify, defend, and hold the Company harmless from any claims and/or liability, fines, penalties and/or attorneys' fees arising from the importation or exportation of customers merchandise and/or any conduct of the Customer, including but not limited to the inaccuracy of entry, export or security data supplied by Customer or its agent or representative, which violates any Federal, State and/or other laws, and further agrees to indemnify and hold the Company harmless against any and all liability, loss, damages, costs, claims, penalties, fines and/or expenses, including but not limited to reasonable attorney's fees, which the Company may hereafter incur, suffer or be required to pay by reason of such claims; in the event that any claim, suit or proceeding is brought against the Company, it shall give notice in writing to the Customer by mail at its address on file with the Company.</p>

		<h3>12. C.O.D. or Cash Collect Shipments </h3>

		<p style="padding-left: 1.5em;">Company shall use reasonable care regarding written instructions relating to "Cash/Collect on Deliver (C.O.D.)" shipments, bank drafts, cashier's and/or certified checks, letter(s) of credit and other similar payment documents and/or instructions regarding collection of monies but shall not have liability if the bank or consignee refuses to pay for the shipment.</p>

		<h3>13. Costs of Collection</h3>

		<p style="padding-left: 1.5em;">In any dispute involving monies owed to Company, the Company shall be entitled to all costs of collection, including reasonable attorney's fees and interest at 18% per annum or the highest rate allowed by law, whichever is less unless a lower amount is agreed to by Company.</p>

		<h3>14. General Lien and Right To Sell Customer's Property </h3>

		<ol type="a">

		<li>Company shall have a continuing lien on any and all property and documents relating thereto of Customer coming into Company's actual or constructive possession, custody or control or en route, which lien shall survive delivery, for all charges, expenses or advances owed to Company with regard to the shipment on which the lien is claimed, a prior shipment(s) and/or both. Customs duties, transportation charges, and related payments advanced by the Company shall be deemed paid in trust on behalf of the Customer and treated a s pass through payments made on behalf of the Customer for which the Company is acting as a mere conduit.</li>

		<li>Company shall provide written notice to Customer of its intent to exercise such lien, the exact amount of monies due and owing, as well as any on - go ing storage or other charges; Customer shall notify all parties having an interest in its shipment(s) of Company's rights and/or the exercise of such lien.</li>

		<li>Unless, within thirty days of receiving notice of lien, Customer posts cash or letter of credit at sight, or, if the amount due is in dispute, an acceptable bond equal to 110% of the value of the total amount due, in favor of Company, guaranteeing payment of the monies owed, plus all storage charges accrued or to be accrued, Company shall have the right to sell such shipment(s) at public or private sale or auction and any net proceeds remaining thereafter shall be refunded to Customer.</li>

		</ol>

		<h3>15. No Duty To Maintain Records For Customer</h3>

		<p style="padding-left: 1.5em;">Customer acknowledges that pursuant to Sections 508 and 509 of the Tariff Act, as amended, (19 USC §1508 and 1509) , it has the duty and is solely liable for maintaining all records required under the Customs and/or other Laws and Regulations of the United States; unless otherwise agreed to in writing, the Company shall only keep such records that it is required to maintain by Statute(s) and/or Regulation(s), but not act as a "record - keeper" or "record - keeping agent" for Customer.</p>

		<h3>16. Obtaining Binding Rulings, Filing Protests, etc</h3>

		<p style="padding-left: 1.5em;">Unless requested by Customer in writing and agreed to by Company in writing, Company shall be under no obligation to undertake any pre - or post - Customs release action, including, but not limited to, obtaining binding rulings, advising of liquidations, filing of petition(s) and/or protests, etc.</p>

		<h3>17. No Duty to Provide Licensing Authority.</h3>

		<p style="padding-left: 1.5em;">Unless requested by Customer in writing and agreed to by the Company in writing, Company shall not be responsible for determining licensing authority or obtaining any license or other authority pertaining to the export fro m or import into the United States.</p>

		<h3>18. Preparation and Issuance of Bills of Lading</h3>

		<p style="padding-left: 1.5em;">Where Company prepares and/or issues a bill of lading, Company shall be under no obligation to specify thereon the number of pieces, packages and/or cartons, etc.; unless specifically requested to do so in writing by Customer or its agent and Customer agrees to pay for same, Company shall rely upon and use the cargo weight supplied by Customer.</p>

		<h3>19. No Modification or Amendment Unless Written </h3>

		<p style="padding-left: 1.5em;">These terms and conditions of service may only be modified, altered or amended in writing signed by both Customer and Company; any attempt to unilaterally modify, alter or amend same shall be null and void.</p>

		<!-- <h3>19. No Duty To Provide Licensing Authority </h3>

		<p style="padding-left: 1.5em;">Unless requested by Customer in writing and agreed to by the Company in writing, Company shall not be responsible for determining licensing authority or obtaining any license or other authority pertaining to the export from or import into the United States.</p> -->

		<h3>20. Compensation of Company</h3>

		<p style="padding-left: 1.5em;">The compensation of the Company for its services shall be included with and is in addition to the rates and charges of all carriers and other agencies selected by the Company to transport and deal with the goods and such compensation shall be exclusive of any brokerage, commissions, dividends, or other revenue received by the Company from carriers, insurers and others in connection with the shipment. On ocean exports, upon request, the Company shall provide a detailed breakout of the components of all charges assessed and a true copy of each pertinent document relating to these charges. In any referral for collection or action against the Customer for monies due the Company, upon recovery by the Company, the Customer shall pay the expenses of collection and/or litigation, including a reasonable attorney fee.</p>

		<h3>21. Force Majeure</h3>

		<p style="padding-left: 1.5em;">Company shall not be liable for losses, damages, delays, wrongful or missed deliveries or nonperformance, in whole or in part, of its responsibilities under the Agreement, resulting from circumstances beyond the control of either Company or its sub - contractors, including but not limited to:</p>

		<ol type="a">

			<li>acts of God, including flood , earthquake, storm, hurricane, power failure or other natural disaster;</li>

			<li>war, hijacking, robbery, theft or terrorist activities;</li>

			<li>incidents or deteriorations to means of transportation,</li>

			<li>embargoes,</li>

			<li>civil commotions or riots,</li>

			<li>defects, nature or inherent vice of the goods;</li>

			<li>acts, breaches of contract or omissions by Customer, Shipper, Consignee or anyone else who may have an interest in the shipment,</li>

			<li>acts by any government or any agency or subdivision thereof, including den ial or cancellation of any import/export or other necessary license; or</li>

			<li>strikes, lockouts or other labor conflicts.</li>

		</ol>

		<h3>22. Severability </h3>

		<p style="padding-left: 1.5em;">In the event any Paragraph(s) and/or portion(s) hereof is found to be invalid and/or un enforceable, the remainder hereof shall remain in Full force and effect. Company's decision to waive any provision herein, either by conduct or otherwise, shall not be deemed to be a further or continuing waiver of such provision or to otherwise waive or invalidate any other provision herein.</p>

		<h3>23. Governing Law; Consent to Jurisdiction and Venue</h3>

		<p style="padding-left: 1.5em;">These terms and conditions of service and the relationship of the parties shall be construed according to the laws of the State of Missouri without giving consideration to principles of conflict of law.</p>

		<p style="padding-left: 1.5em;">Customer and Company</p>

		<ol type="a">

		<li>irrevocably consent to the jurisdiction of the United States District Court and the State courts of Missouri;</li>

		<li>agree that any action relating to the services performed by Company, shall only be brought in said courts;</li>

		<li>consent to the exercise of in personam jurisdiction by said courts over it, and</li>

		<li>further agree that any action to enforce a judgment may be instituted in any jurisdiction.</li>

		</ol>

		<p style="margin-top:	 3rem; font-wieght: bold;">&copy; Approved by the National Customs Brokers and Forwarders Association of America, Inc. (Revised 6/16). Edited and adapted for use by RAM International, a Flat World Company.</p>
	</div>

@endsection
