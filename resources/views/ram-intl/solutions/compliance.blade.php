@extends('app.updated-layout')
@section('title', 'Customs & Compliance | ')
@section('content')

<h1 class="page__title my-1">Customs Compliance Projects</h1>

<h3 class="page__subTitle my-1">Importers and Exporters working with Customs (CBP) and Participating Government Agencies (PGAs) must insure that they are
    complying with a myriad of regulations.</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>You need to ship your goods</h3>
        <h3 class="em">We can Help</h3>

        <img src="{{ Brand::asset('images/chainlink-1-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<h3 class="mt-5">Customs Compliance Projects</h3>

<p class="text dark paragraph general">We are able to perform compliance assessments, help create Import and Export Compliance Management programs and manuals to
    help support your business, as well as assist with short-term compliance projects. <a href="{{ route('contact.index') }}"
        class="link">Contact us</a> for further details!</p>

<h3 class="mt-5">Licensed Customs Broker</h3>

<p class="text dark paragraph general">Our St. Louis and Chicago offices have served as a Licensed Customs Broker since 2000.</p>

<p class="text dark paragraph general">Long-term relationships with carriers allow us to provide the flexibility required to successfully manage
    a shipper's unexpected volume increases.</p>

<h3 class="mt-5">Bond Filing</h3>

<p class="text dark paragraph general">Whether it takes a single entry bond or filing a continuous entry bond, we make sure our clients have the bond that best
    fits their needs.</p>

<h3 class="mt-5">ACE Entry Filer</h3>

<p class="text dark paragraph general">We are actively filing entries in the Automated Commercial Environment (ACE). ACE is the commercial trade processing
    system developed by U.S. Customs and Border Protection (CBP) to facilitate trade and strengthen border security. It incorporates
    automation, providing accurate shipment information and enabling improved visibility for brokers, importers and exporters.</p>

<h3 class="mt-5">Customs Clearance</h3>

<p class="text dark paragraph general">Because of RAM Int'l, we pay attention to all the details and utilize our experience, regulatory expertise and the latest technology to
    ensure that your supply chain remains compliant. Our brokerage team participates in customs automated programs and we
    extend this technological expertise to you. We will work with you to make sure that compliance is the key to your operational success.</p>

<h3 class="mt-5">Customs Examination Station / General Order Warehouse</h3>

<p class="text dark paragraph general">Our headquarters in St. Louis also serve as the Customs Exam Site (Customs Examination Station) as well as
    the General Order Warehouse for the St. Louis U.S. Customs District.</p>
@endsection
