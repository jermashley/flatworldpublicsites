@extends('app.updated-layout')
@section('title', 'Temperature Controlled Cargo | ')
@section('content')


<h1 class="page__title my-1">Temperature Controlled Cargo</h1>

<h3 class="page__subTitle my-1">Get your investemt to market safely and in the best condition</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>You need to ship your goods</h3>
        <h3 class="em">We can Help</h3>

        <img src="{{ Brand::asset('images/refrigerated-containers-1-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<h3 class="mt-5">Perishable Cargo</h3>

<p class="text dark paragraph general">You invest millions of dollars developing your products to take to market and you need a broad range of passive and active
    temperature-controlled shipping options to meet your needs. We offers extensive temperature controlled
    shipping solutions. Whether you're looking to move frozen field study crop samples or high-value pharmaceuticals, your
    compliance staff can rely on our shipping experts to help coordinate your shipments. Our team understands the importance of
    our shipping process as related to your study protocols, USDA, FDA and GLP requirements.</p>

<p class="text dark paragraph general">We continue to invest in our US facilities to support this area of business. In December of 2017, we added
    2-8 C chilled rooms to our Atlanta and St. Louis facilities. We also added a controlled ambient temperature room in St.
    Louis. We maintain an inventory of validated shipping containers in our Atlanta, Dallas, Chicago and
    St. Louis facilities to support the shipping of frozen samples on dry ice.</p>

<p class="text dark paragraph general">Our temperature controlled expertise includes:</p>

<ul class="generalList styled">
    <li>Diverse range of customer relationships offers broad industry knowledge for your business</li>
    <li>A single point of contact for all your temperature controlled shipments</li>
    <li>Global visibility to track and trace your shipments via online access through our CargoWise platform</li>
    <li>24/7 personal commitment to your shipment</li>
</ul>

<p class="text dark paragraph general">Just a few of the areas where we have demonstrated a high level of performance and reliability to for our clients:</p>

<ul class="generalList styled">
    <li>Pharma from EU throughout the USA and USA to EU in both active and passive cooling units</li>
    <li>Frozen and ambient temp crop samples from South and Central America to USA laboratories</li>
    <li>Frozen and ambient temp crop samples from South and Central America to EU laboratories</li>
    <li>Frozen and ambient temp crop samples from North America to EU laboratories</li>
    <li>Frozen banana field study samples from the Philippines to US laboratories</li>
    <li>Download and distribution of returned Temp Tale data as called for in client protocols</li>
</ul>
@endsection
