@extends('app.updated-layout')
@section('title', 'E-Commerce / ASM | ')

@section('content')
<h1 class="page__title my-1">E-Commerce / ASM</h1>

<h3 class="page__subTitle my-1">We are a full-service global Freight Forwarder and Customs Broker, equipped to handle your import or export
    needs.
</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3 class="mt-5 mb-1" style="padding: 0 0 0 12px;">You need to ship your goods</h3>
        <h3 class="em">We can Help</h3>

        <img src="{{ Brand::asset('images/boxes-laptop-1-2048.jpg' ) }}" class="imageBg cover center" alt="">

    </div>
</div>

<h2>Request an E-Commerce Quote</h2>

<p class="text dark paragraph general">To our e-commerce clients – we understand finding the right solution for your transportation can be overwhelming and intimidating.
    We are here to put this part of your business at ease.</p>

<p class="text dark paragraph general">We have created an Importation Guide (attached below, as a PDF file) that will guide you and answer your questions regarding
    the importing process / terminology.</p>

<fw-link href="#ecommerceQuote" design="outline" state="info" size="small" icon="edit">
    Fill Out E-Commerce Quote Form
</fw-link>

<div class="content__featureWall">

    <header class="featureWall__header">

        <span>Services We Provide</span>
        <span></span>

    </header>

    <div class="featureWall__blockFull">

        <ul class="generalList styled">
            <li>Importing / Exporting via Air and Ocean Worldwide</li>
            <li>Custom Brokerage</li>
            <li>Warehousing</li>
            <li>Labeling</li>
            <li>Exterior Inspection</li>
            <li>Limited Interior Inspection</li>
            <li>Domestic LTL</li>
        </ul>

    </div>

    <div class="featureWall__blockFull">

        <h3>To ensure that we are providing accurate pricing, we require the following information, which you will enter below:</h3>

        <ul class="generalList styled">
            <li>Origin Pick-up</li>
            <li>Final Destination – one of our facilities located in Chicago, St Louis, Atlanta, or Dallas</li>
            <li>Total number of pieces/cartons (Supplier can advise)</li>
            <li>Dimensions of each carton (Supplier can advise)</li>
            <li>Weight per carton (Supplier can advise)</li>
            <li>Terms of Sale / Incoterm (i.e. FOB, EXW)</li>
        </ul>

    </div>

    <div class="featureWall__blockFull">

        <h3>Please see the our Importation Guide (attached below) for more information about the international
            transportation process.</h3>

    </div>

</div>

<div class="docDownloads">

    <h2 class="w-100 mb-4">Document Downloads</h2>

    {{-- Flat World Global Solutions Importation Guide --}}
    <fw-card-document file-icon="file-pdf" file-name="RAM Int'l Importation Guide" file-size="260 KB" file-type="Adobe PDF" href="/downloads/ram-import-checklist.pdf">
    </fw-card-document>

</div>

<p class="text dark paragraph general">Please be sure to look over our <a href="{{ route(config('app.prefix').'.terms') }}" target="_blank" class="link">Terms &amp; Conditions</a>    before filling out the form below.</p>

<div class="content__featureWall" id="ecommerceQuote">

    <header class="featureWall__header">

        <span>Get an E-Commerce Quote</span>
        <span></span>

    </header>

    <div class="featureWall__blockFull">
        @php
        $now = new DateTime();
        $ChineseNewYearEnd = new DateTime('2020-02-07 23:59:59');
        @endphp

        @if ($now <= $ChineseNewYearEnd)
        <div style="margin: 0 auto 2rem; padding: 0.8125rem 1.75rem; background-color: hsla(355, 45%, 97%, 1); border: 1px solid hsla(355, 65%, 85%, 1); border-radius: 0.5rem;">
            <p class="text dark paragraph general" style="font-size: 1.125rem; font-weight: 500; line-height: 125%;">
                Due to the Chinese New Year holiday, rates for shipping are fluctuating greatly at this time.  We are
                able to
                provide our quotes based on the current rates, but the rates will need to be checked again after the
                holiday, closer to
                the time of shipping to ensure accuracy.
            </p>

            <p class="text dark paragraph general" style="font-size: 1.125rem; font-weight: 500; line-height: 125%;">We recommend requesting a quote mid-February to ensure that the rates
                are up to date.</p>

            <p class="text dark paragraph general" style="font-size: 1.125rem; font-weight: 500; line-height: 125%;">If you have any questions please email John Eagen at <a href="mailto:jeagen@flatworldgs.com" class="link">jeagen@flatworldgs.com</a>.</p>
        </div>
        @endif

        <e-commerce-form name="ramQuoteForm" action="{{ route('eCommerceForm') }}" method="POST">
            <template v-slot:hidden-fields>
                @csrf
                @method('POST')
            </template>
        </e-commerce-form>
    </div>
</div>
@endsection

@section('footer-scripts')
<script src="{{ mix('js/_eCommerceQuoteForm.js') }}"></script>
@endsection
