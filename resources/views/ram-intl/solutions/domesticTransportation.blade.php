@extends('app.updated-layout')
@section('title', 'Domestic Transportation | ')
@section('content')




<h1 class="page__title my-1">Domestic Transportation</h1>

<h3 class="page__subTitle my-1">Domestic transportation is a key element for almost every international shipment. We offers a full menu of services for LTL, TL, expedited, and intermodal transport. We also offer comprehensive Transportation Management and 3rd Party Logistics solutions through our <a href="https://flatworldsc.com/" target="_blank">Domestic Transportation Division</a>.</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>You need to ship your goods</h3>
        <h3 class="em">We can Help</h3>

        <img src="{{ Brand::asset('images/truck-highway-2-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<h3 class="mt-5">LTL (Less Than Truckload)</h3>

<p class="text dark paragraph general">Choice is critical to a cost effective, yet service sensitive supply chain. Utilizing a menu of LTL carriers to attain optimum
    service performance, leveraging volume to ensure competitive prices, and optimizing multiple base rates are all ways
    to better utilize a company's resources. Because your time is valuable, our personal service with dedicated account management,
    along with state of the art technology offers a more efficient way to manage your LTL transportation dollar. Only "best
    in class" National, Inter-regional, and Regional providers are utilized, so you can be confident in the value your company
    will receive from us. We select the best service-providing carriers in the industry for each geographic
    area and combine that with the optimum price for the service required to provide for the greatest value proposition in
    the market.</p>

<h3 class="mt-5">TL (Truck Load)</h3>

<p class="text dark paragraph general">We provide one of the easiest, most cost-effective and dependable full truckload shipping solutions in the
    industry. Unlike asset-based providers, where the main priority is the utilization of their own company's assets, our main objective is to provide clients with the optimum combination of capacity, price, and ease
    of engagement. We help to remove the hassle of obtaining transportation services for your full truckload and inter-modal
    shipments.</p>

<p class="text dark paragraph general">Long-term relationships with carriers allow us to provide the flexibility required to successfully manage
    a shipper's unexpected volume increases.</p>

<h3 class="mt-5">Expedited</h3>

<p class="text dark paragraph general">Through RAM Int'l, Flat World Global Solutions provides premium expedited shipping services, including ground and air freight forwarding services. We
    also offer dedicated door-to-door premium freight services to and from any location in North America. Our technology,
    network strength, and world class customer service provided through our easy to use web-based quoting, booking, and satellite
    tracking platform provides real-time visibility throughout your supply-chain. Regardless of the mode, service or pick-up
    and delivery locations, we are confident in our ability to provide the most cost effective supply-chain solutions for
    your time sensitive and high value freight.</p>
@endsection
