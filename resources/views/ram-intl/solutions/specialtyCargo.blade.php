@extends('app.updated-layout')
@section('title', 'Specialty Cargo | ')
@section('content')




<h1 class="page__title my-1">Specialty Cargo</h1>

<h3 class="page__subTitle my-1">Our experienced team of professionals have experience in making sure your specialty cargo gets special attention.</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>You need to ship your goods</h3>
        <h3 class="em">We can Help</h3>

        <img src="{{ Brand::asset('images/hazardous-1-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<h3 class="mt-5">Perishable Cargo</h3>

<p class="text dark paragraph general">A service that combines Flat World’s long-standing experience in ground handling, customs clearance and air freight to offer a proven, end-to-end service that ensures the safe transport of perishables in line with strict regulations. We offer flexible air capacity out of multiple gateways, pre and on-carriage temperature control, as well as specialty packaging abilities. From live fish to frozen products, our team has the experience to handle the most difficult of perishables.</p>

<h3 class="mt-5">Haz Mat</h3>

<p class="text dark paragraph general">Since our inception in 1980, our experience in the handling of Hazardous materials for air and ocean are second to none.
    Our experienced staff ensures dthat your Hazardous materials shipments are handled with care and comply with all regulations
    that may apply.</p>

<h3 class="mt-5">Oversize Cargo</h3>

<p class="text dark paragraph general">What ever the shipment is, rest assured it is not to big for our team to handle. Whether your shipment requires
    cranes at the port Ro-Ro service or a need to Charter a plane, we can make it happen.</p>
@endsection
