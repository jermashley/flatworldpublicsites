@extends('app.updated-layout')
@section('title', 'Warehousing & Distribution | ')
@section('content')


<h1 class="page__title my-1">Warehousing &amp; Distribution</h1>

<h3 class="page__subTitle my-1">Domestic transportation is a key element for almost every international shipment. We offer a full menu of services for LTL, TL, Expedited and Intermodal transport. We also offer comprehensive Transportation Management and 3rd Party Logistics solutions through our Domestic Transportation division.</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>You need to ship your goods</h3>
        <h3 class="em">We can Help</h3>

        <img src="{{ Brand::asset('images/warehouse-glow-2-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<h3 class="mt-5">Consolidation and De-consolidation Services</h3>

<p class="text dark paragraph general">Are you bringing in products from multiple vendors or locations for export? Or do you have an import shipment that require
    multiple shipments in the same container going to different consignees? Let us take care of your consolidation
    and deconsolidation needs in one of our strategic US warehouse locations.</p>

<h3 class="mt-5">Government Warehouse</h3>

<p class="text dark paragraph general">We are a trusted partner of U.S. Customs. Because of the attention to detail and compliance our team provides, we serves as
    the Customs Exam Site, General Order Warehouse, and Customs Bonded Container Freight Station for the St. Louis Customs
    District.</p>

<h3 class="mt-5">Pick and Pack Services</h3>

<p class="text dark paragraph general">We can seamlessly serve as an extension of our client by providing pick and pack services at our facilities.
    Visibility is made easy 24/7 through our technology that gives you access to inventory counts, in-bound and out-bound
    shipments, or any custom needs our clients might have.</p>
@endsection
