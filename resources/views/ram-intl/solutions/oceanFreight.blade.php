@extends('app.updated-layout')
@section('title', 'Ocean Freight | ')
@section('content')




<h1 class="page__title my-1">Ocean Freight</h1>

<h3 class="page__subTitle my-1">We offer a full array of ocean freight services and is a licensed non-vessel operating common carrier (NVOCC).</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>You need to ship your goods</h3>
        <h3 class="em">We can Help</h3>

        <img src="{{ Brand::asset('images/%s/cargo_ship-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<h3 class="mt-5">LCL (Less Than Container Load)</h3>

<p class="text dark paragraph general">We are a leading Less-than Container Load (LCL) service provider. LCL offers the possibility of consolidating multiple consignments
    from multiple customers in one Full Container Load (FCL). Customers can ship low volumes without having the cost commitments
    of a full container. Our customers benefit from an optimized logistics spend, more flexibility and seamless door-to-door
    services with the highest level of schedule integrity and reliability in transit times.</p>

<h3 class="mt-5">FCL (Full Container Load)</h3>

<p class="text dark paragraph general">If you are a supplier shipping large volumes of products, we offer Full Container Load (FCL) service. Using
    our years of experience with the world’s top vessel operators, we can adapt quickly to secure the capacity required to
    keep your supply chain running at optimal efficiency. We also offers specialized FCL services, including Refer, Break
    Bulk, and Ro-Ro services.</p>
@endsection
