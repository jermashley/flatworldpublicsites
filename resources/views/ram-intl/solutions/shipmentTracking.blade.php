@extends('app.updated-layout') 
@section('title', "Int'l Tracking & Visibility | ") 
@section('content')




<h1 class="page__title my-1">International Tracking &amp; Visibility</h1>

<h3 class="page__subTitle my-1">Shipment and cargo visibility is one of the most important aspects of our industry. We offer one of the most complete visibility
    and tracking systems allowing you to get the most up-to-date status of your shipments.</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>We help you track your shipments - seamlessly and intuitively</h3>
        <h3 class="em">Get the most up-to-date status</h3>

        <img src="{{ Brand::asset('images/touching-screen-1-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>


<div>

    <p>NEEDS COPY</p>

</div>

<div class="content__featureWall">

    <div class="featureWall__header">

        <span>Overview</span>
        <span></span>

    </div>

    <div class="featureWall__blockHalf">

        <p>NEEDS COPY</p>

    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image" data-featherlight="{{ asset('images/intl-tracking/tracking-interface.png' ) }}">

            <img src="{{ asset('images/intl-tracking/tracking-interface.png' ) }}" alt="">

        </div>
    </div>

</div>

<div class="content__featureWall">

    <div class="featureWall__header">

        <span>The Tracking Card</span>
        <span></span>

    </div>

    <div class="featureWall__blockHalf">

        <p>NEEDS COPY</p>

    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image" data-featherlight="{{ asset('images/intl-tracking/tracking-card-single.png' ) }}">

            <img src="{{ asset('images/intl-tracking/tracking-card-single.png' ) }}" alt="">

        </div>
    </div>

</div>

<div class="content__featureWall">

    <div class="featureWall__header">

        <span>Tracking the Progress</span>
        <span></span>

    </div>

    <div class="featureWall__blockHalf">

        <p>NEEDS COPY</p>

    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image" data-featherlight="{{ asset('images/intl-tracking/tracking-card-progress.png' ) }}">

            <img src="{{ asset('images/intl-tracking/tracking-card-progress.png' ) }}" alt="">

        </div>
    </div>

</div>
@endsection