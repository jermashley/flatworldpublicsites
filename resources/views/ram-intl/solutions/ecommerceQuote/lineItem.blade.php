<div class="lineItem" id="lineItem">

    {{-- <h3 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Line Item</h3> --}}

    <div class="form-group">
        <label for="numberOfCartons" @if ($errors->has('typeOfBusiness')) class="error" @endif>
            <span class="label-text">Number of Cartons</span>
            <input type="number" min="0" name="numberOfCartons[]" value="" placeholder="0" required>
        </label>

        <label for="cartonType" @if ($errors->has('typeOfBusiness')) class="error" @endif>
            <span class="label-text">Carton Type</span>

            <select name="cartonType[]" class="cartonType" required>
                <option disabled="" selected="" value="null">Pick One</option>
                <option class="typeCrate" value="Carton" data-carton="typeCrate">Carton / Crate</option>
                <option class="typePallet" value="Pallet" data-carton="typePallet">Pallets</option>
            </select>
        </label>
    </div>

    <div class="cartonCrate">

        {{-- Crate Spec Fields Go Here --}}

    </div>

    <div class="cartonPallet">

        {{-- Pallet Spec Fields Go Here --}}

    </div>

    <div class="form-group">
        <label for="commodityValue" @if ($errors->has('typeOfBusiness')) class="error" @endif>
            <span class="label-text">Value</span>
            <input type="number" min="0" name="commodityValue[]" value="" placeholder="$100.00" required>
        </label>
        <label for="commodityDescription" @if ($errors->has('typeOfBusiness')) class="error" @endif>
            <div class="label-text">Commodity Description</div>
            <input type="text" name="commodityDescription[]" value="" placeholder="Ex. Baseball Bats" required>
        </label>
    </div>

    <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">

        {{-- <fw-button design="outline" state="critical" size="small" icon="trash-alt" type="button" role="button" value="delete" data-action="delete">
            Delete Line
        </fw-button> --}}

        <button type="button" class="button button-outline-critical button-small" data-action="delete">
                <fw-font-awesome-icon icon="trash-alt"></fw-font-awesome-icon>
            {{-- <i class="fas fa-trash-alt fa-fw" style="margin: 0 .5rem 0 0;"></i> --}}
            Delete Line
        </button>

    </div>

</div>
