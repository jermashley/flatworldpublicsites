<div class="form-group">
    <label for="palletType" @if ($errors->has('typeOfBusiness')) class="error" @endif>
        <span class="label-text">Pallet Type</span>
        <select name="palletType[]" required>
            <option disabled="" selected="" value="">--</option>
            <option value="48 x 40">48" x 40"</option>
            <option value="120CM x 80 CM">120CM x 80CM (Euro 1)</option>
            <option value="12CM x 100 CM">12CM x 100CM (Euro 2)</option>
        </select>
    </label>
</div>

<div class="form-group">
    <fieldset @if ($errors->has('typeOfBusiness')) class="error" @endif>
        <label>
            <span class="label-text">Weight per Carton</span>
        </label>

        <label for="palletWeight" class="pallet-specs">
            <input type="text" name="palletWeight[]" placeholder="1,234" required>
            <select name="palletWeightUnit[]" required>
                <option disabled="" selected="" value="">--</option>
                <option value="LBs">LBs</option>
                <option value="KGs">KGs</option>
            </select>
        </label>
    </fieldset>
</div>
