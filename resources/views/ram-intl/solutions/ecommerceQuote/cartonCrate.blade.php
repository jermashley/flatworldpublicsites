<div class="form-group">
    <fieldset @if ($errors->has('typeOfBusiness')) class="error" @endif>
        <label>
            <span class="label-text">Carton Dimensions</span>
        </label>

        <label for="crateDims" class="crate-specs">
            <input name="crateDimLength[]" type="text" id="length" placeholder="L" required>
            <input name="crateDimWidth[]" type="text" id="width" placeholder="W" required>
            <input name="crateDimHeight[]" type="text" id="height" placeholder="H" required>
            <select name="crateDimUnit[]" required>
                <option disabled="" selected="" value="">--</option>
                <option value="INs">INs</option>
                <option value="CMs">CMs</option>
            </select>
        </label>
    </fieldset>
</div>

<div class="form-group">
    <fieldset @if ($errors->has('typeOfBusiness')) class="error" @endif>
        <label>
            <span class="label-text">Carton Weight</span>
        </label>
        <label for="crateWeight" class="crate-specs">
            <input name="crateWeightNumber[]" type="text" placeholder="1,234" required>
            <select name="crateWeightUnit[]" required>
                <option disabled="" selected="" value="">--</option>
                <option value="LBs">LBs</option>
                <option value="KGs">KGs</option>
            </select>
        </label>
    </fieldset>
</div>
