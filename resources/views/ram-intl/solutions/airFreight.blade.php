@extends('app.updated-layout')
@section('title', 'Air Freight | ')
@section('content')

<h1 class="page__title my-1">Air Freight</h1>

<h3 class="page__subTitle my-1">We have been an IATA licensed Freight Forwarder in the United States for 34 years.</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>You need to ship your goods</h3>
        <h3 class="em">We can Help</h3>

        <img src="{{ Brand::asset('images/airplane-tarmac-1-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<p class="text dark paragraph general mt-5">Through RAM Int'l, Flat World Global Solutions has built a reputation for handling oversized and specialty airfreight shipments for some of the most high profile clients
    in the world. We also offers unique expertise in Haz-Mat shipping. Our door-to-door service handles your cargo from
    pickup, assembly and consolidation, to tracking, delivery and billing.</p>

<p class="text dark paragraph general">Our experienced team collaborates to process export documentation and track shipments from door-to-door. Our systems ensure
    compliance with government regulations with connections to customs agencies around the world.</p>

<p class="text dark paragraph general">We are a C-TPAT approved forwarder and we continually train our staff on best security practices. Our facilities also meet
    the stringent physical security requirements of agencies like the US Transportation Security Administration.</p>
@endsection
