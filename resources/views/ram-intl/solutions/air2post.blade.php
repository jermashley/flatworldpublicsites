@section('head-scripts')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    <script>
    function onSubmit(token) {
        document.getElementById("contactForm").submit();
    }
    </script>
@endsection

@php
    $features = [
      [
        'icon' => 'clipboard-list',
        'text' => 'Label & Shipment Manifest via API',
        'color' => '#E05C69'
      ],
      [
        'icon' => 'laptop',
        'text' => 'Air Freight Shipment Management',
        'color' => '#0082C8'
      ],
      [
        'icon' => 'plane-arrival',
        'text' => 'AMS Data Management & US Customs Pre-Alert',
        'color' => '#E05C69'
      ],
      [
        'icon' => 'scanner',
        'text' => 'Shipment Recovery / Acceptance Scan',
        'color' => '#0082C8'
      ],
      [
        'icon' => 'hand-receiving',
        'text' => 'Clearance, Inspection, Sort & Manifest',
        'color' => '#E05C69'
      ],
      [
        'icon' => 'shipping-fast',
        'text' => 'Parcel Delivery via Final Mile Carrier',
        'color' => '#0082C8'
      ],
    ]
@endphp

@extends('app.updated-layout')

@section('title', 'Air2Post | ')

@section('content')

@section('page-hero')
<div class="pageHero">
  <section class="pageHero-content">
    <div class="productImage">
      <svg viewBox="0 0 894 150" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M893.877 0L0 43.9202H738.229L609.184 150L893.877 0Z" fill="white"/>
        <path fill-rule="evenodd" clip-rule="evenodd" d="M308.358 148.795V130.418H284.659L294.701 121.28C299.454 116.995 302.634 113.179 304.241 109.831C305.915 106.417 306.751 102.769 306.751 98.8856C306.751 94.534 305.546 90.6845 303.136 87.3371C300.726 83.9897 297.379 81.4123 293.094 79.6047C288.81 77.7971 283.889 76.8933 278.332 76.8933C271.035 76.8933 264.809 78.3662 259.654 81.3118C254.499 84.1906 250.583 88.1405 247.905 93.1616L265.78 102.099C266.784 100.024 268.223 98.4169 270.098 97.2788C271.972 96.1407 273.981 95.5717 276.123 95.5717C278.332 95.5717 280.006 96.0738 281.144 97.078C282.349 98.0152 282.952 99.3542 282.952 101.095C282.952 102.501 282.583 103.94 281.847 105.413C281.111 106.886 279.638 108.66 277.428 110.735L252.022 134.234V148.795H308.358ZM105.786 133.732H73.1492L66.9231 148.795H50.2532L81.5846 78.5H97.652L129.084 148.795H112.012L105.786 133.732ZM100.665 121.38L89.5179 94.467L78.3711 121.38H100.665ZM136.377 78.5H152.645V148.795H136.377V78.5ZM200.559 129.213L214.116 148.795H231.589L215.823 126.2C220.442 124.192 223.99 121.313 226.467 117.564C229.011 113.748 230.283 109.229 230.283 104.007C230.283 98.7851 229.045 94.2662 226.568 90.4502C224.091 86.6342 220.543 83.6885 215.923 81.6131C211.371 79.5377 205.981 78.5 199.755 78.5H169.328V148.795H185.596V129.213H199.755H200.559ZM209.998 94.9691C212.542 97.0445 213.814 100.057 213.814 104.007C213.814 107.89 212.542 110.903 209.998 113.045C207.454 115.187 203.739 116.258 198.852 116.258H185.596V91.7556H198.852C203.739 91.7556 207.454 92.8268 209.998 94.9691ZM440.455 150C433.157 150 426.563 148.427 420.672 145.28C414.847 142.134 410.261 137.816 406.914 132.326C403.633 126.769 401.993 120.543 401.993 113.648C401.993 106.752 403.633 100.559 406.914 95.0696C410.261 89.5129 414.847 85.1614 420.672 82.0148C426.563 78.8683 433.157 77.295 440.455 77.295C447.752 77.295 454.313 78.8683 460.137 82.0148C465.962 85.1614 470.547 89.5129 473.895 95.0696C477.242 100.559 478.916 106.752 478.916 113.648C478.916 120.543 477.242 126.769 473.895 132.326C470.547 137.816 465.962 142.134 460.137 145.28C454.313 148.427 447.752 150 440.455 150ZM440.455 136.142C444.605 136.142 448.354 135.205 451.702 133.33C455.049 131.389 457.66 128.711 459.535 125.296C461.476 121.882 462.447 117.999 462.447 113.648C462.447 109.296 461.476 105.413 459.535 101.999C457.66 98.5843 455.049 95.9399 451.702 94.0654C448.354 92.1239 444.605 91.1532 440.455 91.1532C436.304 91.1532 432.555 92.1239 429.207 94.0654C425.86 95.9399 423.216 98.5843 421.274 101.999C419.4 105.413 418.462 109.296 418.462 113.648C418.462 117.999 419.4 121.882 421.274 125.296C423.216 128.711 425.86 131.389 429.207 133.33C432.555 135.205 436.304 136.142 440.455 136.142ZM363.289 78.5001C369.515 78.5001 374.904 79.5378 379.456 81.6131C384.076 83.6885 387.624 86.6342 390.101 90.4502C392.578 94.2662 393.817 98.7852 393.817 104.007C393.817 109.162 392.578 113.681 390.101 117.564C387.624 121.38 384.076 124.326 379.456 126.401C374.904 128.409 369.515 129.414 363.289 129.414H349.129V148.795H332.861V78.5001H363.289ZM362.385 116.158C367.272 116.158 370.988 115.12 373.532 113.045C376.076 110.903 377.348 107.89 377.348 104.007C377.348 100.057 376.076 97.0445 373.532 94.9692C370.988 92.8268 367.272 91.7557 362.385 91.7557H349.129V116.158H362.385ZM498.071 147.791C503.293 149.264 508.682 150 514.239 150C520.666 150 526.122 149.029 530.608 147.088C535.093 145.146 538.441 142.535 540.65 139.255C542.926 135.908 544.064 132.225 544.064 128.209C544.064 123.522 542.826 119.807 540.349 117.062C537.938 114.317 535.026 112.309 531.612 111.037C528.198 109.765 523.813 108.493 518.457 107.221C513.302 106.082 509.452 104.944 506.908 103.806C504.431 102.601 503.193 100.794 503.193 98.3835C503.193 95.9734 504.264 94.0319 506.406 92.559C508.615 91.0862 511.996 90.3498 516.549 90.3498C523.043 90.3498 529.537 92.1908 536.03 95.8729L541.052 83.5211C537.838 81.5127 534.089 79.9729 529.804 78.9018C525.52 77.8306 521.135 77.295 516.649 77.295C510.222 77.295 504.766 78.2658 500.281 80.2072C495.862 82.1487 492.548 84.7931 490.339 88.1405C488.13 91.4209 487.025 95.1031 487.025 99.1868C487.025 103.873 488.23 107.622 490.64 110.434C493.117 113.246 496.063 115.288 499.477 116.56C502.891 117.832 507.277 119.104 512.632 120.376C516.114 121.179 518.858 121.916 520.867 122.585C522.942 123.254 524.616 124.158 525.888 125.296C527.227 126.368 527.896 127.74 527.896 129.414C527.896 131.69 526.792 133.531 524.582 134.937C522.373 136.276 518.959 136.945 514.339 136.945C510.189 136.945 506.038 136.276 501.887 134.937C497.737 133.598 494.155 131.824 491.142 129.614L485.619 141.866C488.766 144.276 492.916 146.251 498.071 147.791ZM568.761 91.7557H546.266V78.5001H607.523V91.7557H585.029V148.795H568.761V91.7557ZM618.651 87.7767C618.32 86.9672 618.154 86.0842 618.154 85.1276C618.154 84.1709 618.32 83.2879 618.651 82.4784C618.994 81.669 619.46 80.9699 620.049 80.3812C620.65 79.7803 621.355 79.3203 622.165 79.0015C622.974 78.6703 623.845 78.5048 624.777 78.5048C625.721 78.5048 626.592 78.6703 627.389 79.0015C628.199 79.3203 628.898 79.7803 629.487 80.3812C630.087 80.9699 630.554 81.669 630.885 82.4784C631.228 83.2879 631.4 84.1709 631.4 85.1276C631.4 86.0842 631.228 86.9672 630.885 87.7767C630.554 88.5861 630.087 89.2913 629.487 89.8923C628.898 90.481 628.199 90.9409 627.389 91.2721C626.592 91.5909 625.721 91.7504 624.777 91.7504C623.845 91.7504 622.974 91.5909 622.165 91.2721C621.355 90.9409 620.65 90.481 620.049 89.8923C619.46 89.2913 618.994 88.5861 618.651 87.7767ZM619.902 82.2025C619.411 83.061 619.166 84.036 619.166 85.1276C619.166 86.2191 619.411 87.1941 619.902 88.0526C620.392 88.9111 621.061 89.5918 621.907 90.0947C622.766 90.5852 623.722 90.8305 624.777 90.8305C625.844 90.8305 626.801 90.5852 627.647 90.0947C628.493 89.5918 629.161 88.9111 629.652 88.0526C630.143 87.1941 630.388 86.2191 630.388 85.1276C630.388 84.036 630.143 83.061 629.652 82.2025C629.161 81.344 628.493 80.6694 627.647 80.1789C626.801 79.676 625.844 79.4246 624.777 79.4246C623.722 79.4246 622.766 79.676 621.907 80.1789C621.061 80.6694 620.392 81.344 619.902 82.2025ZM624.427 85.8266H623.618V88.8069H622.312V81.0803H623.084H623.618H624.703C625.55 81.0803 626.224 81.3011 626.727 81.7426C627.23 82.1718 627.481 82.736 627.481 83.4351C627.481 83.9992 627.322 84.4775 627.003 84.87C626.684 85.2625 626.249 85.5384 625.697 85.6979L627.886 88.8069H626.322L624.427 85.8266ZM623.618 82.2209V84.7596H624.63C625.084 84.7596 625.445 84.6493 625.715 84.4285C625.997 84.2077 626.138 83.8889 626.138 83.4719C626.138 83.0549 625.997 82.7421 625.715 82.5336C625.445 82.3129 625.084 82.2025 624.63 82.2025L623.618 82.2209Z" fill="white"/>
      </svg>
    </div>

    <h2 class="mainHeading">Direct to consumer delivery solution</h2>

    <div class="pageHero-actions">
      <fw-link href="#contact" design="solid" state="critical" size="small" icon="paper-plane">
        Get in touch
      </fw-link>
      <fw-link href="#contentStart" design="outline" state="info" size="small" icon="book-open">
        Learn more
      </fw-link>
      <fw-link href="https://air2post.com" target="_blank" design="text" state="light" size="small" icon="map-marker-alt">
        Track a shipment
      </fw-link>
    </div>
  </section>
  <img class="pageHero-bg" src="{{ asset('images/woman-signing-for-delivery.jpeg') }}" alt="">
</div>
@endsection

<div class="d-flex flex-row flex-wrap justify-content-around" id="contentStart">
  @foreach ($features as $feature)
    <feature-card
      icon="{{ $feature['icon'] }}"
      text="{{ $feature['text'] }}"
      color="{{ $feature['color'] }}"
    ></feature-card>
  @endforeach
</div>

<div class="angled-card">
  <span class="angled-card-title">
    In A Nutshell
  </span>

  <span class="angled-card-divider">&nbsp;</span>

  <span class="angled-card-text">
    Air2Post&reg; is a Direct to Consumer delivery solution for ecommerce parcels weighing 1 ounce to 25 pounds.
  </span>
</div>

<p class="finePrint"><strong>Section 321 Import</strong>: A de minimis shipment commonly referred to as Section 321, allows for
  goods valued at $800 USD or less to enter the US Duty-Free as long as they are not one of several lots covered by a single order or contract, the value of which exceed $800. A great option that can save you and your customers time and money.</p>

<section style="margin: 96px 0px 0px;">
    <h2 class="sectionHeading" style="margin: 0px 0px 24px;">Key Features</h2>

    <div class="d-flex flex-row flex-wrap justify-content-around">
        <fw-card-general
            title="Trasit Time Flexibility"
            image="{{ asset('images/svg/delivery-truck-1.svg') }}"
            image-alt="Delivery truck icon."
            summary="Choose from Select or Premium Levels, or work with our team to develop a more customized solution tailored to your needs."
            contain-image
        >
        </fw-card-general>

        <fw-card-general
            title="Air2Post&reg; Tracking Platform"
            image="{{ asset('images/svg/earth.svg') }}"
            image-alt="Earth icon."
            summary="End-to-end visibility, from Origin Processing, Air Freight Transit, Customs Clearance to End Consumer Delivery Confirmation."
            contain-image
        >
        </fw-card-general>

        <fw-card-general
            title="Enhanced Customer Experience"
            image="{{ asset('images/svg/happy.svg') }}"
            image-alt="Happy emoji icon."
            summary="Competitive, service level based pricing, end-toend performance management and the Air2Post&reg; system will improve the delivery experience over non-integrated postal solutions and add value to your direct to consumer delivery process."
            contain-image
        >
        </fw-card-general>
    </div>
</section>

<div class="angled-card">
  <span class="angled-card-title">
    Customized Solution
  </span>

  <span class="angled-card-divider">&nbsp;</span>

  <span class="angled-card-text">
      We offer customization based on customer and consumer needs — including management of inbound air freight, customs clearance, multiple carrier and service levels, undeliverable returns, and full process tracking management.
  </span>
</div>

<section class="fullWidthWrapper bg-image">
  <h2 class="sectionHeading">Cutting Edge Technology</h2>

  <p>Air2Post&reg;, System, was built from the ground up to support a proprietary process that both manages and provides end-to-end visibility to the parcel life cycle. Starting with the consolidation of parcels into an Air Freight shipment through to Customs Clearance and on to Final Mile Delivery confirmation, everything is documented and the data provided to you. A vital tool and competitive advantage in the International Ecommerce Market.</p>
  <img src="{{ asset('images/red-high-speed-track.jpg') }}" alt="Red high speed track." />
</section>

<section class="fullWidthWrapper" id="contact">
    <h2 class="sectionHeading">Interested in learing more?</h2>
    <h3 class="sectionSubHeading">Fill out the form below and we will have an Air2Post rep contact you with more information.</h3>

    <form action="{{ route('air2PostContact') }}" method="post" class="mt-5" id="contactForm">

        @method('POST')
        @csrf

        <div class="form-group">
            <label for="first_name" @if ($errors->has('first_name')) class="error" @endif>
                <span class="label-text">First name</span>
                <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" placeholder="Jane">
            </label>

            <label for="last_name" @if ($errors->has('last_name')) class="error" @endif>
                <span class="label-text">Last Name</span>
                <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="Doe">
            </label>
        </div>

        <div class="form-group">
            <label for="business" @if ($errors->has('business')) class="error" @endif>
                <span class="label-text">Business Name</span>
                <input type="text" name="business" id="business" value="{{ old('business') }}" placeholder="ACME Industries">
            </label>

            <label for="email" @if ($errors->has('email')) class="error" @endif>
                <span class="label-text">Email</span>
                <input type="email" name="email" id="email" value="{{ old('email') }}" placeholder="jane.doe@email.com">
            </label>
        </div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-button design="solid" state="info" size="small" icon="paper-plane" type="submit" role="button"
                class="g-recaptcha"
                data-sitekey="{{ config('app.recaptcha_public') }}"
                data-callback="onSubmit"
            >
                Send Message
            </fw-button>
        </div>

    </form>
</section>

@endsection
