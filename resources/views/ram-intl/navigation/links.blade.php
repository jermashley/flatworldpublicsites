@php
$solutionLinks = [
    [
        'title' => 'Air2Post',
        'route' => route(config('app.prefix') . '.solutions.air2post')
    ],
    [
        'title' => 'Air Freight',
        'route' => route(config('app.prefix') . '.solutions.airFreight')
    ],
    [
        'title' => 'Ocean Freight',
        'route' => route(config('app.prefix') . '.solutions.oceanFreight')
    ],
    [
        'title' => 'Customs & Compliance',
        'route' => route(config('app.prefix') . '.solutions.compliance')
    ],
    [
        'title' => 'Temp-Controlled Cargo',
        'route' => route(config('app.prefix') . '.solutions.tempControlledCargo')
    ],
    [
        'title' => 'Specialty Cargo',
        'route' => route(config('app.prefix') . '.solutions.specialtyCargo')
    ],
    [
        'title' => 'Warehousing & Distribution',
        'route' => route(config('app.prefix') . '.solutions.warehousing')
    ],
    [
        'title' => 'Domestic Transportation',
        'route' => route(config('app.prefix') . '.solutions.domesticTransportation')
    ],
];

$trackingLinks = [
    [
        'title' => 'Pipeline',
        'route' => 'https://pipeline.flatworldsc.com/login.php'
    ],
    [
        'title' => 'WebTracker',
        'route' => 'http://ramstl.webtracker.wisegrid.net/'
    ],
];

if (Route::is(['home', config('app.prefix').'.solutions.air2post'])) {
    $theme = 'light';
} else {
    $theme = 'dark';
}
@endphp
@if(! Route::is('home'))
<fw-nav-link title="Solutions" theme="{{ $theme }}" dropdown>
    @foreach ($solutionLinks as $link)
        <a href={{ $link['route'] }}>{{ $link['title'] }}</a>
    @endforeach
</fw-nav-link>
@endif

<fw-nav-link title="E-Commerce / ASM" route="{{ route(config('app.prefix') . '.ecommerce') }}" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="Resources" route="{{ route(config('app.prefix') . '.resources') }}" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="Tracking" theme="{{ $theme }}" dropdown>
    @foreach ($trackingLinks as $link)
        <a href={{ $link['route'] }} target="_blank">{{ $link['title'] }}</a>
    @endforeach
</fw-nav-link>

<fw-nav-link title="About Us" route="{{ route(config('app.prefix') . '.about') }}" theme="{{ $theme }}"></fw-nav-link>
