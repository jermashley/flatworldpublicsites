@extends('app.updated-layout')
@section('title', 'Resources | ')
@section('content')


<h1 class="page__title my-1">Resources &amp; Downloads</h1>

<h3 class="page__subTitle my-1">Through RAM Int'l, Flat World Global Solutions’s service excellence includes careful implementation to understand your service requirements! We’ll walk
	you through these documents to support your import business start-up!</h3>



<div class="d-flex flex-row flex-wrap justify-content-around align-items-start" style="margin: 64px 0 0;">

	{{-- Flat World Global Solutions Importation Guide --}}
	<fw-card-document file-icon="file-pdf" file-name="RAM Int'l Importation Guide" file-size="260 KB" file-type="Adobe PDF" href="/downloads/ram-import-checklist.pdf">
	</fw-card-document>

	{{-- Flat World Global Solutions Power of Attorney - Form 5291 --}}
	<fw-card-document file-icon="file-pdf" file-name="Power of Attorney - Form 5291" file-size="70 KB" file-type="Adobe PDF"
	 href="/downloads/ram-form-5291-poa.pdf">
	</fw-card-document>

	{{-- Instructions for Completing the Custom Power of Attorney --}}
	<fw-card-document file-icon="file-pdf" file-name="Power of Attorney Instructions" file-size="1.6 MB" file-type="Adobe PDF"
	 href="/downloads/ram-poa-instructions.pdf">
	</fw-card-document>

	{{-- Flat World Global Solutions Terms and Conditions --}}
	<fw-card-document file-icon="file-pdf" file-name="RAM Int'l Terms and Conditions" file-size="173 KB" file-type="Adobe PDF"
	 href="/downloads/ram-tac.pdf">
	</fw-card-document>

	{{-- Flat World Global Solutions Credit Application --}}
	<fw-card-document file-icon="file-word" file-name="RAM Int'l Credit Application" file-size="41 KB" file-type="Microsoft Word"
	 href="/downloads/ram-intl-credit-app.docx">
	</fw-card-document>

	{{-- CBP Form 5106 --}}
	<fw-card-document file-icon="file-pdf" file-name="CBP Form 5106" file-size="1.2 MB" file-type="Adobe PDF" href="/downloads/cbp_form_5106.pdf">
	</fw-card-document>

	{{-- Commercial Invoice Requirements --}}
	<fw-card-document file-icon="file-pdf" file-name="Commercial Invoice Requirements" file-size="56 KB" file-type="Adobe PDF"
	 href="/downloads/commercial-invoice-requirements.pdf">
	</fw-card-document>

	{{-- Cargo Insurance Election --}}
	<fw-card-document file-icon="file-pdf" file-name="Cargo Insurance Election" file-size="719 KB" file-type="Adobe PDF"
	 href="/downloads/cargo_insurance_election.pdf">
	</fw-card-document>

	{{-- Warehousing & Fulfillment Agreement --}}
	<fw-card-document file-icon="file-word" file-name="Warehousing &amp; Fulfillment Agreement" file-size="41 KB" file-type="Microsoft Word"
	 href="/downloads/warehousing-fulfillment-agreement.docx">
	</fw-card-document>
</div>
@endsection
