@extends('app.updated-layout')

@section('title', 'Contact | ')

@section('content')

    <h1 class="page__title my-1">Contact</h1>

    <h3 class="page__subTitle my-1">If you would like to speak with someone, please call <a href="tel:1-800-844-4726" class="link">(800) 884-4726</a>. You can also email us at <a href="mailto:ram@ram-intl.com.com" class="link">ram@ram-intl.com.com</a>. Otherwise, please fill out the form below. If you are looking for our E-Commerce / ASM services, please see our <a href="{{ route(config('app.prefix').'.ecommerce') }}" class="link">E-Commerce Quote form</a>.</h3>

    @include ('partials.contactForm')

    <div class="modal-old darkBg hidden" id="ecommerceAlert">
        <fw-card-general
            title="E-Commerce Form"
            href="{{ route(config('app.prefix').'.ecommerce') }}"
            button-text="Visit E-Commerce Page"
            summary="If you are an importer for E-Commerce (Amazon), please visit our E-Commerce page to get a quote."
        >
        </fw-card-general>
    </div>

    <div class="d-flex flex-row flex-wrap justify-content-center align-items-start" style="margin: 64px 0 0;">

        {{-- Saint Louis --}}
        <fw-card-general
            title="Saint Louis"
            href="https://goo.gl/maps/aCNZAW1V7eN2"
            button-icon="map-marker-alt"
            button-text="Get Directions"
        >
            <address>
                <span>8949 Frost Ave</span>
                <span>Berkeley, MO</span>
                <span>63134</span>
            </address>
        </fw-card-general>

        {{-- Chicago --}}
        <fw-card-general
            title="Chicago"
            href="https://goo.gl/maps/tk8CaLkYHfJ2"
            button-icon="map-marker-alt"
            button-text="Get Directions"
        >
            <address>
                <span>1300 N. Michael Drive, Unit C</span>
                <span>Wood Dale, IL</span>
                <span>60191</span>
            </address>
        </fw-card-general>

        {{-- Just so the cards break here --}}
        <div class="w-100"></div>

        {{-- Dallas --}}
        <fw-card-general
            title="Dallas"
            href="https://goo.gl/maps/mRB1JG6DuVs"
            button-icon="map-marker-alt"
            button-text="Get Directions"
        >
            <address>
                <span>5251 Frye Road, Suite 150</span>
                <span>Irving, TX </span>
                <span>75061</span>
            </address>
        </fw-card-general>

        {{-- Atlanta --}}
        <fw-card-general
            title="Atlanta"
            href="https://goo.gl/maps/HCcc7SyWNDr"
            button-icon="map-marker-alt"
            button-text="Get Directions"
        >
            <address>
                <span>3000 Centre Parkway, Suite 200</span>
                <span>East Point, GA</span>
                <span>30344</span>
            </address>
        </fw-card-general>

    </div>

@endsection
