@extends('app.updated-layout')

@section('title', 'Alert | ')

@section('content')

  <div class="markdown">
    <h4>
      Dear Colleagues,
    </h4>

    <p>
      Would like to inform you that based on a unanimous decision taken in the
      special general body meeting held on 27th June, the All India Motor
      Transport Congress has decided to go on an indefinite strike starting from
      20th July to revolt against the repressive and retrograde policies of the
      Govt. which are trampling the transport sector.
    </p>

    <p>
      The Transport Congress has threatened that they will not withdraw the
      strike till the time their demands are met which include.
    </p>

    <ul>
      <li>
        Reduction in diesel prices, uniform pricing nationally & quarterly
        revision.
      </li>

      <li>
        Toll Barrier Free India.
      </li>

      <li>
        Transparency and Reduction in Third Party Insurance Premium through
        exemption of GST on TPP and abolishing of excess commission paid to agents
        through Comprehensive Policy.
      </li>

      <li>
        Abolition of TDS, rationalization of presumptive income in section 44AE
        of Income Tax Act & E-way bill operational issues.
      </li>

      <li>
        National Permit for Buses & other tourist Vehicles
      </li>

      <li>
        Abolition of Direct Port Delivery (DPD) tendering system; streamlining
        of Port Congestion.
      </li>

      <li>
        Corruption, Harassment on the Roads by RTOs, Police & CTOs and Double
        Driver Issues.
      </li>
    </ul>

    <p>
      We will closely monitor the situation and keep you updated on the delays
      (if any) with our shipments.
    </p>
</div>

@endsection