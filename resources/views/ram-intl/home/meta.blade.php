<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="description" content="RAM Int'l, DBA Flat World Global Solutions has built a reputation for handling oversized and specialty airfreight shipments for some of the most high profile clients in the world.">
<meta name="keywords" content="International Transportation, International Shipment, Freight Forwarder, Freight Forwarding, Haz-mat Cargo, Shipment Tracking, Air Freight, Ocean Freight, Customs, Compliance, Temp-controlled Cargo, Specialty Cargo, Warehousing, Distribution, Domestic Transportation">
<meta name="author" content="Prologue Technology">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
