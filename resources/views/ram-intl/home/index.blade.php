@extends('app.updated-layout')

@section('hero')

    <div id="hero">

        <div class="hero content">

            <div class="logo">

                <img src="/images/common/gs-full-logo-light.png" alt="img-fluid">

            </div>

            <div class="snippet cta">

                <span>Freight Forwarder &amp; US Customhouse Broker</span>

            </div>

        </div>

    </div>

@endsection



@section('content')

    <div id="home">

        <h3 class="text dark center display small font-weight-200 mb-3">RAM Int'l, DBA Flat World Global Solutions offers a variety of solutions for your international logistics needs</h3>

        <p class="text dark paragraph large mt-5 mb-5">We have built a reputation for handling oversized and specialty airfreight shipments for some of the most high profile clients in the world. We also offers unique expertise in Haz-Mat shipping. Our door-to-door service handles your cargo from pickup, assembly and consolidation, to tracking, delivery and billing. Our experienced team collaborates to process export documentation and track shipments from door-to-door. Our systems ensure compliance with government regulations with connections to customs agencies around the world. We are a C-TPAT approved forwarder and we continually train our staff on best security practices.  Our facilities also meet the stringent physical security requirements of agencies like the US Transportation Security Administration.</p>

        <div class="d-flex flex-row flex-wrap justify-content-center mt-5">

            {{--  Air2Post  --}}
            <fw-card-general
                title="Air2Post"
                image="{{ Brand::asset('images/woman-signing-for-delivery.jpeg') }}"
                image-alt="Cargo plane on the tarmac."
                summary="Air2Post&reg; is a Direct to Consumer delivery solution for ecommerce parcels weighing 1 ounce to 25 pounds offering customization based on customer and consumer needs — including management of inbound air freight, customs clearance, multiple carrier and service levels, undeliverable returns, and full process tracking management."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.solutions.air2post') }}"
                cover-image
            >
            </fw-card-general>

            {{--  Air Freight  --}}
            <fw-card-general
                title="Air Freight"
                image="{{ Brand::asset('images/airplane-tarmac-1-2048.jpg') }}"
                image-alt="Cargo plane on the tarmac."
                summary="Through RAM Int'l, Flat World Global Solutions has built a reputation for handling oversized and specialty airfreight shipments for some of the most high profile clients in the world. We also offers unique expertise in Haz-Mat shipping. Our door-to-door service handles your cargo from pickup, assembly and consolidation, to tracking, delivery and billing."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.solutions.airFreight') }}"
                cover-image
            >
            </fw-card-general>

            {{--  Ocean Freight  --}}
            <fw-card-general
                title="Ocean Freight"
                image="{{ Brand::asset('images/%s/cargo_ship-2048.jpg') }}"
                image-alt="Ocean freighter."
                summary="Through RAM Int'l, Flat World Global Solutions is a leading Less-than Container Load (LCL) service provider. LCL offers the possibility of consolidating multiple consignments from multiple customers in one Full Container Load (FCL). Customers can ship low volumes without having the cost commitments of a full container. Our customers benefit from an optimized logistics spend, more flexibility and seamless door-to-door services with the highest level of schedule integrity and reliability in transit times."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.solutions.oceanFreight') }}"
                cover-image
            >
            </fw-card-general>

            {{--  Customs & Compliance  --}}
            <fw-card-general
                title="Customs & Compliance"
                image="{{ Brand::asset('images/chainlink-1-2048.jpg') }}"
                image-alt="Chain link fence with cargo containers in the background."
                summary="We are able to perform compliance assessments, help create Import and Export Compliance Management programs and manuals to help support your business, as well as assist with short-term compliance projects."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.solutions.compliance') }}"
                cover-image
            >
            </fw-card-general>

            {{--  Temp-Controlled Cargo  --}}
            <fw-card-general
                title="Temp-Controlled Cargo"
                image="{{ Brand::asset('images/refrigerated-containers-1-2048.jpg') }}"
                image-alt="Refridgerated cargo containers."
                summary="You invest millions of dollars developing your products to take to market and you need a broad range of passive and active temperature-controlled shipping options to meet your needs. We offers extensive temperature controlled shipping solutions. Whether you're looking to move frozen field study crop samples or high-value pharmaceuticals, your compliance staff can rely on our shipping experts to help coordinate your shipments. We understand the importance of our shipping process as related to your study protocols, USDA, FDA and GLP requirements."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.solutions.tempControlledCargo') }}"
                cover-image
            >
            </fw-card-general>

            {{--  Specialty Cargo  --}}
            <fw-card-general
                title="Specialty Cargo"
                image="{{ Brand::asset('images/hazardous-1-2048.jpg') }}"
                image-alt="Person handling hazardous material in small bottle with gloves."
                summary="A service that combines our long-standing experience in ground handling, customs clearance and air freight to offer a proven, end-to-end service that ensures the safe transport of perishables in line with strict regulations. We offer flexible air capacity out of multiple gateways, pre and on-carriage temperature control, as well as specialty packaging abilities. From Live Fish to Frozen Products, have the experience to handle the most difficult of perishables."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.solutions.specialtyCargo') }}"
                cover-image
            >
            </fw-card-general>

            {{--  Warehousing & Distribution  --}}
            <fw-card-general
                title="Warehousing & Distribution"
                image="{{ Brand::asset('images/warehouse-glow-2-2048.jpg') }}"
                image-alt="Interior view of large warehouse."
                summary="Are you bringing in products from multiple vendors or locations for export? Or do you have an import shipment that require multiple shipments in the same container going to different consignees? Let us take care of your consolidation and deconsolidation needs in one of our strategic US warehouse locations."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.solutions.warehousing')}}"
                cover-image
            >
            </fw-card-general>

            {{--  Domestic Transportation  --}}
            <fw-card-general
                title="Domestic Transportation"
                image="{{ Brand::asset('images/truck-highway-2-2048.jpg') }}"
                image-alt="Tractor trailer on the highway."
                summary="Choice is critical to a cost effective, yet service sensitive supply chain. Utilizing a menu of LTL carriers to attain optimum service performance, leveraging volume to ensure competitive prices, and optimizing multiple base rates are all ways to better utilize a company's resources. Because your time is valuable, our personal service with dedicated account management, along with state of the art technology offers a more efficient way to manage your LTL transportation dollar. Only 'best in class' National, Inter-regional, and Regional providers are utilized, so you can be confident in the value your company will receive from us. We select the best service-providing carriers in the industry for each geographic area and combine that with the optimum price for the service required to provide for the greatest value proposition in the market."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.solutions.domesticTransportation') }}"
                cover-image
            >
            </fw-card-general>

            {{--  International Shipment Tracking  --}}
            {{--  @component('components.cards.general', [
                'cardClasses' => 'medium header mx-2 mb-5',
                'hasShadow' => true,
                'isAnimated' => true,
                'hasHero' => true,
                'hasContent' => true,
                'hasFooter' => true,
                'hasActions' => true,
                'heroImageClasses' => 'cover center',
                'heroImageSource' => Brand::asset('images/touching-screen-1-2048.jpg'),
                'cardHeading' => 'International Shipment Tracking',
                'headingClasses' => 'text dark center uppercase heading h4 font-weight-600',
                'cardText' => str_limit("Shipment and cargo visibility is one of the most important aspects of our industry. We offer one of the most complete visibility and tracking systems allowing you to get the most up-to-date status of your shipments.", 150),
                'textClasses' => 'text dark center heading h5 font-weight-400 mt-4',
                'actions' => [
                    [
                        'text' => 'Read More',
                        'link' => route(config('app.prefix').'.solutions.shipmentTracking'),
                        'classes' => 'link mt-4',
                    ]
                ]
            ])
            @endcomponent  --}}

            {{--  E-Commerce / ASM  --}}
            <fw-card-general
                title="E-Commerce / ASM"
                image="{{ Brand::asset('images/boxes-laptop-1-2048.jpg') }}"
                image-alt="Miniature shipping boxes on a laptop keyboard."
                summary="Through RAM Int'l, Flat World Global Solutions is a full-service global Freight Forwarder and Customs Broker, equipped to handle your import or export needs."
                button-icon="glasses"
                button-text="Learn More"
                href="{{ route(config('app.prefix').'.ecommerce') }}"
                cover-image
            >
            </fw-card-general>

        </div>

    </div>

@endsection
