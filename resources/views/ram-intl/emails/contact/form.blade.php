@component('mail::message')

@component('mail::panel')
**{{ $form->firstName }} {{ $form->lastName }}** from **{{ $form->business}}** sent a message!
@endcomponent

@component('mail::panel')
Here is {{ $form->firstName}}'s contact Information.

**{{ $form->business }}**<br />
@if($form->business) {{ $form->typeOfBusiness }} @endif

{{ $form->address }}<br />

{{-- {{ $form->address_1 }}<br />
{{ $form->address_2 }}<br />
{{ $form->city }}, {{ $form->state }} {{ $form->zip}} --}}

{{ $form->email }}<br />
**{{ $form->telephone }}** @if($form->extension) **Ext. {{ $form->extension }}** @endif
@endcomponent

@component('mail::panel')
    {{ $form->message }}
@endcomponent

@component('mail::button', ['url' => 'mailto:' . $form->email ])
    Send {{ $form->firstName }} a quick reply
@endcomponent


Thanks,<br>
Your Friendly RAM Int'l Mail Bot

@endcomponent
