@component('mail::message')

@component('mail::panel')
## Hi {{ $form->firstName }}!

Thank you for your email and interest in RAM Int'l, DBA Flat World Global Solutions!

---

@if($form->typeOfBusiness == 'Importer')
If you are an importer for E-Commerce (Amazon), please visit the eCommerce page on our website at [ram-intl.com](http://ram-intl.com/solutions/ecommerce). This webpage will guide you through our process and provide all required documents for importing into the US. On the webpage, there’s a template for your completion in order to receive a quote. Once you submit your request, our Ecommerce team will provide your quote within two business days.

---

Again, thank you and we look forward to working with you!
@endif

@if($form->typeOfBusiness == 'eCommerce')
To better assist you, please visit the eCommerce page on our website at [ram-intl.com](http://ram-intl.com/solutions/ecommerce). This webpage will guide you through our process and provide all required documents for importing into the US. On the webpage, there’s a template for your completion in order to receive a quote. Once you submit your request, we will provide your quote within two business days.

---

Again, thank you and we look forward to working with you!
@endif

[RAM Int'l, DBA Flat World Global Solutions](http://ram-intl.com)
@endcomponent

@if($form->typeOfBusiness == 'Importer')
@component('mail::panel')

> ###### **We would have hated losing a message we typed up, so, here is yours for you to copy**

> {{ $form->message }}

@endcomponent
@endif

@endcomponent
