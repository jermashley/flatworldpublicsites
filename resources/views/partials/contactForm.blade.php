@section('head-scripts')
<script src="https://www.google.com/recaptcha/api.js?render={{ config('app.recaptcha_public') }}"></script>
<script>
grecaptcha.ready(function() {
    grecaptcha.execute('{{ config('app.recaptcha_public') }}', {action: 'contact_index'}).then(function(token) {
       if (token) {
           document.getElementById('recaptcha').value = token
       }
    });
});
</script>
@endsection

<form action="{{ route('contact.store') }}" method="post" class="mt-5" id="contactForm">

    @method('POST')
    @csrf

    <input type="hidden" name="recaptcha" id="recaptcha">

    <h2 class="mb-3" style="padding: 0 0 0 12px;">General Information</h2>

    <div class="form-group">
        <label for="first_name" @if ($errors->has('first_name')) class="error" @endif>
            <span class="label-text">First name</span>
            <input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" placeholder="Jane">
        </label>

        <label for="last_name" @if ($errors->has('last_name')) class="error" @endif>
            <span class="label-text">Last Name</span>
            <input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="Doe">
        </label>
    </div>

    <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Business Information</h2>

    <div class="form-group">
        <label for="business" @if ($errors->has('business')) class="error" @endif>
            <span class="label-text">Business Name</span>
            <input type="text" name="business" id="business" value="{{ old('business') }}" placeholder="ACME Industries">
        </label>

        @if (config('app.prefix') == 'ram-intl')
        <label for="typeOfBusiness" @if ($errors->has('typeOfBusiness')) class="error" @endif>
            <span class="label-text">Business Type</span>
            <select name="typeOfBusiness" id="typeOfBusiness">
                <option value="NULL" selected disabled>Chose One</option>
                <option value="Importer">Importer</option>
                <option value="Exporter">Exporter</option>
                <option value="Broker">Broker</option>
                <option value="eCommerce">eCommerce</option>
            </select>
        </label>
        @endif
    </div>

    <div class="form-group" @if ($errors->has('address_1')) class="error" @endif>
        <label for="address_1">
            <span class="label-text">Address Line 1</span>
            <input type="text" name="address_1" id="address_1" value="{{ old('address_1') }}" placeholder="123 Main St">
        </label>
    </div>

    <div class="form-group" @if ($errors->has('address_2')) class="error" @endif>
        <label for="address_2">
            <span class="label-text">Address Line 2</span>
            <input type="text" name="address_2" id="address_2" value="{{ old('address_2') }}" placeholder="Suite 200">
        </label>
    </div>

    <div class="form-group" @if ($errors->has('city')) class="error" @endif>
        <label for="city">
            <span class="label-text">City</span>
            <input type="text" name="city" id="city" value="{{ old('city') }}" placeholder="Saint Louis">
        </label>

        <label for="state" @if ($errors->has('state')) class="error" @endif>
            <span class="label-text">State</span>
            <select name="state" id="state">
                @foreach ($states as $abbr => $state)
                <option value="{{ $state }}"
                @if ($selectedState === $state) selected @endif
                >{{ $abbr }}</option>
                @endforeach
            </select>
        </label>

        <label for="zip" @if ($errors->has('zip')) class="error" @endif>
            <span class="label-text">Zip Code</span>
            <input type="text" name="zip" id="zip" value="{{ old('zip') }}" placeholder="63101">
        </label>
    </div>

    <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Contact Information</h2>

    <div class="form-group">
        <label for="email" @if ($errors->has('email')) class="error" @endif>
            <span class="label-text">Email</span>
            <input type="email" name="email" id="email" value="{{ old('email') }}" placeholder="jane.doe@email.com">
        </label>

        <label for="telephone" @if ($errors->has('telephone')) class="error" @endif>
            <span class="label-text">Phone Number</span>
            <input type="tel" name="telephone" id="telephone" value="{{ old('telephone') }}" placeholder="(123) 456-7890" inputmode="numeric">
        </label>

        <label for="extension" @if ($errors->has('extension')) class="error" @endif>
            <span class="label-text">Extension</span>
            <input type="text" name="extension" id="extension" value="{{ old('extension') }}" placeholder="123" inputmode="numeric">
        </label>
    </div>

    <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Let us know how we can help you!</h2>

    <div class="form-group">
        <label for="message" @if ($errors->has('message')) class="error" @endif>
            <span class="label-text">Message</span>
            <textarea name="message" id="message" rows="6" placeholder="Please be as descriptive as possible.">{{ old('message') }}</textarea>
        </label>
    </div>

    <div class="form-group tsa_field" style="display: none;">
        <label for="blood_type">
            <span>Blood type</span>
            <input type="text" name="blood_type" id="blood_type">
        </label>
    </div>

    <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
        <fw-button design="solid" state="info" size="small" icon="paper-plane" type="submit" role="button">
            Send Message
        </fw-button>
    </div>

</form>
