@php
	if (Route::is(['home', config('app.prefix').'.solutions.air2post'])) {
		$theme = 'light';
	} else {
		$theme = 'dark';
	}
@endphp

@include(Brand::partial('navigation.links'))

<fw-nav-link title="Contact" route="{{ route('contact.index') }}" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="Flat World Holdings" route="http://flatworldholdings.com/" theme="{{ $theme }}"></fw-nav-link>

@if (Auth::check())
<fw-nav-link icon="user" theme="{{ $theme }}" dropdown>
		<div>
			<div>
				{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}
			</div>

			@foreach (Auth::user()->roles as $role)
			<div style="font-size: .8rem; opacity: .6; margin: .2rem 0 0;">
				{{ $role->display_name }}{{$loop->last ? '' : ', '}}
			</div>
			@endforeach
		</div>

		<hr style="border-color: rgba(0,0,0,.1);
		border-width: 1px;
		border-bottom: none;">

		@if (!Entrust::hasRole('guest'))

		@if (Entrust::hasRole(['developer', 'super.admin', 'admin', 'blogger']))
		<a href="{{ route('admin.blog.index') }}" class="navigation-overflow-link">
			Blog Posts <fw-font-awesome-icon icon="file-alt"></fw-font-awesome-icon>
		</a>
		@endif

		@if (Entrust::hasRole(['developer', 'super.admin', 'admin', 'hr']))
		<a href="{{ route('admin.career.index') }}" class="navigation-overflow-link">
			Career Listings <fw-font-awesome-icon icon="graduation-cap"></fw-font-awesome-icon>
		</a>
		@endif

		<hr style="border-color: rgba(0,0,0,.1);
		border-width: 1px;
		border-bottom: none;">
		@endif

		<a href="{{ route('logout') }}" class="navigation-overflow-link">
			Log Out <fw-font-awesome-icon icon="sign-out"></fw-font-awesome-icon>
		</a>
</fw-nav-link>
@endif
