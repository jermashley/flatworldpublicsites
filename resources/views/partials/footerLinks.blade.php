@include(Brand::partial('footer.links'))

<a href="{{ route('contact.index') }}" class="footer-link">
	Contact Us
</a>

@if(Auth::check())
<a href="{{ route('logout') }}" class="footer-link">
	Log Out
	<fw-font-awesome-icon icon="sign-out"></fw-font-awesome-icon>
	{{-- <i class="far fa-sign-out fa-fw ml-2"></i> --}}
</a>
@else
<a href="{{ route('login') }}" class="footer-link">
	Log In
	<fw-font-awesome-icon icon="sign-in"></fw-font-awesome-icon>
	{{-- <i class="far fa-sign-in fa-fw ml-2"></i> --}}
</a>
@endif

<a href="http://flatworldholdings.com/" target="_blank" class="footer-link">
	Flat World Holdings
	<fw-font-awesome-icon icon="external-link"></fw-font-awesome-icon>
	{{-- <i class="far fa-external-link fa-fw ml-2"></i> --}}
</a>