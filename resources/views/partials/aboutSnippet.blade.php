<p class="text center dark heading h4 font-weight-400 line-height-large mt-3">At {{ config('app.name') }}, we believe in a culture of excellence built on a foundation of continuous improvement. We know how
  fast global supply chains change, and how your shipping department needs tools that allow you to compare rates, keep
  track of goods throughout the entire life cycle of a shipment, increase visibility and efficiency, and above all make
  your supply chain a competitive advantage, rather than a liability.</p>

<h3 class="page__subTitle mt-5">Core Purpose</h3>
<p class="text center dark heading h4 font-weight-400 line-height-large mt-3">Enable organizations to find better ways to manage their supply chains, while providing an enriching environment for employees to build careers and a better way of life for themselves and their families.</p>

<h3 class="page__subTitle mt-5">Core Values</h3>
<ul class="generalList styled mt-3">
  <li>Innovation – Aspire to create new solutions and be willing to take risks.</li>
  <li>Integrity – Never violate the trust that our clients, our vendors, and our teammates have honored us with.</li>
  <li>Excellence - Passion for excellence for our clients, to each other, and in our community.</li>
  <li>Commitment – We must do everything in our power to deliver on promises made.</li>
</ul>

<hr class="mt-5 mb-5">
