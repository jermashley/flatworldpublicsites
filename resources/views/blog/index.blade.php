@extends('app.updated-layout')

@section('title', 'Blog | ')

@section('content')

    <h1 class="page__title my-1">{{ Brand::name() }} Blog</h1>

    <h3 class="page__subTitle my-1">Keep up-to-date with the latest news from Flat World Global Solutions. Check in for our weekly news post.</h3>


    <div class="d-flex flex-row flex-wrap justify-content-around" style="margin: 64px 0 24px;">

        @foreach ($blogs as $blog)
        <fw-card-blog
            featured-image="{{ $blog->featured_image }}"
            image-alt="Blog image"
            title="{{ $blog->title }}"
            summary="{{ $blog->summary }}"
            created-at="{{ $blog->created_at }}"
            company="{{ $blog->company->name }}"
            href="{{ route('blog.show', ['blog' => $blog->slug]) }}"
        >
        </fw-card-blog>
        @endforeach

    </div>

    {{ $blogs->links('components.paginator') }}

@endsection
