@extends('app.updated-layout')

@section('title', $blog->title.' | ')

@section('meta')
<meta property="og:title" content="{{ $blog->title }}">
<meta property="og:description" content="{{ $blog->summary }}">
<meta property="og:image" content="{{ $blog->featured_image }}">
<meta property="og:site_name" content="{{ config('app.name') }}">
@endsection

@section('content')

    <div class="blog__hero">
        <img src="{{ $blog->featured_image }}" alt="{{ $blog->title }} hero image.">
    </div>

    <div class="blog__content">
        <div class="blog__meta">
            Posted on
            <span>{{ date('F d, Y', strtotime($blog->created_at)) }}</span>
        </div>

        <h1 class="blog__title">{{ $blog->title }}</h1>

        <h3 class="blog__summary">{{ $blog->summary }}</h3>

        <div class="addthis_inline_share_toolbox" style="margin: 0 0 0 -7px;"></div>

        <hr
            style="
                border-color: rgba(0,0,0,.1);
                border-width: 1px;
                border-bottom: none;
            "
        >

        <div class="blog__body markdown">

            @markdown($blog->content)

        </div>
    </div>

@endsection

@section('footer-scripts')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c4a2f08a740f59b"></script>
@endsection
