@extends('app.updated-layout')

@section('title', 'Create User | ')

@section('content')

<h1 class="page__title my-1">Create User</h1>

@if(Auth::user()->hasRole('admin'))
<h3 class="page__subTitle my-1">
	This user will be a part of {{ Auth::user()->company->name }}.
</h3>
@endif

<form action="{{ route('user.store') }}" method="post" class="mt-5">
	@method('POST')
	@csrf

	<div class="form-group">
		<label for="first_name" @if ($errors->has('first_name')) class="error" @endif>
			<span class="label-text">First Name</span>
			<input type="text" name="first_name" id="first_name" value="{{ old('first_name') }}" placeholder="John" required>
		</label>

		<label for="last_name" @if ($errors->has('last_name')) class="error" @endif>
			<span class="label-text">Last Name</span>
			<input type="text" name="last_name" id="last_name" value="{{ old('last_name') }}" placeholder="Doe" required>
		</label>
	</div>

	<div class="form-group">
		<label for="email" @if ($errors->has('email')) class="error" @endif>
			<span class="label-text">E-Mail</span>
			<input type="email" name="email" id="email" value="{{ old('email') }}" placeholder="john.doe@email.com" required>
		</label>
	</div>

	<div class="form-group">
		<label for="role_id" @if ($errors->has('role_id')) class="error" @endif>
			<span class="label-text">Role</span>
			<select name="role_id" id="role_id" required>
				@foreach ($roles as $role)

					@if (Auth::user()->hasRole('super.admin'))
						@if ($role->name == 'developer')
						@continue
						@endif
					@endif

					@if (Auth::user()->hasRole('admin'))
						@if ($role->name == 'developer' || $role->name == 'super.admin')
						@continue
						@endif
					@endif

					<option value="{{ $role->id }}">{{ $role->display_name }}</option>

				@endforeach
			</select>
		</label>

		@if (Auth::user()->hasRole(['developer', 'super.admin']))
		<label for="company_id" @if ($errors->has('company_id')) class="error" @endif>
			<span class="label-text">Company</span>
			<select name="company_id" id="company_id" required>
				@foreach ($companies as $company)
					<option value="{{ $company->id }}">{{ $company->name }}</option>
				@endforeach
			</select>
		</label>
		@endif
	</div>

	<div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
		<fw-link href="{{ route('user.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
			Cancel
		</fw-link>

		<fw-button design="solid" state="info" size="small" icon="save" type="submit" role="button">
			save
		</fw-button>
	</div>

</form>

@endsection