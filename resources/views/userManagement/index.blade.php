@extends('app.updated-layout')

@section('title', 'Manage Users | ')

@section('content')

	<h1 class="page__title my-1">Hi {{ Auth::user()->first_name }}!</h1>

	<h3 class="page__subTitle my-1">
		Here's where you can manage all of the users in your organization.
	</h3>

	<fw-link href="{{ route('user.create') }}" target="_self" design="solid" state="info" size="small" icon="plus">
		Create User
	</fw-link>

	@foreach ($users as $user)
	<div class="userManagementCompact">
		<img src="https://images.unsplash.com/photo-1500462918059-b1a0cb512f1d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=0e3917e828b7c2502b0041813eff1294&auto=format&fit=crop&w=668&q=80" class="avatar" alt="">
		<div class="userInfo">
			<div class="user">{{ $user->first_name }} {{ $user->last_name }}</div>
			<div class="roles">
				@if ($user->id == Auth::user()->id)
				<span style="color: #F23D4C;">
					<fw-font-awesome-icon icon="asterisk"></fw-font-awesome-icon>
					{{-- <i class="fas fa-asterisk fa-fw" style="margin: 0 2px 0 0;"></i> --}}
				</span>
				@endif
				@foreach ($user->roles as $role)
				<span>{{ $role->display_name }}{{$loop->last ? '' : ', '}}</span>
				@endforeach
			</div>
			<div class="company">
				{{ $user->company->name }}
			</div>
		</div>
		<div class="controls">
			{{-- @unless ($user->hasRole('developer') || $user->id == Auth::user()->id) --}}
			<fw-link href="{{ route('user.edit', ['user' => $user->id]) }}" target="_self" design="text" state="dark" size="small" icon="cog" />
			{{-- @endunless --}}
		</div>
	</div>
	@endforeach

@endsection