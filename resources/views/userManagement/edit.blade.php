@extends('app.updated-layout')

@section('title', $user->first_name . ' ' . $user->last_name . ' | ')

@section('content')

	<div class="userManagementDetails">
		<div class="userManagementDetailsHeader">
			<img src="https://images.unsplash.com/photo-1500462918059-b1a0cb512f1d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=0e3917e828b7c2502b0041813eff1294&auto=format&fit=crop&w=668&q=80" class="avatar" alt="">

			<span>Update {{ $user->first_name }}'s Information</span>
		</div>

		<div class="userManagementDetailsContent">
			<form action="{{ route('user.update', ['id' => $user->id]) }}" method="post" enctype="multipart/form-data" id="updateUserForm">
				@csrf
				@method('PATCH')

				<div class="form-group">
					<label for="first_name" @if ($errors->has('first_name')) class="error" @endif>
						<span class="label-text">First Name</span>
						<input type="text" name="first_name" id="first_name" value="{{ $user->first_name }}" placeholder="First Name" required>
					</label>

					<label for="last_name" @if ($errors->has('last_name')) class="error" @endif>
						<span class="label-text">Last Name</span>
						<input type="text" name="last_name" id="last_name" value="{{ $user->last_name }}" placeholder="Last Name" required>
					</label>
				</div>

				<div class="form-group">
					<label for="email" @if ($errors->has('email')) class="error" @endif>
						<span class="label-text">E-Mail</span>
						<input type="email" name="email" id="email" value="{{ $user->email }}" placeholder="john.doe@email.com" required>
					</label>
				</div>

				<h3 class="mt-5 mb-1" style="padding: 0 0 0 12px;">What is {{$user->first_name }}'s role?</h3>

				<div class="form-group">
					<label for="role_id" @if ($errors->has('role_id')) class="error" @endif>
						<span class="label-text">Role</span>
						<select name="role_id" id="role_id" required>
							@foreach ($roles as $role)

								@if (Auth::user()->hasRole('super.admin'))
									@if ($role->name == 'developer')
									@continue
									@endif
								@endif

								@if (Auth::user()->hasRole('admin'))
									@if ($role->name == 'developer' || $role->name == 'super.admin')
									@continue
									@endif
								@endif

								<option value="{{ $role->id }}"
									@if ($user->hasRole($role->name)) selected @endif
									>{{ $role->display_name }}</option>

							@endforeach
						</select>
					</label>
				</div>

				@if (Auth::user()->hasRole(['developer', 'super.admin']))
				<h3 class="mt-5 mb-1" style="padding: 0 0 0 12px;">What company does {{$user->first_name }} work for?</h3>

				<div class="form-group">
					<label for="company_id" @if ($errors->has('company_id')) class="error" @endif>
						<span class="label-text">Company</span>
						<select name="company_id" id="company_id" required>
							@foreach ($companies as $company)
								<option value="{{ $company->id }}"
									@if ($company->id == $user->company->id) selected @endif
									>{{ $company->name }}</option>
							@endforeach
						</select>
					</label>
				</div>
				@endif

			</form>

		</div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-link href="{{ route('user.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
                Cancel
            </fw-link>

            <fw-button design="solid" state="info" size="small" icon="save" type="submit" role="button">
					Save
            </fw-button>
		</div>

	</div>

@endsection