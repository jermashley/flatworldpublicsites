@extends('app.updated-layout')

@section('title', 'Contact | ')

@section('content')

    <h1 class="page__title my-1">Contact</h1>

    @include ('partials.contactForm')

    <div class="mt-5 contact" style="display: flex; flex-flow: row; justify-content: center;">

            <fw-card-general
                title="Flat World Hospitality"
                href="https://goo.gl/maps/LWfEYJeJXG62"
                button-icon="map-marker-alt"
                button-text="Get Directions"
            >
                <div class="contactInfo">
                    <span>
                        <fw-font-awesome-icon icon="linkedin"></fw-font-awesome-icon>
                        {{-- <i class="fa fa-phone"></i> --}}
                        <a href="tel: +18557838547" class="link">(855) 783-8547</a>
                    </span>
                    <span>
                        <fw-font-awesome-icon icon="fax"></fw-font-awesome-icon>
                        {{-- <i class="fa fa-fax"></i> --}}
                        (636) 787-0940
                    </span>
                    <span>
                        <fw-font-awesome-icon icon="envelope"></fw-font-awesome-icon>
                        {{-- <i class="fa fa-envelope"></i> --}}
                        <a href="mailto: projectmanagement@flatworldgs.com" class="link">projectmanagement@flatworldgs.com</a>
                    </span>
                </div>

                <address>
                    <span>2342 Technology Drive</span>
                    <span>Suite 310</span>
                    <span>O'Fallon, Missouri 63368</span>
                </address>
            </fw-card-general>

    </div>

@endsection
