@php
$links = [
	// [
	// 	'text' => 'Resources',
	// 	'destination' => route(config('app.prefix').'.resources')
	// ],
	// [
	// 	'text' => 'Terms & Conditions',
	// 	'destination' => route(config('app.prefix').'.terms')
	// ],
]
@endphp

@unless (empty($links))
	@foreach ($links as $link)
	<a href="{{ $link['destination'] }}" class="footer-link">
		{{ $link['text'] }}
		@unless (empty($link['icon']))
		<fw-font-awesome-icon icon="{{ $link['icon'] }}"></fw-font-awesome-icon>
		{{-- <i class="far fa-{{ $link['icon'] }} fa-fw ml-2"></i> --}}
		@endunless
	</a>
	@endforeach
@endunless
