@php
$humans = [
    [
        'name' => 'Stacey Gallagher',
        'title' => 'Director, Hospitality',
        'image' => Brand::asset('images/%s/about/stacey-gallagher.jpg')
    ],
    [
        'name' => 'Tara Steinbach',
        'title' => 'Lead Warehouse & Installation Manager',
        'image' => Brand::asset('images/%s/about/tara-steinbach.jpg')
    ],
    [
        'name' => 'TJ Schrik',
        'title' => 'Business Development',
        'image' => Brand::asset('images/%s/about/tj-schrik.jpg')
    ],
    [
        'name' => 'Kat Rowe',
        'title' => 'Project Manager',
        'image' => Brand::asset('images/%s/about/kat-rowe.jpg')
    ],
    [
        'name' => 'Quiana Stilwell',
        'title' => 'Project Manager',
        'image' => Brand::asset('images/%s/about/quiana-stilwell.jpg')
    ],
    [
        'name' => 'Shane Sullivan',
        'title' => 'Lead Project Manager',
        'image' => Brand::asset('images/%s/about/shane-sullivan.jpg')
    ],
    [
        'name' => 'Pam Catiller',
        'title' => 'Forecasting / Proposal Manager',
        'image' => Brand::asset('images/%s/about/pam-catiller.jpg')
    ],
    [
        'name' => 'Jennifer Krickl',
        'title' => 'Audit Specialist',
        'image' => Brand::asset('images/%s/about/jennifer-krickl.jpg')
    ],
]
@endphp

@extends('app.updated-layout')
@section('title', 'About Us | ')
@section('content')

<h1 class="page__title my-1">About Us</h1>

<h3 class="page__subTitle mt-5">Director Statement</h3>

<p class="text center dark heading h4 font-weight-400 line-height-large mt-3">Flat World Global Solutions is only as good as the people that interact with our customers on a day-to-day basis. That is why we choose the BEST! At Flat World we believe in a culture of excellence built on a foundation of continuous improvement and exceeding our customers expectation is our mission!</p>

<h3 class="page__subTitle mt-5">Core Purpose</h3>
<p class="text center dark heading h4 font-weight-400 line-height-large mt-3">Enable organizations to find better ways to manage their supply chains, while providing an enriching environment for employees to build careers and a better way of life for themselves and their families.</p>

<h3 class="page__subTitle mt-5">Core Values</h3>
<ul class="generalList styled mt-3">
    <li>Innovation – Aspire to create new solutions and be willing to take risks.</li>
    <li>Integrity – Never violate the trust that our clients, our vendors, and our teammates have honored us with.</li>
    <li>Excellence - Passion for excellence for our clients, to each other, and in our community.</li>
    <li>Commitment – We must do everything in our power to deliver on promises made.</li>
</ul>

<hr class="mt-5 mb-5">

<div class="content__featureWall" style="justify-content: center;">
    @foreach ($humans as $human)
    <fw-card-general title="{{ $human['name'] }}" image="{{ $human['image'] }}" image-alt="{{ $human['name'] }} portrait." summary="{{ $human['title'] }}"
        circle-image>
    </fw-card-general>
    @endforeach
</div>
@endsection
