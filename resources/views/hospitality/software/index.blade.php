@extends('app.updated-layout')
@section('title', 'Software | ')
@section('content')

<h1 class="page__title my-1">Software</h1>

<h3 class="page__subTitle my-1">Flat World Global Solutions will provide our clients with a best-in-class customer experience on every project</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3 class="em">We are continuously improving our products and services so you can continuously improve yours.</h3>

        <img src="{{ Brand::asset('https://image.shutterstock.com/z/stock-photo-business-people-teamwork-and-planning-concept-smiling-business-team-with-marker-and-stickers-325620971.jpg' ) }}"
            class="imageBg cover center" alt="">

    </div>
</div>

<p class="text center dark heading h4 font-weight-400 line-height-large">Our project management software is cloud based, proprietary and 100% customizable. HIVE™ was built by a team of programmers at <a href="https://prologuetechnology.com" target="_blank">Prologue Technology</a>, a Flat World Holdings, which allows us to stay at the forefront of new technology solutions. Our HIVE™ software takes a complex project and reduces it to an easy-to-use platform. With the help of our technology and our team, Flat World Global Solutions can let you expand at the speed of business, with no missed details and no surprises.</p>

{{-- Reports --}}
<div class="content__featureWall">

    <header class="featureWall__header">

        <span>Reports</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">Our Reports page give you access to reports that provide details from the PO level through installation. Most reports
            are available in excel format or PDF. In some cases your company logo can be added to the report, saving you
            time and energy recreating your PSR. In addition, these reports are built in-house. If you do not see the report
            you need, just ask, if we have the data we can create the report!</p>

    </div>

    <div class="featureWall__blockHalf">

        <a href="{{ Brand::asset('images/%s/screenshots/reports.png') }}" class="block__image clean-link" data-fancybox="screenshots">

                <img src="{{ Brand::asset('images/%s/screenshots/reports.png') }}" alt="">

                <div class="image__imageText tucked">
                    <span class="heading truncate">Reports Screen <fw-font-awesome-icon icon="expand-arrows"></fw-font-awesome-icon></span>

                    <span class="text">Here you have access to many different reports that provide you details from the PO level all the way through installation.</span>
                </div>

            </a>

    </div>

</div>


{{-- Project Management --}}
<div class="content__featureWall">

    <header class="featureWall__header">

        <span>Project Management</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">The Project Manager tab is the main hub for budget and PO information. Here we have several tools to insure you always
            have the information you need. Our platform will give you a snapshot of the original bid and the current spend
            so there are no surprises at close out. You have access to all PO information, including ready dates, follow
            up notes, tracking details, and hard copy PO’s regardless of stage.</p>

    </div>

    <div class="featureWall__blockHalf">

        <a href="{{ Brand::asset('images/%s/screenshots/project-manager-1.png') }}" class="block__image clean-link" data-fancybox="screenshots">

                <img src="{{ Brand::asset('images/%s/screenshots/project-manager-1.png') }}" alt="">

                <div class="image__imageText tucked">
                    <span class="heading truncate">Project Management Screen <fw-font-awesome-icon icon="expand-arrows"></fw-font-awesome-icon></span>
                    <span class="text">The Project Manager tab is the main hub for budget and PO information.  Here we have several tools to insure you are always kept up to date.</span>
                </div>

            </a>

    </div>

</div>


{{-- Inbound --}}
<div class="content__featureWall">

    <header class="featureWall__header">

        <span>Inbound</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">The Inbound tab houses your transportation data. You can find details on shipments regardless of whether they are
            waiting to be picked up, in route, or have already been delivered. Each BOL hyperlink will provide you with shipper,
            consignee, carrier, pro, current status, tracking details, eta, and item spec details. In addition, you have
            access to the hard copy, BOL, Packing List, Proof of Delivery and Signed Receiving Report if applicable.</p>

    </div>

    <div class="featureWall__blockHalf">

        <a href="{{ Brand::asset('images/%s/screenshots/inbound.png') }}" class="block__image clean-link" data-fancybox="screenshots">

                <img src="{{ Brand::asset('images/%s/screenshots/inbound.png') }}" alt="">

                <div class="image__imageText tucked">
                    <span class="heading truncate">Inbound Screen <fw-font-awesome-icon icon="expand-arrows"></fw-font-awesome-icon></span>
                    <span class="text">The Inbound tab is where all of the transportation data is housed. You can find details on shipment waiting to pick up, en-route, delivered and prepaid and add information.</span>
                </div>

            </a>

    </div>

</div>


{{-- Outbound --}}
<div class="content__featureWall">

    <header class="featureWall__header">

        <span>Outbound</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">The outbound tab is your warehouse inventory module. You can easily see the exact inventory at any given time. Our
            system is kept up-to-date within 24 hours of receipt or delivery. You can trust the information you are seeing
            is accurate and real-time. The outbound tab allows you to access hard copy signed delivery tickets.</p>

    </div>

    <div class="featureWall__blockHalf">

        <a href="{{ Brand::asset('images/%s/screenshots/outbound.png') }}" class="block__image clean-link" data-fancybox="screenshots">

                <img src="{{ Brand::asset('images/%s/screenshots/outbound.png') }}" alt="">

                <div class="image__imageText tucked">
                    <span class="heading truncate">Outbound Screen <fw-font-awesome-icon icon="expand-arrows"></fw-font-awesome-icon></span>
                    <span class="text">The outbound tab is your warehouse inventory module.  You can very easily see the exact inventory at any given time.</span>
                </div>

            </a>

    </div>

</div>


{{-- Installation --}}
<div class="content__featureWall">

    <header class="featureWall__header">

        <span>Installation</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">Our Installation tab ensures the final stage of your project runs just as smooth as the first. Here you will find
            pre and post punch data, as well as a comprehensive list of final punch items. This data is fed directly to HIVE
            from our onsite installation crew through the use of our proprietary platform. No more paper logs, just real-time
            data allowing us to react quickly to ensure an on-time, stress-free installation!</p>

    </div>

    <div class="featureWall__blockHalf">

        <a href="{{ Brand::asset('images/%s/screenshots/installation.png') }}" class="block__image clean-link" data-fancybox="screenshots">

                <img src="{{ Brand::asset('images/%s/screenshots/installation.png') }}" alt="">

                <div class="image__imageText tucked">
                    <span class="heading truncate">Installation Screen <fw-font-awesome-icon icon="expand-arrows"></fw-font-awesome-icon></span>
                    <span class="text">Our Installation tab is a great resource to ensure the final stage of your project runs just as smooth as the first.</span>
                </div>

            </a>

    </div>

</div>


{{-- Invoice Manager --}}
<div class="content__featureWall">

    <header class="featureWall__header">

        <span>Invoice Manager</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">The invoice manager tab allows you to access invoice data for all transportation, warehouse and installation services. Flat World offers many invoice options: consolidated statement, transactional, pre-funded, and locked in CoG. Let us know which method best suits your business!</p>

    </div>

    <div class="featureWall__blockHalf">

        <a href="{{ Brand::asset('images/%s/screenshots/invoice.png') }}" class="block__image clean-link" data-fancybox="screenshots">

                <img src="{{ Brand::asset('images/%s/screenshots/invoice.png') }}" alt="">

                <div class="image__imageText tucked">
                    <span class="heading truncate">Invoice Manager Screen <fw-font-awesome-icon icon="expand-arrows"></fw-font-awesome-icon></span>
                    <span class="text">The invoice manager tab allows you to pull invoice data for all transportation, warehouse and installation services.</span>
                </div>

            </a>

    </div>

</div>
@endsection
