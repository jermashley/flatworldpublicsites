<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">

<head>
  <title></title>
  <!--[if !mso]><!-- -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <!--<![endif]-->
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style type="text/css">
    #outlook a {
      padding: 0;
    }

    .ReadMsgBody {
      width: 100%;
    }

    .ExternalClass {
      width: 100%;
    }

    .ExternalClass * {
      line-height: 100%;
    }

    body {
      margin: 0;
      padding: 0;
      min-height: 25rem;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      background: rgba(255,255,255,1);
    }

    .container {
      width: 100%;
    }

    .contentCard {
        width: 50%;
        min-width: 40rem;
        min-height: 25rem;
        margin: 0rem auto;
        border: .1rem solid rgba(245,245,245,1);
        padding: 2.5rem 5rem;
    }

    .fieldSection {
        width: 80%;
        margin: 0rem auto;
    }

    .footer {
        width: 80%;
        text-align: center;
        text-transform: uppercase;
        margin: 5rem auto;
        font-size: 1.05rem;
        font-weight: lighter;
    }

    h1 {
        color: rgba(190,190,190,1);
        font-weight: bold;
        text-align: center;
    }

    h3 {
        color: rgba(190,190,190,1);
        font-weight: lighter;
        text-align: center;
        text-transform: uppercase;
    }

    p {
        color: rgba(150,150,150,1);
        line-height: 1.75rem;
    }

    a {
        color: rgba(100,100,100,1);
        text-decoration: none;
        font-weight: bold;
        transition: 350ms ease;
    }

    a:hover {
        color: rgba(190,190,190,1);
    }

    hr {
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, #f0f0f0, #c8c8c8, #f0f0f0);
        background-image: -moz-linear-gradient(left, #f0f0f0, #c8c8c8, #f0f0f0);
        background-image: -ms-linear-gradient(left, #f0f0f0, #c8c8c8, #f0f0f0);
        background-image: -o-linear-gradient(left, #f0f0f0, #c8c8c8, #f0f0f0);
        margin: 3rem auto;
        width: 100%;
    }

    table,
    td {
      border-collapse: collapse;
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }

    img {
      border: 0;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }
  </style>
  <!--[if !mso]><!-->
  <style type="text/css">
    @media only screen and (max-width:480px) {
      @-ms-viewport {
        width: 320px;
      }
      @viewport {
        width: 320px;
      }
    }
  </style>
  <!--<![endif]-->
  <!--[if mso]>
<xml>
  <o:OfficeDocumentSettings>
    <o:AllowPNG/>
    <o:PixelsPerInch>96</o:PixelsPerInch>
  </o:OfficeDocumentSettings>
</xml>
<![endif]-->
  <!--[if lte mso 11]>
<style type="text/css">
  .outlook-group-fix {
    width:100% !important;
  }
</style>
<![endif]-->

  <style type="text/css">
    @media only screen and (min-width:480px) {
      .mj-column-per-100 {
        width: 100%!important;
      }
    }
  </style>
</head>

<body>

    <div class="container">
        <div class="contentCard">
            <h1><strong>{{ $form->firstName }} {{ $form->lastName }}</strong> sent us a new message through the <em>Hospitality Services Website</em>!</h1>
            <hr>
            <div class="fieldSection">
                <h3>Here is {{ $form->firstName }}'s message:</h3>
            </div>

            <div class="fieldSection">{{ $form->message }}</div>

            <hr>

            <div class="footer">
                <p>You can reply to {{ $form->firstName }} by clicking <a href="mailto:{{ $form->email }}">here</a></p>.
            </div>
        </div>
    </div>

</body>

</html>
