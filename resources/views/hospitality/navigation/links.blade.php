@php
if (Route::is('home')) {
  $theme = 'light';
} else {
  $theme = 'dark';
}
@endphp

<fw-nav-link title="Services" route="{{ route(config('app.prefix') . '.services') }}" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="Software" route="{{ route(config('app.prefix') . '.software') }}" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="Process" route="{{ route(config('app.prefix') . '.process') }}" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="About Us" route="{{ route(config('app.prefix') . '.about') }}" theme="{{ $theme }}"></fw-nav-link>
