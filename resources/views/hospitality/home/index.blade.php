@extends('app.updated-layout')

@section('hero')

    <div id="hero" class="">

        <div class="hero content">

            <div class="logo">

                <img src="/images/common/gs-full-logo-light.png" alt="">

            </div>

            <div class="snippet cta">

                <span>Best-in-class customer experience on every project</span>

            </div>

        </div>

    </div>

@endsection



@section('content')

<div id="home">

    <div class="content__featureWall">

        <div class="featureWall__blockHalf">

            <div class="featuredText">

                <h1>Flat World Global Solutions</h1>

                <p class="d-flex flex-row justify-content-between">
                    <span>visibility</span> / <span>flexibility</span> / <span>service</span> / <span>budget</span>
                </p>

            </div>

        </div>

        <div class="featureWall__blockHalf featuredArt">

            <img src="{{ Brand::asset('images/flying-macbook.jpg') }}" class="img-fluid" alt="">

        </div>

        <h4 class="text dark left heading h4 uppercase font-weight-700 mt-5">we provide our clients with a best-in-class customer experience on every project</h4>

        <p class="text dark left paragraph general">Flat World Global Solutions provides optimum solutions to challenging projects through the use of cutting edge logistics technology. Flat World Global Solutions is a consumer-driven company that combines a high level of visibility to FF&E and OSE goods from issuance of the purchase order to installation at the job site with proven cost containment strategies. We enhance our client’s productivity, profitability, performance and marketplace competitiveness.</p>

        <p class="text dark left paragraph general">Visibility, service, flexibility, and budget are at the forefront of every project, and everything we do is designed to deliver a superior customer experience.</p>

        <div class="featureWall__blockFull">

            <video width="100%" height="auto" poster="https://fwholdingsblog.s3.amazonaws.com/public-sites/hospitality-video-poster.jpg" controls>
                <source src="https://fwholdingsblog.s3.amazonaws.com/public-sites/hospitality-video.mp4" />
            </video>

        </div>

        <p class="text dark left paragraph general mt-5">We believe in a culture of excellence built on a foundation of continuous improvement. We know how fast global supply chains change, and the importance of keeping track of goods throughout the entire life cycle of a shipment, increase visibility and efficiency, and making your supply chain a competitive advantage.</p>

        <fw-link href="{{ route('contact.index') }}" target="_self" design="outline" state="info" size="small" icon="pen">
            Get In Touch
        </fw-link>

    </div>

</div>

@endsection
