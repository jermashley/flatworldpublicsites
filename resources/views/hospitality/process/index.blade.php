@extends('app.updated-layout')
@section('title', 'Process | ')
@section('content')

<h1 class="page__title my-1">Process</h1>

<h3 class="page__subTitle my-1">Taking control of your supply chain and becoming a Flat world Global Solutions Project Management customer is easy!</h3>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>We learn from experience</h3>
        <h3 class="em">And Strive for excellence</h3>

        <img src="{{ Brand::asset('images/csrs-1-2048.jpg' ) }}" class="imageBg cover center" alt="">

    </div>
</div>

<p class="text dark heading h4 font-weight-400 line-height-medium mt-4"><strong>Step 1:</strong> Fill Out <a href="/downloads/hospitality/welcome-packet.pdf" class="link">Credit App <fw-font-awesome-icon icon="download"></fw-font-awesome-icon></a></p>

<p class="text dark heading h4 font-weight-400 line-height-medium mt-4">
    <strong>Step 2:</strong> Lets Us know how we can help you. We tailor our service to meet your needs. We can handle all
    four services or any combination that suits your specific project requirements.
    <ul class="generalList styled">
        <li><a href="{{ route(config('app.prefix').'.services') }}#po-management" class="link">PO Management</a></li>
        <li><a href="{{ route(config('app.prefix').'.services') }}#freight-management" class="link">Freight Management</a></li>
        <li><a href="{{ route(config('app.prefix').'.services') }}#warehouse-management" class="link">Warehouse Management</a></li>
        <li><a href="{{ route(config('app.prefix').'.services') }}#installantion-management" class="link">Installation Management</a></li>
        <li><a href="{{ route(config('app.prefix').'.services') }}#claims-management" class="link">Claims Management</a></li>
    </ul>
</p>

<p class="text dark heading h4 font-weight-400 line-height-medium mt-4">
    <strong>Step 3:</strong> Provide Project Details for Proposal
    <ul class="generalList styled">
        <li>
            <strong>Transportation:</strong>
            <ul class="generalList styled">
                <li>Copies of PO’s or Budget Document with Manufacture (FOB points), Quantity, Description, Declared Value, and
                    Item Numbers</li>
                <li>Jobsite Requirements (liftgate, notify, straight truck, etc.)</li>
                <li>Timeline of Project</li>
            </ul>
        </li>
        <li>
            <strong>Warehouse:</strong>
            <ul class="generalList styled">
                <li>Length of Storage (Timeline)</li>
                <li>Weight Expected to be stored</li>
                <li>Distance Requirements if applicable</li>
                <li>Jobsite Requirements (liftgate, notify, straight truck, etc.)</li>
            </ul>
        </li>
        <li>
            <strong>Installation:</strong>
            <ul class="generalList styled">
                <li>Schedule</li>
                <li>Floor Plans</li>
                <li>Union/Non Union</li>
                <li>Site Requirements (Hard Hats, Work Hours, Elevator Usage, Etc.)</li>
            </ul>
        </li>
    </ul>
</p>
@endsection
