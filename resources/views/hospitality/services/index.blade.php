@extends('app.updated-layout')
@section('title', 'Services | ')
@section('content')

<h1 class="page__title my-1">Services</h1>

<h3 class="page__subTitle my-1">Flat World Global Solutions will provide our clients with a best-in-class customer experience on every project</h3>

<p class="text center dark heading h4 font-weight-400 line-height-large mt-5">Choice, visibility, service, and budget are always at the forefront of every project. Flat World Global Solutions provides optimum solutions to challenging projects through the use of cutting edge logistics technology. Flat World Global Solutions is a consumer-driven company that combines a high level of visibility to FF&amp;E and OS&amp;E goods from issuance of the purchase order to installation at the job site with proven cost containment strategies, all designed to deliver a superior customer experience. We strive to enhance our client’s productivity, profitability, performance and marketplace competitiveness.</p>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>We learn from experience</h3>
        <h3 class="em">And Strive for excellence</h3>

        <img src="{{ Brand::asset('images/csrs-1-2048.jpg' ) }}" class="imageBg cover center" alt="">

    </div>
</div>

{{--
<div class="d-flex flex-row justify-content-around flex-wrap">

    @component('components.cards.general', [ 'cardClasses' => 'small mx-4 mb-5', 'hasShadow' => true, 'isAnimated' => true, 'hasContent'
    => true, 'hasDivider' => true, 'hasFooter' => true, 'hasActions' => true, 'cardHeading' => 'Freight Management', 'headingClasses'
    => 'text dark center heading h5 font-weight-600', 'cardText' => str_limit('We have the ability to handle all types a
    freight: small package, LTL, TL, rail, expedited, as well as international air &amp; ocean. Our team will ensure that
    all', 100), 'textClasses' => 'text dark center heading h5 line-height-medium', 'actions' => [ [ 'text' => 'Read More',
    'link' => '#freight-management', 'classes' => 'link mt-4' ] ] ]) @endcomponent @component('components.cards.general',
    [ 'cardClasses' => 'small mx-4 mb-5', 'hasShadow' => true, 'isAnimated' => true, 'hasContent' => true, 'hasDivider' =>
    true, 'hasFooter' => true, 'hasActions' => true, 'cardHeading' => 'Warehouse Management', 'headingClasses' => 'text dark
    center heading h5 font-weight-600', 'cardText' => str_limit('Warehouse solutions are offered in all major markets, whether
    it be through our extensive network of agents or via our owned assets. Each warehouse is put through an', 100), 'textClasses'
    => 'text dark center heading h5 line-height-medium', 'actions' => [ [ 'text' => 'Read More', 'link' => '#warehouse-management',
    'classes' => 'link mt-4' ] ] ]) @endcomponent @component('components.cards.general', [ 'cardClasses' => 'small mx-4 mb-5',
    'hasShadow' => true, 'isAnimated' => true, 'hasContent' => true, 'hasDivider' => true, 'hasFooter' => true, 'hasActions'
    => true, 'cardHeading' => 'Installation Management', 'headingClasses' => 'text dark center heading h5 font-weight-600',
    'cardText' => str_limit('Flat World Hospitality (FWH) also handles installation of Fixture, Furniture and Equipment (FF&amp;E)
    and Operational Supplies and Equipment (OS &amp; E), giving us', 100), 'textClasses' => 'text dark center heading h5
    line-height-medium', 'actions' => [ [ 'text' => 'Read More', 'link' => '#installation-management', 'classes' => 'link
    mt-4' ] ] ]) @endcomponent @component('components.cards.general', [ 'cardClasses' => 'small mx-4 mb-5', 'hasShadow' =>
    true, 'isAnimated' => true, 'hasContent' => true, 'hasDivider' => true, 'hasFooter' => true, 'hasActions' => true, 'cardHeading'
    => 'Claims Management', 'headingClasses' => 'text dark center heading h5 font-weight-600', 'cardText' => str_limit('Flat
    World has an entire team dedicated to claims resolution. We also offer our customers a unique reorder program designed
    to alleviate the additional hardship related', 100), 'textClasses' => 'text dark center heading h5 line-height-medium',
    'actions' => [ [ 'text' => 'Read More', 'link' => '#claims-management', 'classes' => 'link mt-4' ] ] ]) @endcomponent

</div> --}}

<div class="content__featureWall" id="po-management">

    <header class="featureWall__header">
        <span>Purchase Order Management</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p>While we do not handle any purchasing in house, we do assist our customers team in managing all PO’s which gives
                their team more time to deal with other tasks!  We do this by uploading your PO’s to our online cloud based
                software.  We then contact each supplier to make sure they have received your PO, find out the lead-time
                and insure your goods will be ready to meet your required date.  All of this data is entered into our system
                (HIVE&trade;) and available for our customers 24 hours a day.  In addition, we send out customized reports
                once a week with all pertinent information regarding your projects and the PO Statuses.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">
            <img src="{{ Brand::asset('images/laptop-creditCard-1-2048.jpg') }}" alt="">

            <div class="image__imageText tucked">
                <span class="heading truncate">Purchase Order Dashboard</span>
                <span class="text">Our PO Management team works as an extension of our customer team saving our customers valuable time and money!</span>
            </div>
        </div>
    </div>

</div>

<div class="content__featureWall" id="freight-management">

    <header class="featureWall__header">
        <span>Freight Management</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p>Flat World handles all types of freight; small package, LTL, full truckloads, rail, expedited, international ocean and international air. When your purchase orders are ready, we route them via the most cost effective service that meets the needs for your individual project and purchase order.  We understand that one size does not fit all when it comes to maintaining your freight budget and your project delivery schedule.  Once the pickup is scheduled, we send an auto-generated alert to you letting you know that your shipment has departed, it references your tag number, quantity and description so that there is no wondering what has shipped.  Once the goods deliver, we send a second alert letting you know delivery is complete providing POD name, date and time.  This data is all also available online, via EDI or electronic transfer direct to your system or via specialized reporting.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">
            <img src="{{ Brand::asset('images/truck-highway-1-2048.jpg') }}" alt="">

            <div class="image__imageText tucked">
                <span class="heading truncate">Freight Management</span>
                <span class="text">Whether your project requires an on-time delivery or standard freight solutions we have what it takes to make it successful!</span>
            </div>
        </div>
    </div>

</div>

<div class="content__featureWall" id="warehouse-management">

    <header class="featureWall__header">
        <span>Warehouse Management</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p>Warehouse solutions are offered in all major markets, whether it be through our extensive network of agents or
                via our owned assets. Each warehouse is put through an extensive vetting process to ensure that they are
                up to our standards and able to execute operational excellence. All warehouse inventory is up to date, available
                to you 24 hours a day via the web. Original PO side markings and descriptions are used - making the delivery
                request streamlined and accurate.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">
            <img src="{{ Brand::asset('images/warehouse-glow-2-2048.jpg') }}" alt="">

            <div class="image__imageText tucked">
                <span class="heading truncate">Warehouse Management Software</span>
                <span class="text">Warehouse Inventory is available 24 hours a day via our web based proprietary software, the HIVE&trade;!</span>
            </div>
        </div>
    </div>

</div>

<div class="content__featureWall" id="installation-management">

    <header class="featureWall__header">
        <span>Installation Management</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p>Flat World offers installation of Fixture, Furniture and Equipment (FFE) and Operational Supplies and Equipment (OSE), giving us a true turnkey solution for our customers. FWH now handles our pre and post punches electronically via iPad to insure up to date installation status available to our customers.  This also creates a comprehensive punch list to ensure all parties are working from one master punch list to complete each project on time. In addition, Flat World now places an internal employee on each full installation.  We utilize traveling crews capable of handling more than 20-30 installations at any given time.  We use these crews for installation to ensure the highest quality install on all of our projects.  Our installation crews are available in select markets.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">
            <img src="{{ Brand::asset('images/hotel-lobby-1-2048.jpg') }}" alt="">

            <div class="image__imageText tucked">
                <span class="heading truncate">Innovative Installation Management Application</span>
                <span class="text">Our new way to streamline communication, ensuring the project is on track and everyone has the most accurate information.</span>
            </div>
        </div>
    </div>

</div>

<div class="content__featureWall" id="claims-management">

    <header class="featureWall__header">
        <span>Claims Management</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p>Flat World has an entire team dedicated to claims resolution. We also offer our customers a unique reorder program
                designed to alleviate the additional hardship related to the reorder of damaged good. We are proud to say
                that less than 1% of shipments have resulted in an OSD claim.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">
            <img src="{{ Brand::asset('images/business-people-1-2048.jpg') }}" alt="">

            <div class="image__imageText tucked">
                <span class="heading truncate">Excellent Claims Record</span>
                <span class="text">LESS THAN 1% of shipments have resulted in an OSD Claim!</span>
            </div>
        </div>
    </div>

</div>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3 class="em">proven results</h3>

        <div class="imageBg">
            <img src="{{ Brand::asset('images/people-charts-1-2048.jpg') }}" class="imageBg cover top" alt="">
        </div>

    </div>
</div>

<div class="content__statCard">

    <span class="line-1">97%</span>
    <span class="line-2">on-time performance</span>
    <span class="line-3">Transportation Management.</span>

</div>

<div class="content__statCard">

    <span class="line-1">5% - 7% CoG</span>
    <span class="line-2">Transportation Savings</span>
    <span class="line-3">vs 7% - 10% Industry Standard</span>

</div>

<div class="content__statCard">

    <span class="line-1">10% - 20%</span>
    <span class="line-2">Warehouse Savings</span>
    <span class="line-3">Using Flat World Global Solutions P.O. Management and Routing</span>

</div>

<div class="content__statCard">

    <span class="line-1">Installation Savings</span>
    <span class="line-2">Flat World has <em>never</em> missed an install deadline.</span>

</div>
@endsection
