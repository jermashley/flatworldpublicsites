<style>
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    color: #697B8D !important;
  }

  code {
    display: inline-block;
    width: 100%;
    padding: 6px 8px;
    background-color: rgba(247, 247, 248, 0.3);
  }

  hr {
    opacity: 0.35;
    margin: 16px auto;
  }
</style>

@component('mail::message')
# Howdy Flat World Global Solutions!

Someone just submitted an ecommerce form. Check out the details below.

@component('mail::panel')
## eCommerce / ASM Form Submission

---

{{-- General Information --}}
@component('emails/ecommerce/partials/generalInformation', [
'form' => $form
])
@endcomponent

@endcomponent

{{-- Shipment Details --}}
@component('emails/ecommerce/partials/shipmentDetails', [
'form' => $form
])
@endcomponent

{{-- Line Items --}}
@foreach ($form->lineItems as $lineItem)
@component('emails/ecommerce/partials/lineItem', [
'lineItem' => $lineItem,
'loop' => $loop
])
@endcomponent
@endforeach

@component('mail::panel')
Sincerely,

Your friendly Flat World Global Solutions Mail Bot
@endcomponent

@endcomponent
