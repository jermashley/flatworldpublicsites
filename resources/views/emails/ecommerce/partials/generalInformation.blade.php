|                                                                         |
|----------------|--|--|--------------------------------------------------|
|**Name**:       |  |  | {{ $form->firstName }} {{ $form->lastName }}     |
|**Company**:    |  |  | {{ $form->companyName }}                         |
|**Email**:      |  |  | [{{ $form->email }}](mailto:{{ $form->email }})  |
|**Telephone**:  |  |  | {{ $form->phone }}                               |
|**Address**:    |  |  | {{ $form->address }}                             |
@if ($form->message !== '' || null)
|**Message**:    |  |  | {{ $form->message }}                             |
@endif