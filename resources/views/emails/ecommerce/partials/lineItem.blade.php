@component('mail::panel')
## Line Item {{ $loop->index + 1 }}

---

| |
|----------------------|--|--|-------------------------------|
|**Package Type**: | | | ```{{ $lineItem['type'] }}``` |
|**Number of Units**: | | | ```{{  $lineItem['count'] }}``` |
@if ($lineItem['type'] === 'Carton/Box')
|**Dimensions**: | | | ```{{ number_format($lineItem['length']) }}{{ $lineItem['dimensionsUnit'] }} X
{{ number_format($lineItem['width']) }}{{ $lineItem['dimensionsUnit'] }} X
{{ number_format($lineItem['height']) }}{{ $lineItem['dimensionsUnit'] }}``` |
@endif
@if ($lineItem['type'] === 'Pallet')
|**Pallet Type**: | | | ```{{ $lineItem['palletType'] }}``` |
@endif
|**Weight per Unit**: | | | ```{{ number_format($lineItem['weight']) }}{{ $lineItem['weightUnit'] }}``` |
|**Total Line Item Weight**:| |
|```{{ number_format($lineItem['weight'] * $lineItem['count']) }}{{ $lineItem['weightUnit'] }}``` |
|**Total Value of Goods**: | | | ```${{ $lineItem['value'] }}``` |
@if($lineItem['harmonizedTariffCode'])
|**Harmonized Tariff Code**:| | | ```{{ $lineItem['harmonizedTariffCode'] }}``` |
@endif
|**Product Description**: | | | ```{{ $lineItem['description'] }}``` |

@endcomponent
