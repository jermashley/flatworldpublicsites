@component('mail::panel')
## Shipment Details

---

|                                                                     |
|----------------------|--|--|----------------------------------------|
|**Transport Type**:   |  |  | {{ $form->transportType }}             |
|**Location Type**:    |  |  | {{ $form->pickUpLocation }}            |
|**Pick Up Date**:     |  |  | {{ $form->pickUpDate }}                |
|**Country**:          |  |  | {{ $form->pickUpCountry }}             |
|**Postal**:           |  |  | {{ $form->pickUpPostal }}              |
|**Pick Up Address**:  |  |  | {{ $form->pickUpAddress }}             |
|                                                                     |
|**Destination**:      |  |  | {{ $form->destination }}               |
|**Add Insurance**:    |  |  | {{ $form->addInsurance }}              |
|**Is Hazardous**:     |  |  | {{ $form->isHazardous }}               |
|**Has Bond**:         |  |  | {{ $form->hasBond }}                   |

@endcomponent