<style>
  h1, h2, h3, h4, h5, h6 {
    color: #697B8D !important;
  }

  code {
    display: inline-block;
    width: 100%;
    padding: 6px 8px;
    background-color: rgba(247, 247, 248, 0.3);
  }

  hr {
    opacity: 0.35;
    margin: 16px auto;
  }
</style>

@php
$now = new DateTime();
$ChineseNewYearEnd = new DateTime('2020-02-07 23:59:59');
@endphp


@component('mail::message')
# Howdy {{ $form->firstName }}!

@if ($now <= $ChineseNewYearEnd)
Due to the Chinese New Year holiday, rates for shipping are fluctuating greatly at this time.  We are able to provide
our quotes based on the current rates, but the rates will need to be checked again after the holiday, closer to the time
of shipping to ensure accuracy.

We recommend requesting a quote mid-February to ensure that the rates are up to date.

If you have any questions please email Rose-Anne del Rio at <a href="mailto:rdelrio@ram-intl.com">rdelrio@ram-intl.com</a>.
@else
Thank you for your request, we will respond with a quote within two business days.
@endif

Meanwhile, feel free to take look through the documents on [our Resources page]({{ route(config('app.prefix').'.resources') }})

##### Please take a moment to read through our [Terms & Conditions]({{ route(config('app.prefix').'.terms') }}).

@component('mail::panel')
## eCommerce / ASM Form Submission

*A courtesy copy of your quote request for your records.*

---

{{-- General Information --}}
@component('emails/ecommerce/partials/generalInformation', [
'form' => $form
])
@endcomponent

@endcomponent

{{-- Shipment Details --}}
@component('emails/ecommerce/partials/shipmentDetails', [
  'form' => $form
])
@endcomponent

{{-- Line Items --}}
@foreach ($form->lineItems as $lineItem)
  @component('emails/ecommerce/partials/lineItem', [
    'lineItem' => $lineItem,
    'loop' => $loop
  ])
  @endcomponent
@endforeach

@component('mail::panel')
Again, thank you and we look forward to working with you!

[Flat World Global Solutions](http://ram-intl.com)
@endcomponent

@endcomponent
