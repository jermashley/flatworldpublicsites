@component('mail::message')

@component('mail::panel')

## Hey {{ $form->first_name }}!

Thank you for your email and interest in {{ config('app.name') }}!  We have recieved your message and will be in touch as soon as possible.

@endcomponent

@if(config('app.prefix') == 'ram-intl')
@component('mail::panel')
@if($form->typeOfBusiness == 'Importer')
If you are an importer for E-Commerce (Amazon), please visit the eCommerce page on our website at [ram-intl.com](http://ram-intl.com/solutions/ecommerce). This webpage will guide you through our process and provide all required documents for importing into the US. On the webpage, there’s a template for your completion in order to receive a quote. Once you submit your request, our Ecommerce team will provide your quote within two business days.

---

Again, thank you and we look forward to working with you!
@endif

@if($form->typeOfBusiness == 'eCommerce')
To better assist you, please visit the eCommerce page on our website at [ram-intl.com](http://ram-intl.com/solutions/ecommerce). This webpage will guide you through our process and provide all required documents for importing into the US. On the webpage, there’s a template for your completion in order to receive a quote. Once you submit your request, we will provide your quote within two business days.

---

Again, thank you and we look forward to working with you!
@endif
@endcomponent
@endif

Thanks,<br>
{{ config('app.name') }}

@endcomponent
