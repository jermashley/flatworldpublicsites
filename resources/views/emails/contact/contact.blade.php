@component('mail::message')

@component('mail::panel')
**{{ $form->first_name }} {{ $form->last_name }}** from **{{ $form->business}}** sent a message!
@endcomponent

@component('mail::panel')
Here is {{ $form->first_name}}'s contact Information.

**{{ $form->business }}**<br />
@if(config('app.prefix') == 'ram-intl') {{ $form->typeOfBusiness }} @endif

{{ $form->address_1 }}<br />
{{ $form->address_2 }}<br />
{{ $form->city }}, {{ $form->state }} {{ $form->zip}}

{{ $form->email }}<br />
**{{ $form->telephone }}** @if($form->extension) **Ext. {{ $form->extension }}** @endif
@endcomponent

@component('mail::panel')
    {{ $form->message }}
@endcomponent

@component('mail::button', ['url' => 'mailto:' . $form->email ])
    Send {{ $form->first_name }} a quick reply
@endcomponent


Thanks,<br>
Your Friendly {{ config('app.name') }} Mail Bot

@endcomponent
