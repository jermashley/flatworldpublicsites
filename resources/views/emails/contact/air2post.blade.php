@component('mail::message')

@component('mail::panel')
**{{ $form->first_name }} {{ $form->last_name }}** from **{{ $form->business}}** wants to learn more about Air2Post!
@endcomponent

@component('mail::panel')
{{ $form->first_name }}'s email: {{ $form->email }}

@component('mail::button', ['url' => 'mailto:' . $form->email ])
    Send {{ $form->first_name }} a quick reply
@endcomponent


Thanks,<br>
Your Friendly {{ config('app.name') }} Mail Bot

@endcomponent
