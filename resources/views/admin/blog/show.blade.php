@extends('app.updated-layout') 
@section('title', $blog->title . ' | ') 
@section('content')

<h1 class="blog__title">{{ $blog->title }}</h1>

<h3 class="blog__summary">{{ $blog->summary }}</h3>

<div class="d-flex flex-column">
    <div class="splashImage blogSplash">

        <h3 class="em">{{ date('F d, Y', strtotime($blog->created_at)) }}</h3>

        <img src="{{ $blog->featured_image }}" class="imageBg cover center" alt="">

    </div>
</div>

<div class="blog__content markdown">

    @markdown($blog->content)

</div>
@endsection