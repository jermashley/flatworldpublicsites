@extends('app.updated-layout')

@section('title', '[Edit] ' . $blog->title . ' | ')

@section('content')

<blog-create blog-id="{{ $blog->id }}"></blog-create>

@endsection
