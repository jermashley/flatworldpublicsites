@extends('app.updated-layout')

@section('title', '[New] Blog | ')

@section('content')

    <blog-create></blog-create>

@endsection