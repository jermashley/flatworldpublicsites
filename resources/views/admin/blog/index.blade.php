@extends('app.updated-layout')

@section('title', 'Blog | ')

@section('content')

@if (Entrust::hasRole(['developer', 'super.admin']))
<h1 class="my-1 page__title">Flat World Public Sites - Blog Posts</h1>
@else
<h1 class="my-1 page__title">{{ Brand::name() }} Blog</h1>
@endif

@if (Entrust::hasRole(['developer', 'super.admin']))
<h3 class="my-1 page__subTitle">Manage blog posts for all Flat World sites.</h3>
@else
<h3 class="my-1 page__subTitle">Manage the blog posts for the company.</h3>
@endif

<fw-link href="{{ route('admin.blog.create') }}" target="_self" design="solid" state="info" size="small" icon="plus"
    class="mt-5 w-100">
    Create Post
</fw-link>

<div class="flex-row flex-wrap d-flex justify-content-between" style="margin: 64px 0 24px;">

    @foreach ($blogs as $blog)
    <fw-card-blog-admin featured-image="{{ $blog->featured_image }}" image-alt="Blog image" title="{{ $blog->title }}"
        @if ($blog->published === 1 && is_null($blog->deleted_at))
        blog-state-class="success"
        @elseif ($blog->published === 0 && is_null($blog->deleted_at))
        blog-state-class="info-alt"
        @elseif (!is_null($blog->deleted_at))
        blog-state-class="critical"
        @endif

        @if ($blog->published === 1 && is_null($blog->deleted_at))
        blog-state="Published"
        @elseif ($blog->published === 0 && is_null($blog->deleted_at))
        blog-state="Draft"
        @elseif (!is_null($blog->deleted_at))
        blog-state="Deleted"
        @endif

        created-at="{{ $blog->created_at }}"
        author="{{ $blog->user->first_name ?? 'Flat' }} {{ $blog->user->last_name ?? 'World' }}"
        company="{{ $blog->company->name }}"
        href="{{ route('admin.blog.edit', ['blogAdmin' => $blog->slug]) }}"
        >
    </fw-card-blog-admin>
    @endforeach

</div>

@if (Route::is('blog.index'))
{{ $blogs->links('components.paginator') }}
@endif

@endsection
