@extends('app.updated-layout')
@section('title', 'Admin | Careers | ')
@section('content')

<h1 class="page__title my-1">{{ Brand::name() }} Careers</h1>

<h3 class="page__subTitle my-1">Manage all career opportunities here. Careful with that delete button!</h3>


<div class="d-flex flex-row flex-wrap justify-content-around" style="margin: 64px 0 24px;">

  <table style="width: 100%;">
    <thead>
      <tr>
        <th style="width: 2rem;">Active</th>
        <th>Title</th>
        <th>Short Description</th>
        <th style="width: 16rem;">
          <div style="display: flex; flex-flow: row wrap; justify-content: flex-end; align-items: center;">
              <a href="{{ route('admin.career.create') }}" class="button button-small button-solid-success">New Career</a>
          </div>
        </th>
      </tr>
    </thead>

    <tbody>
      @foreach ($careers as $career)
      <tr>
        <td class="charicon" style="width: 2rem;">{!! $career->published === 1 ? '&#x2714;' : '' !!}</td>
        <td>{{ $career->title }}</td>
        <td>{{ $career->short_description }}</td>
        <td style="width: 16rem;">
          <div style="display: flex; flex-flow: row wrap; justify-content: flex-end; align-items: center;">
            <a href="{{ route('admin.career.edit', ['career' => $career->id]) }}" class="button button-small button-text-info">Edit</a>
            <form action="{{ route('api.career.destroy', ['career' => $career->id]) }}" method="POST">
                @csrf
                @method('DELETE')
                <button class="button button-small button-text-critical">Delete</button>
            </form>
          </div>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>

{{-- {{ $blogs->links('components.paginator') }} --}}
@endsection

@section('custom-css')
<style>
  table {
    width: 100%;
    border-spacing: 0px;
    border-collapse: separate;
  }

  thead {
    background-color: hsla(215, 5%, 93%, 1);
  }

  thead th {
    padding: 12px 16px;

    color: hsla(215, 7%, 40%, 1);
    font-size: 12px;
    font-weight: 800;
    text-align: left;
    text-transform: uppercase;
  }

  table tbody tr:nth-child(even) {
    background-color: hsla(215, 7%, 97%, 1);
  }

  tbody td {
    padding: 16px;

    color: hsla(215, 7%, 40%, 1);
    font-size: 14px;
    font-weight: 500;
    text-align: left;
  }

  .charicon {
    font-family: "Times", "Times New Roman", "serif", "sans-serif", "EmojiSymbols";
    font-size: 16px;
    opacity: 0.75;
  }
</style>
@endsection
