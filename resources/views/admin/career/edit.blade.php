@extends('app.updated-layout')
@section('title', 'Admin | Careers | ')
@section('content')
<career-admin page-title="Edit Career" career-id="{{ $career->id }}"></career-admin>
@endsection
