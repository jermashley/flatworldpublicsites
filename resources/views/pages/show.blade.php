@extends('app.updated-layout') 
@section('content') @if (Auth::check())
<fw-link href="{{ route('pages.edit', ['page' => $page->id]) }}" target="_self" design="solid" state="info" size="small"
    icon="edit">
    Edit This Page
</fw-link>
@endif

<h1 class="page__title my-1">{{ $page->title }}</h1>

<h3 class="page__subTitle my-1">{{ $page->summary }}</h3>

<div class="d-flex flex-column">
    <div class="splashImage blogSplash">

        <h3 class="em">{{ date('F d, Y', strtotime($page->created_at)) }}</h3>

        <img src="{{ $page->featured_image }}" class="imageBg cover center" alt="">

    </div>
</div>

<h2 class="mt-5 mb-4">Image Gallery</h2>

<div class="gallery row">
    @foreach($gallery_images as $gallery_image)
    <a href="{{ $gallery_image->url }}" data-fancybox="gallery">
                <img src="{{ $gallery_image->url }}" alt="">
            </a> @endforeach
</div>

<div class="markdown">

    @markdown($page->content)

</div>
@endsection