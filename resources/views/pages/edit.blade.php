@extends('app.updated-layout')

@section('title', '[Edit] Page | ')

@section('content')

    <h1 class="blog__title">Edit Page</h1>

    <form action="{{ route('pages.update', ['page' => $page->id]) }}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf


        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Title and Featured Image</h2>

        <div class="form-group">
            <label for="title" @if ($errors->has('title')) class="error" @endif>
                <span class="label-text">Post Title</span>
                <input type="text" name="title" id="title" value="{{ $page->title }}" placeholder="Post Title" required>
            </label>

            <label for="featured_image" @if ($errors->has('featured_image')) class="error" @endif>
                <span class="label-text">Featured Image</span>
                <input type="file" name="featured_image" id="featured_image">
            </label>
        </div>

        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Gallery Images</h2>

        <div class="form-group">

            <label for="images[]" @if ($errors->has('images')) class="error" @endif>
                <span class="label-text">Gallery Images</span>
                <input type="file" name="images[]" id="featured_image" multiple required>
            </label>

        </div>

        <div class="d-flex flex-row overflow-x mb-4">
            @foreach($images as $image)
                @component('components.cards.general', [
                    'cardClasses' => 'medium heroOnly no-flex-shrink mr-2',
                    'hasShadow' => true,
                    'isAnimated' => true,
                    'hasHero' => true,
                    'hasAdminControls' => true,
                    'heroImageClasses' => 'cover center',
                    'heroImageSource' => $image->url,
                    'deleteLink' => route('image.delete', ['id' => $image->id]),
                ])
                @endcomponent
            @endforeach
        </div>

        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Page Content</h2>

        <div class="form-group">
            <label for="summary" @if ($errors->has('content')) class="error" @endif>
                <span class="label-text">Post Summary</span>
                <textarea name="summary" id="summary" rows="3" placeholder="Drop in a short description about this post." required>{{ $page->summary }}</textarea>
            </label>
        </div>

        <div class="form-group mb-4">
            <label for="content" @if ($errors->has('content')) class="error" @endif>
                <span class="label-text">Post Content</span>
                <textarea name="content" id="post_content" rows="10" placeholder="Please be as descriptive as possible." required>{{ $page->content }}</textarea>
            </label>
        </div>

        @role(['developer', 'super.admin'])
        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">What company does this need to post on?</h2>

        <div class="form-group">
            <fieldset @if ($errors->has('company')) class="error" @endif>
                @foreach($companies as $company)
                <label class="radio" for="{{ $company->shortname }}">
                    @if($company->id == $page->company_id)
                    <input type="radio" name="company_id" id="{{ $company->shortname }}" value="{{ $company->id }}" checked required>
                    @else
                    <input type="radio" name="company_id" id="{{ $company->shortname }}" value="{{ $company->id }}" required>
                    @endif
                    <span class="label-text">{{ $company->name }}</span>
                </label>
                @endforeach
            </fieldset>
        </div>
        @endrole

        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Do you want this to post now?</h2>

        <div class="form-group">
            <fieldset @if ($errors->has('published')) class="error" @endif>
                <label class="inputGroup radio" for="published">
                    @if ($page->published === 1)
                    <input type="radio" name="published" id="published" value="1" checked required>
                    @else
                    <input type="radio" name="published" id="published" value="1" required>
                    @endif
                    <span class="label-text">Published</span>
                </label>

                <label class="inputGroup radio" for="draft">
                    @if ($page->published === 0)
                    <input type="radio" name="published" id="draft" value="0" checked required>
                    @else
                    <input type="radio" name="published" id="draft" value="0" required>
                    @endif
                    <span class="label-text">Draft</span>
                </label>
            </fieldset>
        </div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-link href="{{ route('pages.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
                Cancel
            </fw-link>

            <fw-button design="solid" state="info" size="small" icon="paper-plane" type="submit" role="button">
                Update Page
            </fw-button>
        </div>
    </form>

@endsection