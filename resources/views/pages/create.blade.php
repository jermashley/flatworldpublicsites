@extends('app.updated-layout')

@section('title', '[New] Page | ')

@section('content')

    @if ($errors->any())
    <div class="message-flash critical">
        <span class="close" data-toggle="hide-parent"><fw-font-awesome-icon icon="times"></fw-font-awesome-icon></span>
        <div>
            @foreach ($errors->all() as $error)
            <span class="response__heading" style="display: block; text-align: left;">{{ $error }}</span>
            @endforeach
        </div>
    </div>
    @endif

    <h1 class="blog__title">Create New Page</h1>

    <form action="{{ route('pages.store') }}" method="POST" enctype="multipart/form-data">
        @csrf

        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Title and Featured Image</h2>

        <div class="form-group">
            <label for="title" @if ($errors->has('title')) class="error" @endif>
                <span class="label-text">Post Title</span>
                <input type="text" name="title" id="title" value="{{ old('title') }}" placeholder="Post Title" required>
            </label>

            <label for="featured_image" @if ($errors->has('featured_image')) class="error" @endif>
                <span class="label-text">Featured Image</span>
                <input type="file" name="featured_image" id="featured_image" required>
            </label>
        </div>

        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Gallery Images</h2>

        <div class="form-group">

            <label for="images[]" @if ($errors->has('images')) class="error" @endif>
                <span class="label-text">Gallery Images</span>
                <input type="file" name="images[]" id="featured_image" multiple required>
            </label>

        </div>

        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Page Content</h2>

        <div class="form-group">
            <label for="summary" @if ($errors->has('summary')) class="error" @endif>
                <span class="label-text">Post Summary</span>
                <textarea name="summary" id="summary" rows="3" placeholder="Drop in a short description about this post." required>{{ old('summary') }}</textarea>
            </label>
        </div>

        <div class="form-group">
            <label for="content" @if ($errors->has('content')) class="error" @endif>
                <span class="label-text">Post Content</span>
                <textarea name="content" id="post_content" rows="10" placeholder="Please be as descriptive as possible." required>{{ old('content') }}</textarea>
            </label>
        </div>

        @role(['developer', 'super.admin'])
        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">What company does this need to post on?</h2>

        <div class="form-group">
            <fieldset @if ($errors->has('company')) class="error" @endif>
                @foreach($companies as $company)
                <label class="radio" for="{{ $company->shortname }}">
                    <input type="radio" name="{{ $company->shortname }}" id="{{ $company->shortname }}" value="{{ $company->id }}" required>
                    <span class="label-text">{{ $company->name }}</span>
                </label>
                @endforeach
            </fieldset>
        </div>
        @endrole



        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Do you want this to post now?</h2>

        <div class="form-group">
            <fieldset @if ($errors->has('published')) class="error" @endif>
                <label class="inputGroup radio" for="published">
                    <input type="radio" name="published" id="published" value="1" required>
                    <span class="label-text">Published</span>
                </label>

                <label class="inputGroup radio" for="draft">
                    <input type="radio" name="published" id="draft" value="0" required>
                    <span class="label-text">Draft</span>
                </label>
            </fieldset>
        </div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-link href="{{ route('pages.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
                Cancel
            </fw-link>

            <fw-button design="solid" state="info" size="small" icon="paper-plane" type="submit" role="button">
                Post Page
            </fw-button>
        </div>

    </form>

@endsection