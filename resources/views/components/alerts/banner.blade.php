<div class="alert banner {{ $state }}" style="opacity: 0;">
	<div class="icon">
		<span>
			@if ($state == 'info')
			<fw-font-awesome-icon icon="info-circle"></fw-font-awesome-icon>
			@elseif ($state == 'critical' or $state == 'error')
			<fw-font-awesome-icon icon="exclamation-triangle"></fw-font-awesome-icon>
			@elseif ($state == 'success')
			<fw-font-awesome-icon icon="thumbs-up"></fw-font-awesome-icon>
			@elseif ($state == 'warning')
			<fw-font-awesome-icon icon="exclamation-triangle"></fw-font-awesome-icon>
			@else
			<fw-font-awesome-icon icon="chess-board"></fw-font-awesome-icon>
			@endif
		</span>
	</div>

	<div class="message">
		@if (! empty($errors->all()) && $state == 'error')
		<span class="title">Errors on Page</span>
		<ul>
			@foreach ($errors->all() as $error)
			<li>{!! $error !!}</li>
			@endforeach
		</ul>
		@elseif (empty($errors->all()))
		<span>{!! $message or '' !!}</span>
		@endif
	</div>

	<div class="dismiss">
		<span><fw-font-awesome-icon icon="times"></fw-font-awesome-icon></span>
	</div>
</div>