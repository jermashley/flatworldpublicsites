<@if (! empty($button))
button
@if(! empty(array_get($button, 'role')))
role="{{ array_get($button, 'role', '') }}"
@endif

@if(! empty(array_get($button, 'type')))
type="{{ array_get($button, 'type', '') }}"
@endif

@if(! empty(array_get($button, 'value')))
value="{{ array_get($button, 'value', '') }}"
@endif

@if(! empty(array_get($button, 'form')))
form="{{ array_get($button, 'form', '') }}"
@endif

@elseif (! empty($link))
a
href="{{ array_get($link, 'href', '#') }}"
target="{{ array_get($link, 'target', '_self') }}"
@endif

class="button button-{{ $style or 'fixMe' }}-{{ $state or 'fixMe' }} button-{{ $size or 'medium' }} {{ $classes or '' }}"

@unless(empty($dataAttributes))
@foreach ($dataAttributes as $data)
data-{{ $data['attribute'] }}="{{ $data['value'] }}"
@endforeach
@endunless
>

@if (isset($label['icon']))
<span>
	<fw-font-awesome-icon icon="{{ array_get($label, 'icon', 'chess-board') }}"></fw-font-awesome-icon>
	{{-- <i class="fas fa-{{ array_get($label, 'icon', 'chess-board') }} fa-fw @if (isset($label['text'])) mr-2 @endif"></i> --}}
</span>
@endif

@if (isset($label['text']))
<span>
	{{ array_get($label, 'text', 'This Button Needs Text') }}
</span>
@endif

@isset($overflow)
{!! $overflow !!}
@endunless

</@if (! empty($button))
button
@elseif (! empty($link))
a
@endif>