<div class="card blog">
	<div class="hero">
		<img src="{{ $image or '' }}" alt="{{ $imageAlt or '' }}">
	</div>
	<div class="headings">
		<div class="title">{{ str_limit($title, 45) }}</div>
		<div class="meta">
			{{ $date }}
			by
			<strong>{{ $author }}</strong>
			on
			<strong>{{ $company }}</strong>
		</div>
	</div>
	<div class="summary">
		<span>
			{{ str_limit($summary, 150) }}
		</span>
	</div>
	<div class="action">
		@component ('components.button', [
			'link' => array_dot([
				'href' => $link,
			]),
			'style' => 'text',
			'state' => 'info',
			'size' => 'small',
			'label' => array_dot([
				'icon' => 'glasses',
				'text' => 'Read More'
			])
		])
		@endcomponent
	</div>
</div>