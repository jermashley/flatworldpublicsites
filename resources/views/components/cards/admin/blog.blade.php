<div class="card blog-admin">
	<div class="hero">
		<img src="{{ $image or '' }}" alt="{{ $imageAlt or '' }}">
	</div>

	<div class="summary">
		<div class="meta">
			{{ $date }}
		</div>

		<div class="title">
			{{ str_limit($title, 45) }}
		</div>

		<div class="meta">
			<span>
				Published by
				<strong>{{ $author }}</strong>
				on
				<strong>{{ $company }}</strong>
			</span>
		</div>
	</div>

	<div class="actions">
		<div style="margin: 0 16px;">
			@component ('components.tag', [
				'state' => $tagState,
				'text' => $tagText
			])
			@endcomponent
		</div>

		@component ('components.button', [
			'link' => array_dot([
				'href' => $link
			]),
			'style' => 'text',
			'state' => 'dark',
			'size' => 'small',
			'label' => array_dot([
				'icon' => 'cog',
				'text' => ''
			])
		])
		@endcomponent
	</div>
</div>