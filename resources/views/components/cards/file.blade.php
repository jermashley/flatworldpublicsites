<div class="card file">
	<div class="icon">
		<span>
			<fw-font-awesome-icon icon="file-{{ $fileicon or '' }}"></fw-font-awesome-icon>
			{{-- <i class="far fa- fa-fw"></i> --}}
		</span>
	</div>
	<div class="summary">
		<div class="filename">
			{{ str_limit($filename or '', 35) }}
		</div>
		<div class="filesize">
			2MB
		</div>
		<div class="filetype">
			Microsoft Word Document
		</div>
	</div>
	<div class="action">
		@component ('components.button', [
			'link' => array_dot([
				'href' => '#',
			]),
			'style' => 'text',
			'size' => 'small',
			'state' => 'dark',
			'label' => array_dot([
				'icon' => 'cloud-download',
			]),
		])
		@endcomponent
	</div>
</div>