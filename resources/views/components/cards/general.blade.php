<div class="card general">
    <div class="hero">
            <img src="{{ $image or '' }}" alt="{{ $imageAlt or '' }}">
    </div>
    <div class="heading">
        <span>{{ str_limit($title, 45) }}</span>
    </div>
    <div class="summary">
			{{ str_limit($summary, 120) }}
    </div>
    <div class="action">
        @component ('components.button', [
            'link' => array_dot([
                'href' => $link,
            ]),
            'style' => 'text',
            'size' => 'small',
            'state' => 'info',
            'label' => array_dot([
                'icon' => 'glasses',
                'text' => 'Read More',
            ]),
        ])
        @endcomponent
    </div>
</div>