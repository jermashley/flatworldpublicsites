<div class="overflow">
	@foreach ($links as $link)
	<div class="link">
		<a href="{{ $link['href'] }}" class="link">{{ $link['text'] }}</a>
	</div>
	@endforeach
</div>