<div class="userProfileGrid">
	@foreach ($userProfile as $value)
	<div class="row">
		<div class="rowTitle">
			{{ $value['rowTitle'] }}
		</div>

		<div class="rowValue">
			{!! $value['rowValue'] !!}
		</div>
	</div>
	@endforeach
</div>