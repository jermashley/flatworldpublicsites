@unless (empty($linkedin))
<a href="{{ $linkedin }}" class="link">
  <fw-font-awesome-icon icon="linkedin"></fw-font-awesome-icon>
  {{-- <i class="fab fa-linkedin"></i> --}}
</a>
@endunless

@unless (empty($twitter))
<a href="{{ $twitter }}" class="link">
  <fw-font-awesome-icon icon="twitter"></fw-font-awesome-icon>
  {{-- <i class="fab fa-twitter"></i> --}}
</a>
@endunless

@unless (empty($facebook))
<a href="{{ $facebook }}" class="link">
  <fw-font-awesome-icon icon="facebook"></fw-font-awesome-icon>
  {{-- <i class="fab fa-facebook"></i> --}}
</a>
@endunless

@unless (empty($instagram))
<a href="{{ $instagram }}" class="link">
  <fw-font-awesome-icon icon="instagram"></fw-font-awesome-icon>
  {{-- <i class="fab fa-instagram"></i> --}}
</a>
@endunless