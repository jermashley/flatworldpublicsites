<div class="card {{ $cardSize }} {{ $leadImageLocation or '' }} {{ $hasShadow or '' }} {{ $isAnimated or '' }} {{ $cardClasses or '' }}" @unless (empty($cardId)) id="{{ $cardId }}" @endunless>

    @unless (empty($hasLeadImage))
    <div class="lead">

        @if (Auth::check())
        <div class="adminControls">

            <div class="company-name">
                <span style="display: block;">{{ $companyName }}</span>
            </div>

            <a href="{{ $editLink }}" class="adminControls__button edit">
                <fw-font-awesome-icon icon="pen"></fw-font-awesome-icon>
                {{-- <i class="fal fa-pen fa-fw"></i> --}}
            </a>

            <div class="adminControls__button delete" data-toggle="delete-post">
                <fw-font-awesome-icon icon="trash-alt"></fw-font-awesome-icon>
                {{-- <i class="fal fa-trash-alt fa-fw"></i> --}}

            </div>

            <div class="adminControls__button confirmDelete" data-toggle="confirm-delete" style="display: none;">

                <a href="{{ $deleteLink }}" class="confirmYes">Yes</a>

                <span class="confirmNo">No</span>

            </div>

            <div class="company-name">
                <span style="display: block;">{{ $publishStatus }}</span>
            </div>

        </div>
        @endif

        <img src="{{ $leadImageSource or '' }}" class="imageBg {{ $leadImageFit or '' }} {{ $leadImagePosition or '' }} {{ $leadImageIsDimmed or '' }}" alt="{{ $leadImageDescription or '' }}">

    </div>
    @endunless

    <div class="cardContent">

        @unless (empty($timestamp))
        <span class="timestamp">{{ $timestamp or '' }}</span>
        @endunless

        @unless (empty($cardHeading))
        <span class="cardContent__heading {{ $headingSize or '' }}">
            {{ str_limit($cardHeading, 35) }}
        </span>
        @endunless

        @unless (empty($hasDivider))
        <span class="cardContent__divider"></span>
        @endunless

        @unless (empty($cardText))
        <span class="cardContent__text {{ $textSize or '' }}">
            {{ str_limit($cardText) }}
        </span>
        @endunless

        @unless (empty($hasAction))
        <a href="{{ $actionLink or '#' }}" class="cardContent__action {{ $actionClasses or 'link' }}">
            {{ $actionText or 'Read More' }}
        </a>
        @endunless

    </div>

</div>
