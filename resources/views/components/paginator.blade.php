@if ($paginator->hasPages())
    <div class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <a class="icon item disabled">
                <fw-font-awesome-icon icon="chevron-double-left"></fw-font-awesome-icon>
                {{-- <i class="fas fa- fa-fw"></i> --}}
            </a>
        @else
            <a class="icon item" href="{{ $paginator->previousPageUrl() }}" rel="prev">
                <fw-font-awesome-icon icon="chevron-double-left"></fw-font-awesome-icon>
                {{-- <i class="fas fa- fa-fw"></i> --}}
            </a>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <a class="icon item disabled">{{ $element }}</a>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <a class="item active" href="{{ $url }}">{{ $page }}</a>
                    @else
                        <a class="item" href="{{ $url }}">{{ $page }}</a>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <a class="icon item" href="{{ $paginator->nextPageUrl() }}" rel="next">
                <fw-font-awesome-icon icon="chevron-double-right"></fw-font-awesome-icon>
                {{-- <i class="fas fa- fa-fw"></i> --}}
            </a>
        @else
            <a class="icon item disabled">
                <fw-font-awesome-icon icon="chevron-double-right"></fw-font-awesome-icon>
                {{-- <i class="fas fa-chevron-double-right fa-fw"></i> --}}
            </a>
        @endif
    </div>
@endif
