@extends('app.updated-layout')
@section('hero')

<div id="hero">

    <div class="hero content">

        <div class="logo">

            <img src="/images/common/gs-full-logo-light.png" alt="">

        </div>

        <div class="snippet cta">

            <span>Driving Innovation</span>

        </div>

    </div>

</div>
@endsection

@section('content')

<div id="home">

    <h1 class="text dark left heading h1 uppercase font-weight-700 mt-5">Flat World Global Solutions</h1>

    <p class="text dark left paragraph general">Excellence through industry expertise, best in-class customer service and leading-edge technology. At Flat World Supply
        Chain, we generate highly optimized and customized supply chain solutions for our clients with unified technology
        systems integrated through our global network.</p>

    <div class="d-flex flex-column">
        <div class="d-flex flex-column">
            <div class="splashImage">

                <h3>transportation insight and technology tools</h3>
                <h3 class="em">enhance your ability to make quick and informed decisions</h3>

                <img src="{{ Brand::asset('images/truck-highway-2-2048.jpg') }}" class="imageBg cover center" alt="">

            </div>
        </div>
    </div>

    <p class="text dark left paragraph general">Our solutions are delivered through proprietary software that is completely customizable and is able to integrate with almost any Enterprise Resource Planning (ERP) or operating system.</p>

    <p class="text dark left paragraph general">Flat World provides transportation insight and technology tools to enhance our client's ability to make quick and informed decisions. We help to drive efficiencies throughout our client's business processes; including PO management, optimized carrier selection, transportation execution tasks, and freight bill audit and payment.</p>

    <p class="text dark left paragraph general">Flat World provides the traditional logistics services of <a href="{{ route(config('app.prefix').'.solutions.tl') }}" class="link">Truckload Brokerage</a>, <a href="{{ route(config('app.prefix').'.solutions.ltl') }}" class="link">LTL (Less-than-Truckload) Management</a>, and <a href="{{ route(config('app.prefix').'.solutions.expedited') }}" class="link">Expedited Shipping</a>, along with more specialized services such as Project Management, Warehousing, and Installation work through <a href="https://flatworldhospitality.com" target="_blank" class="link">Flat World Global Solutions Project Management Division</a>. All of the services provided by Flat World Global Solutions are supported by a diverse suite of technology tools that can be integrated seamlessly with any client operating system or ERP.</p>

    <p class="text dark left paragraph general">Flat World's value proposition is in providing the safest, most efficient, and most cost effective method for moving our client's products to market. Flat World combines customized supply chain management, innovative technology solutions, and the optimum combination of marketplace price and service for client's logistics needs.</p>

    <video width="100%" height="auto" poster="https://fwholdingsblog.s3.amazonaws.com/public-sites/supplychain-placeholder.jpg" controls>
            <source src="https://fwholdingsblog.s3.amazonaws.com/public-sites/supplychain-video.mp4" />
        </video>

    <h4 class="text dark left heading h4 uppercase font-weight-700 mt-5">Flat World Global Solutions</h4>

    <p class="text dark left paragraph general mb-5">Choice, visibility, service level, and net costs are always at the forefront of the transportation professional’s mind.
        Flat World Global Solutions, through a strategic partnership with <a href="http://prologuetechnology.com/" target="_blank"
            class="link">Prologue Technology&trade;</a> provides optimum solutions to challenging transportation problems through
        the use of cutting edge logistics technology. Flat World is a consumer-driven company that provides enterprise software
        applications designed to help our clients deliver a superior customer experience to their customer, while enhancing
        their productivity, profitability, performance and competitiveness.</p>

    <div class="d-flex flex-row flex-wrap justify-content-around">
        {{-- Less-than-Truckload --}}
        <fw-card-general title="Less-Than-Truckload" image="{{ Brand::asset('images/%s/illustrations/ltl.jpg') }}" image-alt="Less-than-truckload art."
            summary="Choice is critical to a cost effective, yet service sensitive supply chain. Utilizing a menu of LTL carriers to attain optimum service performance."
            button-icon="glasses" button-text="Learn More" href="{{ route(config('app.prefix').'.solutions.ltl') }}" contain-image>
        </fw-card-general>

        {{-- Truckload --}}
        <fw-card-general title="Truckload" image="{{ Brand::asset('images/%s/illustrations/tl.jpg') }}" image-alt="Truckload art."
            summary="Flat World provides one of the easiest, most cost-effective and dependable full truckload shipping solutions in the industry."
            button-icon="glasses" button-text="Learn More" href="{{ route(config('app.prefix').'.solutions.tl') }}" contain-image>
        </fw-card-general>

        {{-- Expedited --}}
        <fw-card-general title="Expedited" image="{{ Brand::asset('images/%s/illustrations/expedited.jpg') }}" image-alt="Expedited art."
            summary="Flat World provides premium expedited shipping services, including ground and air freight forwarding services."
            button-icon="glasses" button-text="Learn More" href="{{ route(config('app.prefix').'.solutions.expedited') }}" contain-image>
        </fw-card-general>

        {{-- Hospitality Services --}}
        <fw-card-general title="Hospitality Services" image="{{ Brand::asset('images/%s/illustrations/hospitality.jpg') }}" image-alt="Hospitality Services art."
            summary="Flat World provides premium hospitality services, including PO and inbound shipment management of FF&E from vendor to project site."
            button-icon="glasses" button-text="Learn More" href="{{ route(config('app.prefix').'.solutions.hospitality') }}"
            contain-image>
        </fw-card-general>

        {{-- International Services --}}
        <fw-card-general title="International Services" image="{{ Brand::asset('images/%s/illustrations/international.jpg') }}" image-alt="International Services art."
            summary="We handle import and export for all ocean (FCL/LCL) and air shipments all over the world." button-icon="glasses"
            button-text="Learn More" href="{{ route(config('app.prefix').'.solutions.international') }}" contain-image>
        </fw-card-general>

        {{-- Return Goods Management --}}
        <fw-card-general title="Return Goods Management" image="{{ Brand::asset('images/%s/illustrations/returns.jpg') }}" image-alt="Return Goods Management art."
            summary="Our Return Goods Management services include complete coordination of return goods to a central distribution facility, to multiple distribution facilities, or to a secondary market."
            button-icon="glasses" button-text="Learn More" href="{{ route(config('app.prefix').'.solutions.returns') }}" contain-image>
        </fw-card-general>
    </div>

</div>
@endsection
