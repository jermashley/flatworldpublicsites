@extends('app.updated-layout')
@section('title', 'Return Goods Management | ')
@section('content')

<h1 class="page__title my-1">Return Goods Management</h1>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3 class="em">We provide an effective product returns strategy</h3>

        <img src="{{ Brand::asset('images/truck-highway-1-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<p class="text dark left paragraph general">Product returns have long been viewed by companies as a necessary evil, a painful process, a cost center, and an area of
    potential customer dissatisfaction. However, many successful business organizations have realized that an effective product
    returns strategy can provide a number of benefits, such as improved customer service and customer knowledge, effective
    inventory management and product dispositioning.</p>

<p class="text dark left paragraph general">Visibility to your returns, or rather the lack-there-of, may be costing your company more money than you think. Flat World
    Global Solutions's Return Goods Management services can impact your bottom line in a very big way. Many companies are now
    placing a high priority on gaining better visibility to their Inbound Returned Goods by utilizing the technology tools
    available through Flat World Global Solutions.</p>

<p class="text dark left paragraph general">We scale a Return Goods program to fit each customer's needs. We'll work with you to develop a Standard Operating Procedure
    (SOP) and then we execute on that SOP. All through the process, we provide reporting on KPI's (Key Performance Indicators)
    to give you the information that is important to your company.</p>

<h3 class="text dark left uppercase heading h3 font-weight-600 mt-5">How do we receive the information about your returns?</h3>

<ul class="generalList styled right-arrow-circle">
    <li>Your customers or you calling Flat World directly</li>
    <li>Your customers calling a toll free number that we answer with your name</li>
    <li>You or your customers faxing us</li>
    <li>You or your customers e-mailing us</li>
    <li>Communications via electronic portal directly into our TMS</li>
</ul>

<h3 class="text dark left uppercase heading h3 font-weight-600 mt-5">Reduce expenses on your returns through Flat World's Return Goods services:</h3>

<ul class="list-unstyled generalList">
    <li>
        <h4 class="text dark left uppercase heading h4 font-weight-800 mt-3">Technology</h4>
        <ul class="generalList styled right-arrow-circle">
            <li>Proprietary project management software allows for a customized solution designed specific to each client’s needs</li>
        </ul>
    </li>

    <li>
        <h4 class="text dark left uppercase heading h4 font-weight-800 mt-3">Project Management</h4>
        <ul class="generalList styled right-arrow-circle">
            <li>Coordination of returns</li>
            <li>Delivery exception management (OS&D)</li>
            <li>Coordination of recalls and withdrawals</li>
            <li>Asset recovery and inventory control</li>
            <li>Home-site recovery</li>
            <li>Return center consolidation</li>
        </ul>
    </li>

    <li>
        <h4 class="text dark left uppercase heading h4 font-weight-800 mt-3">Resource Network</h4>
        <ul class="generalList styled right-arrow-circle">
            <li>Return centers throughout North America</li>
            <li>Closeout specialists</li>
            <li>Donation facilities</li>
            <li>Destruction/disposal operations reporting the information back to you</li>
        </ul>
    </li>

    <li>
        <h4 class="text dark left uppercase heading h4 font-weight-800 mt-3">Reporting</h4>
        <ul class="generalList styled right-arrow-circle">
            <li>We provide monthly reports showing what products each client returned</li>
            <li>Reasons for returns</li>
            <li>Custom Reports allow for; product count, customer, pickup locations, and many other data fields</li>
        </ul>
    </li>
</ul>
@endsection
