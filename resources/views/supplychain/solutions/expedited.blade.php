@extends('app.updated-layout') 
@section('title', 'Expedited | ') 
@section('content')

<h1 class="page__title my-1">Expedited</h1>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>ground and air freight forwarding services</h3>
        <h3 class="em">real-time visibility throughout your supply chain</h3>

        <img src="{{ Brand::asset('images/airplane-tarmac-1-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<p class="text dark left paragraph general">Flat World provides premium expedited shipping services, including ground and air freight forwarding services. We also offer
    dedicated door-to-door premium freight services to and from any location in North America. Our technology, network strength,
    and world class customer service provided through our easy to use web-based quoting, booking, and satellite tracking
    platform provides real-time visibility throughout your supply-chain.</p>

<p class="text dark left paragraph general">Regardless of the mode, service or pick-up and delivery locations, we are confident in our ability to provide the most cost
    effective supply-chain solutions for your time sensitive and high value freight.</p>
@endsection