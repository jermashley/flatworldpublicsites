@extends('app.updated-layout')

@section('title', 'Solutions | ')

@section('content')

    <h1 class="page__title my-1">Solutions</h1>

    <h3 class="page__subTitle my-1">Flat World Global Solutions offers a variety of solutions for your domestic and international logistics needs</h3>

    <p class="text dark left paragraph general">Flat World provides transportation insight and technology tools to enhance our client's ability to make quick and informed decisions. We help to drive efficiencies throughout our client's business processes; including PO management, optimized carrier selection, transportation execution tasks, and freight bill audit and payment. Flat World provides the traditional logistics services of Truckload Brokerage, LTL (Less-than-Truckload) Management, and Expedited Shipping, along with more specialized services such as Project Management, Warehousing, and Installation work through Flat World Global Solutions. All of the services provided by Flat World Global Solutions are supported by a diverse suite of technology tools that can be integrated seamlessly with any client operating system or ERP.</p>


    <div class="d-flex flex-row flex-wrap justify-content-around">
        {{--  Less-than-Truckload  --}}
        @component('components.cards.general', [
            'cardClasses' => 'medium header mx-2 mb-5',
            'hasShadow' => true,
            'isAnimated' => true,
            'hasHero' => true,
            'hasContent' => true,
            'hasFooter' => true,
            'hasActions' => true,
            'heroImageClasses' => 'contain center',
            'heroImageSource' => Brand::asset('images/%s/illustrations/ltl.jpg'),
            'cardHeading' => 'Less-Than-Truckload (LTL)',
            'headingClasses' => 'text dark center uppercase heading h4 font-weight-600',
            'cardText' => 'Choice is critical to a cost effective, yet service sensitive supply chain. Utilizing a menu of LTL carriers to attain optimum service performance.',
            'textClasses' => 'text dark center heading h5 font-weight-400 mt-4',
            'actions' => [
                [
                    'text' => 'Read More',
                    // 'link' => route('solutions.ltl'),
                    'classes' => 'link mt-4',
                ]
            ]
        ])
        @endcomponent

        {{--  Truckload  --}}
        @component('components.cards.general', [
            'cardClasses' => 'medium header mx-2 mb-5',
            'hasShadow' => true,
            'isAnimated' => true,
            'hasHero' => true,
            'hasContent' => true,
            'hasFooter' => true,
            'hasActions' => true,
            'heroImageClasses' => 'contain center',
            'heroImageSource' => Brand::asset('images/%s/illustrations/tl.jpg'),
            'cardHeading' => 'Truckload (TL)',
            'headingClasses' => 'text dark center uppercase heading h4 font-weight-600',
            'cardText' => 'Flat World provides one of the easiest, most cost-effective and dependable full truckload shipping solutions in the industry.',
            'textClasses' => 'text dark center heading h5 font-weight-400 mt-4',
            'actions' => [
                [
                    'text' => 'Read More',
                    // 'link' => route('solutions.tl'),
                    'classes' => 'link mt-4',
                ]
            ]
        ])
        @endcomponent

        {{--  Expedited  --}}
        @component('components.cards.general', [
            'cardClasses' => 'medium header mx-2 mb-5',
            'hasShadow' => true,
            'isAnimated' => true,
            'hasHero' => true,
            'hasContent' => true,
            'hasFooter' => true,
            'hasActions' => true,
            'heroImageClasses' => 'contain center',
            'heroImageSource' => Brand::asset('images/%s/illustrations/expedited.jpg'),
            'cardHeading' => 'Expedited',
            'headingClasses' => 'text dark center uppercase heading h4 font-weight-600',
            'cardText' => 'Flat World provides premium expedited shipping services, including ground and air freight forwarding services.',
            'textClasses' => 'text dark center heading h5 font-weight-400 mt-4',
            'actions' => [
                [
                    'text' => 'Read More',
                    // 'link' => route('solutions.expedited'),
                    'classes' => 'link mt-4',
                ]
            ]
        ])
        @endcomponent

        {{--  Hospitality Services  --}}
        @component('components.cards.general', [
            'cardClasses' => 'medium header mx-2 mb-5',
            'hasShadow' => true,
            'isAnimated' => true,
            'hasHero' => true,
            'hasContent' => true,
            'hasFooter' => true,
            'hasActions' => true,
            'heroImageClasses' => 'contain center',
            'heroImageSource' => Brand::asset('images/%s/illustrations/hospitality.jpg'),
            'cardHeading' => 'Hospitality Services',
            'headingClasses' => 'text dark center uppercase heading h4 font-weight-600',
            'cardText' => 'Flat World provides premium hospitality services, including po and inbound shipment management of FF&E from vendor to project site.',
            'textClasses' => 'text dark center heading h5 font-weight-400 mt-4',
            'actions' => [
                [
                    'text' => 'Read More',
                    // 'link' => route('solutions.hospitality'),
                    'classes' => 'link mt-4',
                ]
            ]
        ])
        @endcomponent

        {{--  International Services  --}}
        @component('components.cards.general', [
            'cardClasses' => 'medium header mx-2 mb-5',
            'hasShadow' => true,
            'isAnimated' => true,
            'hasHero' => true,
            'hasContent' => true,
            'hasFooter' => true,
            'hasActions' => true,
            'heroImageClasses' => 'contain center',
            'heroImageSource' => Brand::asset('images/%s/illustrations/international.jpg'),
            'cardHeading' => 'International Services',
            'headingClasses' => 'text dark center uppercase heading h4 font-weight-600',
            'cardText' => 'We handle import and export for all ocean (FCL/LCL) and air shipments all over the world.',
            'textClasses' => 'text dark center heading h5 font-weight-400 mt-4',
            'actions' => [
                [
                    'text' => 'Read More',
                    // 'link' => route('solutions.international'),
                    'classes' => 'link mt-4',
                ]
            ]
        ])
        @endcomponent

        {{--  Return Goods Management  --}}
        @component('components.cards.general', [
            'cardClasses' => 'medium header mx-2 mb-5',
            'hasShadow' => true,
            'isAnimated' => true,
            'hasHero' => true,
            'hasContent' => true,
            'hasFooter' => true,
            'hasActions' => true,
            'heroImageClasses' => 'contain center',
            'heroImageSource' => Brand::asset('images/%s/illustrations/returns.jpg'),
            'cardHeading' => 'Return Goods Management',
            'headingClasses' => 'text dark center uppercase heading h4 font-weight-600',
            'cardText' => 'Our Return Goods Management services include complete coordination of return goods to a central distribution facility, to multiple distribution facilities, or to a secondary market.',
            'textClasses' => 'text dark center heading h5 font-weight-400 mt-4',
            'actions' => [
                [
                    'text' => 'Read More',
                    // 'link' => route('solutions.returns'),
                    'classes' => 'link mt-4',
                ]
            ]
        ])
        @endcomponent
    </div>

@endsection
