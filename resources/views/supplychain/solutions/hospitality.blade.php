@extends('app.updated-layout')
@section('title', 'Hospitality Logistics Services | ')
@section('content')

<h1 class="page__title my-1">Hospitality Logistics Services</h1>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3 class="em">provide our expertise to local, national, and international companies across the globe</h3>

        <img src="{{ Brand::asset('images/hotel-lobby-1-2048.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<div class="content__featureWall">

    <header class="featureWall__header">
        <span>Project Management</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">

        <h3>Services Provided</h3>

        <ul class="generalList styled right-arrow-circle">
            <li>Purchase Order Management</li>
            <li>Inbound shipment management of FF&E from vendor to project site</li>
            <li>Coordinate transfer from warehouse to project site</li>
            <li>Invoice audit and cost management for all aspects of the project</li>
            <li>Cargo claims management - We file all claims and follow through payment</li>
            <li>24 hour customer service provided by highly experienced professionals dedicated to your project</li>
        </ul>

    </div>

    <div class="featureWall__blockHalf">

        <h3>Value Proposition</h3>

        <ul class="generalList styled right-arrow-circle">
            <li>One contact from P.O. conception to final installation</li>
            <li>Comprehensive on-line reporting (P.O. status, Bill of Lading status, delivery status, warehouse inventory, project
                progress status, project cost)</li>
            <li>Direct management of all transportation needs (Less-than-Truckload, Truckload, Expedited, Blanket Wrap, Import/Export,
                Small Package)</li>
            <li>Highly trained and experienced team of professionals working to provide best-in-class experience for every project</li>
        </ul>

    </div>

</div>

<div class="content__featureWall">

    <header class="featureWall__header">
        <span>Warehousing and Installation</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">

        <h3>Services Provided</h3>

        <ul class="generalList styled right-arrow-circle">
            <li>Storage (Public, Contract, and Seasonal Warehousing Available)</li>
            <li>Container Loading and Unloading</li>
            <li>Cross Docking</li>
            <li>Warehouse Management System (Inventory Control)</li>
            <li>Pick and Pack</li>
            <li>Break-Bulk Shipping</li>
            <li>J.I.T. (Just-In-Time)</li>
            <li>Sub Assembly / Kitting</li>
            <li>Special Project Assistance</li>
            <li>Small Package Shipments (e.g. UPS, FedEx)</li>
            <li>Expedited Will Call/Customer Pick-Up Service</li>
            <li>Delivery to Job Site</li>
            <li>Installation of Furniture, Fixtures and Equipment</li>
        </ul>

    </div>

    <div class="featureWall__blockHalf">

        <h3>Value Proposition</h3>

        <p class="text dark left paragraph general">Exceeding our customer's expectation is our mission. Tailoring a project specific solution for our customer's warehousing and installation needs and then actively monitoring each project is the way we accomplish that mission. Today Flat World Global Solutions is proud to provide our warehousing and installation expertise to local, national, and international companies across the globe. We've spent the past decade building our service reputation, and we understand superior service is the cornerstone of being an effective partner to our customers.</p>

    </div>

</div>

<div class="content__featureWall">

    <header class="featureWall__header">
        <span>Transportation Management</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">

        <h3>Services Provided</h3>

        <ul class="generalList styled right-arrow-circle">
            <li>Needs Analysis</li>
            <li>
                Carrier Contract Negotiations
                <ul class="generalList styled right-arrow-circle">
                    <li>Less than Truckload (LTL)</li>
                    <li>Truckload (TL)</li>
                    <li>Blanket Wrap Service</li>
                </ul>
            </li>
            <li>Routing Optimization</li>
            <li>Execute shipments with all required documentation & electronically submit instructions to carrier</li>
            <li>Shipment coordination from pickup to delivery with statuses visible throughout</li>
            <li>Consolidation Services</li>
            <li>Inbound / Reverse Logistics</li>
        </ul>

    </div>

    <div class="featureWall__blockHalf">

        <h3>Value Proposition</h3>

        <ul class="generalList styled right-arrow-circle">
            <li>Provide transportation management tools that create efficiencies, provide better information, and reduce overall
                costs in your supply chain</li>
            <li>Improve your transportation cost structures while maintaining best in class delivered service</li>
            <li>Provide technology solutions to improve your team’s productivity and efficiency</li>
            <li>Ensure actual costs meet expectations, and align those costs to your business activities</li>
            <li>Provide on-line and custom reporting for entire project</li>
            <li>Continuous review and adjustment to insure our transportation strategy is bringing value to your business</li>
        </ul>

    </div>

</div>

<fw-link href="http://flatworldhospitality.com/" target="_blank" design="text" state="info" size="small" icon="external-link-square">
    Visit Flat World Global Solutions Project Management for more detailed information
</fw-link>
@endsection
