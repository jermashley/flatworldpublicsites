@extends('app.updated-layout') 
@section('title', 'Truckload (TL) | ') 
@section('content')

<h1 class="page__title my-1">Truckload (TL)</h1>

{{--
<h3 class="page__subTitle my-1">A pretty little snippet that gives the gist of the page</h3> --}}

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3>transportation insight and technology tools</h3>
        <h3 class="em">enhance you ability to make quick and informed decisions</h3>

        <img src="{{ Brand::asset('images/truck-highway-3.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<p class="text dark left paragraph general">Flat World provides one of the easiest, most cost-effective and dependable full truckload shipping solutions in the industry.
    Unlike asset-based providers, where the main priority is the utilization of their own company's assets, the main objective
    for Flat World is to provide clients with the optimum combination of capacity, price, and ease of engagement. We help
    to remove the hassle of obtaining transportation services for your full truckload and inter-modal shipments.</p>

<p class="text dark left paragraph general">Long-term relationships with carriers allow Flat World to provide the flexibility required to successfully manage a shipper's
    unexpected volume increases.</p>
@endsection