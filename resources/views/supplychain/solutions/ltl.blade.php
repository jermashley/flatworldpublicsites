@extends('app.updated-layout')
@section('title', 'Less-Than-Truckload (LTL) | ')
@section('content')

<h1 class="page__title my-1">Less-Than-Truckload (LTL)</h1>

<div class="d-flex flex-column">
    <div class="splashImage">

        <h3 class="em">Flat World's Value Proposition</h3>
        <h3>Large enough to demand the best price/value combination, but small enough to offer the customized services and the
            attention you deserve.</h3>

        <img src="{{ Brand::asset('images/truck-hwy-pano.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<p class="text dark left paragraph general">Choice is critical to a cost effective, yet service sensitive supply chain. Utilizing a menu of LTL carriers to attain optimum
    service performance, leveraging volume to ensure competitive prices, and optimizing multiple base rates are all ways
    to better utilize a company's resources. Because your time is valuable, our personal service with dedicated account management,
    along with state of the art technology offers a more efficient way to manage your LTL transportation dollar. Only "best
    in class" National, Inter-regional, and Regional providers are utilized, so you can be confident in the value your company
    will receive from Flat World Global Solutions. We select the best service-providing carriers in the industry for each geographic
    area and combine that with the optimum price for the service required to provide for the greatest value proposition in
    the market.</p>
@endsection
