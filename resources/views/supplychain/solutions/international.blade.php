@extends('app.updated-layout')
@section('title', 'International Services | ')
@section('content')

<h1 class="page__title my-1">International Services</h1>

{{--
<h3 class="page__subTitle my-1">A pretty little snippet that gives the gist of the page</h3> --}}

<div class="d-flex flex-column">
    <div class="splashImage">
        <h3>Move your Goods across the globe</h3>
        <h3 class="em">Turn a complexity into a strategic advantage</h3>

        <img src="{{ Brand::asset('images/shipping-yard-containers-1.jpg') }}" class="imageBg cover top" alt="">

    </div>
</div>



<p class="text dark left paragraph general">Moving cargo across the globe involves tremendous risk and complexity. <a href="https://ram-intl.com/" target="_blank">Flat World Global Solutions International</a>, provides end-to-end, door-to-door service to minimize that risk and turn a complexity into a strategic advantage.</p>

<div class="content__featureWall">

    <header class="featureWall__header">
        <span>Air / Ocean / Ground</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p class="text dark left paragraph general"><a href="http://ram-intl.com/" class="link">Flat World Global Solutions International</a> provides flexible and integrated transportation
                services both internationally and domestically. Our approach begins with our experienced and dedicated team
                that provide skilled expertise in all modes of transportation. By partnering with the most respected and
                trusted carriers, paired with the right people, capacity and pricing Flat World Global Solutions is able to provide
                solutions to our customers transportation needs.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">

            <img src="{{ Brand::asset('images/jet-underbelly-1.jpeg') }}" alt="">

        </div>
    </div>

</div>

<div class="content__featureWall">

    <header class="featureWall__header">
        <span>Specialty / Project Cargo</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p class="text dark left paragraph general">Our specialty/project cargo group offers logistics management and transportation to ensure your freight reaches
                its destination.Our solutions offer the perfect combination of bottom line and deadline. Turn-key solutions
                include door-to-door service - from pickup, assembly & consolidation, totracking, delivery and billing.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">

            <img src="{{ Brand::asset('images/hazardous-1-2048.jpg') }}" alt="">

        </div>
    </div>

</div>

<div class="content__featureWall">

    <header class="featureWall__header">
        <span>Customs &amp; Compliance</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p class="text dark left paragraph general">We pay attention to all the details and utilize our experience,regulatory expertise and the latest technology
                to ensure that your supply chain remains compliant.Our customs brokerage specialist answer questions and
                offer support while coordinating effective transactions between our customers and customs authorities. Our
                mission is to maximize compliance while minimizing costs.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">

            <img src="{{ Brand::asset('images/ship-containers-1.jpg') }}" alt="">

        </div>
    </div>

</div>

<div class="content__featureWall">

    <header class="featureWall__header">
        <span>Custom Crating</span>
        <span></span>
    </header>

    <div class="featureWall__blockHalf">
        <div class="block__text">
            <p class="text dark left paragraph general">Flat World Global Solutions's <a href="http://ramcrating.com/" class="link">Crating Division</a> provides ISPM-15 compliant wood packaging
                that keeps your cargo safe, no matter where the commodity is going. Led by experts who have spent their career
                in the custom crating business, Flat World Global Solutions is your choice for excellence in custom crate construction.</p>
        </div>
    </div>

    <div class="featureWall__blockHalf">
        <div class="block__image">

            <img src="{{ Brand::asset('images/crates.jpg') }}" alt="">

        </div>
    </div>

</div>
@endsection
