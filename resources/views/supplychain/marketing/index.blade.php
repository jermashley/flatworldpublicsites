@extends('app.updated-layout')

@section('title', '| Marketing Materials')

@section('content')

    <h1 class="page__title my-1">Marketing Materials</h1>

    <h3 class="page__subTitle my-1">Find all the marketing materials you need here.  Just click or tap on the card to download a copy of the brochure.</h3>

    <div class="d-flex flex-row flex-wrap justify-content-around">

        @component('components.largeCard')

            @slot('largeCardLink')
                #
            @endslot

            @slot('largeCardImage')
                    https://images.unsplash.com/photo-1426604966848-d7adac402bff?ixlib=rb-0.3.5&q=85&fm=jpg&crop=entropy&cs=srgb&s=871dd87cbe5b3c7aee81322196d4fdfd
            @endslot

            @slot('largeCardClass')
                cover
            @endslot

            @slot('largeCardHeading')
                Pangea&trade; Bi-Fold
            @endslot

            @slot('largeCardContent')
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam animi dolor, eaque esse ex illo impedit maiores necessitatibus nisi nostrum omnis praesentium quae quidem quos repellendus, reprehenderit sed voluptate? Rerum.
            @endslot

            @slot('largeCardAction')
                Download
            @endslot

        @endcomponent

        @component('components.largeCard')

            @slot('largeCardLink')
                #
            @endslot

            @slot('largeCardImage')
                https://images.unsplash.com/photo-1426604966848-d7adac402bff?ixlib=rb-0.3.5&q=85&fm=jpg&crop=entropy&cs=srgb&s=871dd87cbe5b3c7aee81322196d4fdfd
            @endslot

            @slot('largeCardClass')
                cover
            @endslot

            @slot('largeCardHeading')
                Pangea&trade; Bi-Fold
            @endslot

            @slot('largeCardContent')
                Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam animi dolor, eaque esse ex illo impedit maiores necessitatibus nisi nostrum omnis praesentium quae quidem quos repellendus, reprehenderit sed voluptate? Rerum.
            @endslot

            @slot('largeCardAction')
                Download
            @endslot

        @endcomponent

    </div>


@endsection