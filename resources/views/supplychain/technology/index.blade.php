@extends('app.updated-layout') 
@section('title', 'Technology | ') 
@section('content')

<h1 class="page__title my-1">Technology</h1>

<h3 class="page__subTitle my-1">Our technology innovation is the foundation of our transportation industry solutions</h3>

<p class="text dark left paragraph general">Choice, visibility, service level, and net costs are always at the forefront of the transportation professional’s mind, and
    through a strategic partnership with Prologue Technology&trade;, is able to provide solutions that are on the cutting
    edge of logistics technology. Prologue is a company committed to putting many of the functions of an outsourced logistics
    solution back under company control, while also helping to lower transportation/distribution costs and improve efficiency.
    <a href="http://prologuetechnology.com/" target="_blank" class="link">Prologue Technology&trade;</a> powers the <a href="http://prologuetechnology.com/products/pipeline"
        target="_blank" class="link">Pipeline&trade; Transportation Management System (TMS)</a> as a suite of web-native modules
    ranging from a multi-carrier rating engine, to mode/shipment optimization, to shipping execution, to transparent visibility
    tools; while also providing robust database functionality with point-and-click user interface for the practical user.
    Pipeline&trade;, FlatWorld's proprietary TMS system, provides any transportation professional with the tools they need
    to add value to their company and to increase efficiency within their Supply Chain, all in a "pay for use" environment
    that allows for a low cost, quick return on investment (ROI) solution.</p>

<div class="d-flex flex-row flex-wrap justify-content-around mt-5">

    <fw-card-general title="Pipeline TMS" image="{{ Brand::asset('images/%s/pipeline-logo-blue-square.png') }}" imageAlt="" summary="Pipeline is Prologue’s flagship product and is the premier TMS for shippers and Third Party Logistics Companies (3PLs). Shippers can simplify their shipping process by combining their carriers in one system, use the robust rating engine to compare quotes, generate and email bills of lading and shipping labels directly from the system, create electronic pickup requests, view shipment status for all carriers, audit shipments, and gain insight through up-to-date reporting."
        button-icon="" button-text="Learn More" href="#pipeline" contain-image>
    </fw-card-general>

    <fw-card-general title="Pangea&trade;" image="{{ Brand::asset('images/%s/pangea-e.png') }}" imageAlt="" summary="Pangea&trade; offers an improved, streamlined shipment execution workflow by integrating with your hardware, including digital scales, dimentioners, and printers. The result? Dramatically increased efficiency. Pangea&trade; also integrates on the sales order or fulfillment level, allowing customers to easily consolidate multiple orders headed for the same destination, reduces data entry, and eliminates the need to manually enter invoice details for a shipment."
        button-icon="" button-text="Learn More" href="#pangea" contain-image>
    </fw-card-general>

</div>


{{-- Pipeline Product Details --}}

<div class="d-flex flex-column">
    <div class="splashImage" id="pipeline">

        <img src="{{ Brand::asset('images/%s/pipeline-logo-blue.png') }}" class="img-fluid" style="width: 40%;">

        <img src="{{ Brand::asset('images/truck-hwy-pano.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<div class="d-flex flex-row flex-wrap justify-content-around">

    {{-- Rate/Service Comparison --}} {{-- @component('components.cards.general', [ 'cardClasses' => 'small mx-2 mb-5', 'hasShadow'
    => true, 'isAnimated' => true, 'hasContent' => true, 'hasFooter' => true, 'hasActions' => true, 'contentClasses' => 'd-flex
    flex-column justify-content-around', 'cardHeading' => 'Rate/Service Comparison', 'headingClasses' => 'text dark center
    uppercase heading h5', 'textClasses' => 'text dark center heading h3 mt-5', 'actions' => [ [ 'text' => 'Learn More',
    'link' => '#rateServiceComparison', 'classes' => 'link mt-5', ] ] ]) @slot('cardText') <i class="fa fa-chart-bar fa-fw"
        data-fa-transform="grow-10"></i> @endslot @endcomponent --}} {{-- Tracking --}} {{-- @component('components.cards.general',
    [ 'cardClasses' => 'small mx-2 mb-5', 'hasShadow' => true, 'isAnimated' => true, 'hasContent' => true, 'hasFooter' =>
    true, 'hasActions' => true, 'contentClasses' => 'd-flex flex-column justify-content-around', 'cardHeading' => 'Tracking',
    'headingClasses' => 'text dark center uppercase heading h5', 'textClasses' => 'text dark center heading h3 mt-5', 'actions'
    => [ [ 'text' => 'Learn More', 'link' => '#tracking', 'classes' => 'link mt-5', ] ] ]) @slot('cardText') <i class="fa fa-crosshairs fa-fw"
        data-fa-transform="grow-10"></i> @endslot @endcomponent --}} {{-- Reporting --}} {{-- @component('components.cards.general',
    [ 'cardClasses' => 'small mx-2 mb-5', 'hasShadow' => true, 'isAnimated' => true, 'hasContent' => true, 'hasFooter' =>
    true, 'hasActions' => true, 'contentClasses' => 'd-flex flex-column justify-content-around', 'cardHeading' => 'Reporting',
    'headingClasses' => 'text dark center uppercase heading h5', 'textClasses' => 'text dark center heading h3 mt-5', 'actions'
    => [ [ 'text' => 'Learn More', 'link' => '#reporting', 'classes' => 'link mt-5', ] ] ]) @slot('cardText') <i class="fa fa-chart-line fa-fw"
        data-fa-transform="grow-10"></i> @endslot @endcomponent --}} {{-- Consolidated Invoicing --}} {{-- @component('components.cards.general',
    [ 'cardClasses' => 'small mx-2 mb-5', 'hasShadow' => true, 'isAnimated' => true, 'hasContent' => true, 'hasFooter' =>
    true, 'hasActions' => true, 'contentClasses' => 'd-flex flex-column justify-content-around', 'cardHeading' => 'Consolidated
    Invoicing', 'headingClasses' => 'text dark center uppercase heading h5', 'textClasses' => 'text dark center heading h3
    mt-5', 'actions' => [ [ 'text' => 'Learn More', 'link' => '#consolidation', 'classes' => 'link mt-5', ] ] ]) @slot('cardText')
    <i class="fa fa-file-pdf fa-fw" data-fa-transform="grow-10"></i> @endslot @endcomponent --}}

</div>

<div class="content__featureWall" id="rateServiceComparison">

    <header class="featureWall__header">

        <span>Rate / Service Compensation</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <ul class="generalList styled">
            <li>Top tier transportation providers selected by the client and chosen specifically for the clients needs</li>
            <li>Regional, Super-Regional, and National Carriers</li>
            <li>Standard and guaranteed service offerings available</li>
            <li>Actual cost per shipment negotiated specifically for each client</li>
            <li>Accurate transit times for each carrier</li>
        </ul>

    </div>

    <div class="featureWall__blockHalf">

        <div class="block__image" data-featherlight="{{ Brand::asset('images/%s/pipeline-screenshots/screen_rate-service.jpg') }}">

            <img src="{{ Brand::asset('images/%s/pipeline-screenshots/screen_rate-service.jpg') }}" alt="">

        </div>

    </div>

</div>

<div class="content__featureWall" id="tracking">

    <header class="featureWall__header">

        <span>Tracking</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <ul class="generalList styled">
            <li>Shipment tracking information available electronically for all shipments from all carriers</li>
            <li>Tracking data can be integrated and sent electronically to client’s ERP or internal operating system</li>
        </ul>

    </div>

    <div class="featureWall__blockHalf">

        <div class="block__image" data-featherlight="{{ Brand::asset('images/%s/pipeline-screenshots/screen_tracking.jpg') }}">

            <img src="{{ Brand::asset('images/%s/pipeline-screenshots/screen_tracking.jpg') }}" alt="">

        </div>

    </div>

</div>

<div class="content__featureWall" id="reporting">

    <header class="featureWall__header">

        <span>Reporting &amp; Data Mining</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <ul class="generalList styled">
            <li>Standard & custom on-demand reporting</li>
            <li>Ad-hoc data mining capability allows client to access the data that is meaningful to them</li>
        </ul>

    </div>

    <div class="featureWall__blockHalf">

        <div class="block__image" data-featherlight="{{ Brand::asset('images/%s/pipeline-screenshots/screen_reporting.jpg') }}">

            <img src="{{ Brand::asset('images/%s/pipeline-screenshots/screen_reporting.jpg') }}" alt="">

        </div>

    </div>

</div>

<div class="content__featureWall" id="consolidation">

    <header class="featureWall__header">

        <span>Consolidated Invoicing</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <ul class="generalList styled">
            <li>Flexible invoicing capabilities by location, by mode, by week, individually, or by batch</li>
            <li>Customized to include necessary accounting information or coding</li>
            <li>Invoicing data available for export into customer accounting systems, as well as available for pre-audit and/or
                GL code allocation</li>
        </ul>

    </div>

    <div class="featureWall__blockHalf">

        <div class="block__image" data-featherlight="{{ Brand::asset('images/%s/pipeline-screenshots/screen_invoicing.jpg') }}">

            <img src="{{ Brand::asset('images/%s/pipeline-screenshots/screen_invoicing.jpg') }}" alt="">

        </div>

    </div>

</div>



{{-- Pangea&trade; Product Details --}}

<div class="d-flex flex-column">
    <div class="splashImage" id="pangea">

        <img src="{{ Brand::asset('images/%s/pangea-text-logo-white.png') }}" class="img-fluid" style="width: 40%;">

        <img src="{{ Brand::asset('images/warehouse.jpg') }}" class="imageBg cover center" alt="">

    </div>
</div>

<div class="d-flex flex-row flex-wrap justify-content-around">

    {{-- Consolidate Shipments --}} {{-- @component('components.cards.general', [ 'cardClasses' => 'small mx-2 mb-5', 'hasShadow'
    => true, 'isAnimated' => true, 'hasContent' => true, 'hasFooter' => true, 'hasActions' => true, 'contentClasses' => 'd-flex
    flex-column justify-content-around', 'cardHeading' => 'Consolidate Shipments', 'headingClasses' => 'text dark center
    uppercase heading h5', 'textClasses' => 'text dark center heading h3 mt-5', 'actions' => [ [ 'text' => 'Learn More',
    'link' => '#consolidateShipments', 'classes' => 'link mt-5', ] ] ]) @slot('cardText') <i class="fa fa-code-merge fa-fw"
        data-fa-transform="grow-10"></i> @endslot @endcomponent --}} {{-- Save Time --}} {{-- @component('components.cards.general',
    [ 'cardClasses' => 'small mx-2 mb-5', 'hasShadow' => true, 'isAnimated' => true, 'hasContent' => true, 'hasFooter' =>
    true, 'hasActions' => true, 'contentClasses' => 'd-flex flex-column justify-content-around', 'cardHeading' => 'Save Time',
    'headingClasses' => 'text dark center uppercase heading h5', 'textClasses' => 'text dark center heading h3 mt-5', 'actions'
    => [ [ 'text' => 'Learn More', 'link' => '#timeSaving', 'classes' => 'link mt-5', ] ] ]) @slot('cardText') <i class="fa fa-clock fa-fw"
        data-fa-transform="grow-10"></i> @endslot @endcomponent --}} {{-- Increase Profitability --}} {{-- @component('components.cards.general',
    [ 'cardClasses' => 'small mx-2 mb-5', 'hasShadow' => true, 'isAnimated' => true, 'hasContent' => true, 'hasFooter' =>
    true, 'hasActions' => true, 'contentClasses' => 'd-flex flex-column justify-content-around', 'cardHeading' => 'Increase
    Profitability', 'headingClasses' => 'text dark center uppercase heading h5', 'textClasses' => 'text dark center heading
    h3 mt-5', 'actions' => [ [ 'text' => 'Learn More', 'link' => '#increaseProfitability', 'classes' => 'link mt-5', ] ]
    ]) @slot('cardText') <i class="fa fa-chart-line fa-fw" data-fa-transform="grow-10"></i> @endslot @endcomponent --}}

</div>

<div class="content__featureWall" id="consolidateShipments">

    <header class="featureWall__header">

        <span>Consolidate Shipments</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">Pangea&trade; also saves you money by providing full visibility of all your options, and helping you choose the proper
            carrier. The platform displays all applicable rates, all customer parcel carriers, LTL (less-than-truckload)
            carriers, and cartage carriers. Pangea&trade; creates additional savings by consolidating your shipments and
            reducing the time your team spends on checking rates and creating documents.</p>

    </div>

    <div class="featureWall__blockHalf">

        <div class="block__image" data-featherlight="{{ Brand::asset('images/%s/pangeaScreens/pangea-ui-sales-consolidation.jpg') }}">

            <img src="{{ Brand::asset('images/%s/pangeaScreens/pangea-ui-sales-consolidation.jpg') }}" alt="">

        </div>

    </div>

</div>

<div class="content__featureWall" id="timeSaving">

    <header class="featureWall__header">

        <span>Save Time</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">Pangea&trade; affords your front-line shipment processors the ease of a fully-featured software package for preprocessing,
            routing, labeling, and consolidating shipments. The application saves you time, seamlessly integrating with your
            scales, scanners, and printers, reduceing data entry, and eliminating the need to manually enter invoice details
            for a shipment.</p>

    </div>

    <div class="featureWall__blockHalf">

        <div class="block__image" data-featherlight="{{ Brand::asset('images/%s/pangeaScreens/pangea-ui-print-docs.jpg') }}">

            <img src="{{ Brand::asset('images/%s/pangeaScreens/pangea-ui-print-docs.jpg') }}" alt="">

        </div>

    </div>

</div>

{{--
<div class="content__featureWall" id="boostProductivity">

    <header class="featureWall__header">

        <span>Boost Productivity</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <ul class="generalList styled">
            <li>Shipment tracking information available electronically for all shipments from all carriers</li>
            <li>Tracking data can be integrated and sent electronically to client’s ERP or internal operating system</li>
        </ul>

    </div>

    <div class="featureWall__blockHalf">

        <div class="block__image" data-featherlight="{{ Brand::asset('images/%s/pangeaScreens/pangea-ui-sales-consolidation.jpg') }}">

            <img src="{{ Brand::asset('images/%s/pangeaScreens/pangea-ui-sales-consolidation.jpg') }}" alt="">

        </div>

    </div>

</div> --}}

<div class="content__featureWall" id="increaseProfitability">

    <header class="featureWall__header">

        <span>Increase Profitability</span>
        <span></span>

    </header>

    <div class="featureWall__blockHalf">

        <p class="text dark left paragraph general">Pangea&trade; provides a dramatic efficiency increase, giving you options to chose the most cost-effective method
            for your your shipments in record time.</p>

    </div>

    <div class="featureWall__blockHalf">

        <div class="block__image" data-featherlight="{{ Brand::asset('images/%s/pangeaScreens/pangea-ui-carriers.jpg') }}">

            <img src="{{ Brand::asset('images/%s/pangeaScreens/pangea-ui-carriers.jpg') }}" alt="">

        </div>

    </div>

</div>
@endsection