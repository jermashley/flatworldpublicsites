@extends('app.updated-layout')

@section('title', 'Terms and Conditions of Service | ')

@section('content')

	<h1 class="page__title my-1">Terms and Conditions</h1>

	<div class="markdown">
		<p>UNLESS AND TO THE EXTENT OTHERWISE SPECIFIED IN WRITING BETWEEN APPLICANT AND FLAT WORLD GLOBAL SOLUTIONS INC. OR ANY OF ITS SUBSIDIARIES (“SELLER” or “Flat World Global Solutions”), AS CONSIDERATION FOR THE USE OF FLAT WORLD GLOBAL SOLUTIONS’S SERVICES AND/OR THE ADVANCEMENT OF CREDIT, APPLICANT(S) INDIVIDUALLY, JOINTLY AND SEVERALLY (“Customer”) AGREES TO THE TERMS AND CONDITIONS SET FORTH BELOW. THESE TERMS AND CONDITIONS ARE EXPRESSLY INCORPORATED INTO THE CREDIT AGREEMENT SIGNED BY CUSTOMER, AS REFERENCED BY THE SITE LOCATION OF THESE TERMS AND CONDITIONS (www.flatworldsc.com).</p>

		<p style="color: rgba(224, 92, 105, 1);">Flat World Global Solutions’s  Terms & Conditions of Service constitute a legally binding contract between the “Company” and the “Customer”. Company, a licensed property broker operating as Flat World Global Solutions, LLC  under License No. MC2611294, undertakes to arrange for the interstate transportation of shipments on behalf of its customers (the "Customer") from various origins and destinations throughout the United States, Canada and Mexico. The transportation is furnished by carriers selected by Company.</p>

		<p>The Customer agrees to these TERMS AND CONDITIONS, which no agent or employee of the parties may change, alter or in any way transform. These TERMS AND CONDITIONS shall apply to all shipments by Customer. Flat World Global Solutions reserves the right to alter or amend these TERMS AND CONDITIONS. If not stated within the carrier's General Rules Tariff, the following TERMS AND CONDITIONS shall control. In the case of conflict between the TERMS AND CONDITIONS contained herein and those set forth by the individual selected carrier's General Rules Tariff, the selected carrier's General Rules Tariff shall control; however, under no circumstance shall the scope of Flat World Global Solutions’s liability be greater than specified in these TERMS AND CONDITIONS. All Terms, including, but not limited to, all the limitations of liability, shall apply to the selected carrier and their agents and contracted carriers.</p>

		<h3>Rates</h3>

		<p>Less Than a Truck Load (“LTL”) rates are based on the freight class as determined by the NMFC (National Motor Freight Classification), unless specifically negotiated, and are weight based. Truck Load (“TL”) rates are based on Dock Door Pickup/Dock Door Delivery and Shipper Load/Consignee Unload and are state to state and mileage based. Additional charges may apply for charges including but not limited to, Tractor Detention, Trailer Detention, and Driver Assistance, and Layover. Air Freight rates are based on the greater of actual or dimensional weight. If an Air Freight shipment contains oversize freight, additional charges and transit days may apply. Van Line rates are driven by state to state/mileage, weight (actual or density) and commodity/product type. Flatbed rates are based on equipment type, state to state/mileage and weight. If a flatbed shipment contains oversize freight, additional charges and transit days may apply. All displayed transit times are estimates only and do not include day of pickup. Pickup dates are not guaranteed.</p>

		<h3>Terms of Payment</h3>

		<p>
			All Customers are subject to credit approval. FLAT WORLD GLOBAL SOLUTIONS intends to perform a credit check based on the information provided at the time of enrollment by the Customer. The amount of credit, if any, granted to the Customer is at the sole discretion of FLAT WORLD GLOBAL SOLUTIONS. Subject to approval of Customer’s credit, net payment shall be due 30 days from invoice date unless otherwise noted in writing. Past-due invoices are subject to a service charge, calculated on the outstanding balance, at the lesser of (i) the rate of one and one-half percent (1 1/2)% per month or (ii) the highest legal rate authorized by applicable law. The service charge is not intended as an alternative to payment when due, and upon delinquency further purchases may be declined and the Customer’s account may be referred for collection. Customer agrees to pay all costs including reasonable collection costs, attorney’s fees and expenses related to the enforcement of applicant’s obligations hereunder. The Customer is liable for all charges payable on account of such Customer's shipment, including but not limited to transportation, fuel and other applicable accessorial charges, including all adjustments issued by the carrier(s) after the shipment, and all duties, customs assessments, governmental penalties and fines, taxes, and FLAT WORLD GLOBAL SOLUTIONS's attorney fees and legal costs allocable to this shipment and/or all disputes related thereto. Customer agrees to pay any convenience fees charged by Flat World Global Solutions, Inc. related to the payment of services via credit card or other electronic payment methods. Unless otherwise agreed, Brokers scheduling shipments for clients shall be liable, jointly and severally, for all charges payable on account of such client's shipment. FLAT WORLD GLOBAL SOLUTIONS shall have a lien on the shipment for all sums due it relating to this shipment or any other amounts owed by Customer. FLAT WORLD GLOBAL SOLUTIONS reserves the right to amend or adjust the original quoted amount or re-invoice the Customer if the original quoted amount was based upon incorrect information provided at the time of the original quote or if additional services by the carrier were required or otherwise authorized by the Customer to perform the pickup, transportation and delivery functions therein. When paying by credit card or electronic funds in advance of the shipment (“Pre-Pay”), the Customer agrees it will be responsible for all charges payable, including any adjustments, on account of such Customer's shipment. These charges and adjustments, if any, may be automatically debited from the Customer's credit card or bank account. Customer is permitted thirty (30) business days from the date of the invoice to dispute any invoiced charges. If FLAT WORLD GLOBAL SOLUTIONS does not receive a written dispute within the allowable thirty (30) business days, the disputed item will be denied by FLAT WORLD GLOBAL SOLUTIONS. This information can be provided to you prior to booking any shipment, or anytime after the shipment. FLAT WORLD GLOBAL SOLUTIONS reserves the right, at its sole discretion, to refuse any shipment at any time.
		</p>

		<h3>Warranties</h3>

		<p>
			The Customer is responsible for and warrants their compliance with all applicable laws, rules, and regulations including but not limited to customs laws, import and export laws and governmental regulation of any country to, from, through or over which the shipment may be carried. The Customer agrees to furnish such information and complete and attach to the Bill of Lading such documents as are necessary to comply with such laws, rules and regulations. FLAT WORLD GLOBAL SOLUTIONS assumes no liability to the Customer or to any other person for any loss or expense due to the failure of the Customer to comply with this provision. Any individual or entity acting on behalf of the Customer in scheduling shipments hereunder warrants that it has the right toact on behalf of the Customer and the right to legally bind Customer.
		</p>

		<h3>Bills of Lading</h3>

		<p>
			All Bills of Lading are NON-NEGOTIABLE and have been prepared by the enrolled Customer or by FLAT WORLD GLOBAL SOLUTIONS on behalf of the Customer and shall be deemed, conclusively, to have been prepared by the Customer. Any unauthorized alteration or use of Bills of Lading or tendering of shipments to any carrier other than that designated by FLAT WORLD GLOBAL SOLUTIONS, or the use of any Bill of Lading not authorized or issued by FLAT WORLD GLOBAL SOLUTIONS can VOID FLAT WORLD GLOBAL SOLUTIONS's obligations to make any payments relating to this shipment and VOID all rate quotes. If the Customer does not complete all the documents required for carriage, or if the documents which they submit are not appropriate for the services, pick up or destination requested, the Customer hereby instructs FLAT WORLD GLOBAL SOLUTIONS, where permitted by law, to complete, correct or replace the documents for them at the expense of the Customer. However, FLAT WORLD GLOBAL SOLUTIONS is not obligated to do so. If a substitute form of Bill of Lading is needed to complete delivery of this shipment and FLAT WORLD GLOBAL SOLUTIONS completes that document, the terms of this Bill of Lading will govern. FLAT WORLD GLOBAL SOLUTIONS is not liable to the Customer or to any other person for any actions taken on behalf of the Customer under this provision.
		</p>

		<h3>Limitations of Liability and Claims</h3>

		<p>
			FLAT WORLD GLOBAL SOLUTIONS has no responsibility, liability or involvement in the issuance of insurance, the denial of insurance, or in the payment of claims. Flat World Global Solutions does not carry insurance for customers. Any insurance purchased is purchased directly through the trucking company used. The individual carrier's governing General Rules Tariff determines the standard liability cargo insurance coverage offered by all carriers. If the shipment contains freight with a predetermined exception value, as determined by the selected carrier, the maximum exception liability will override the otherwise standard liability coverage. Insurance information will be provided to the customer upon request.
		</p>

		<p>
			FLAT WORLD GLOBAL SOLUTIONS will attempt to assist in the resolution of freight claims, but has no responsibility or liability related to any claim. All freight cargo claims should be submitted immediately to FLAT WORLD GLOBAL SOLUTIONS to help ensure timely resolution. If the loss or damage is apparent, the consignee must note such loss or damage information on the bill of lading/delivery receipt. If the loss or damage is not apparent (concealed), the Customer must contact Flat World Global Solutions within 3 days after taking delivery. The filing of a claim does not relieve the responsible party for payment of freight charges. Freight payment is necessary in order for a carrier to process a claim.
		</p>

		<p>
			FLAT WORLD GLOBAL SOLUTIONS is not liable for any loss, late-delivery, non-delivery, or consequential damages caused by the act, default or omission of the carrier, Customer or any other party who claims interest in the shipment, or caused by the nature of the shipment or any defect thereof. FLAT WORLD GLOBAL SOLUTIONS is not liable for losses, late-delivery or non-delivery caused by violation(s) by the Customer of any of the TERMS AND CONDITIONS contained in the Bill of Lading or of the carrier's General Rules Tariff including, but not limited to, improper or insufficient packing, securing, marking or addressing, or of failure to observe any of the rules relating to shipments not acceptable for transportation or shipments acceptable only under certain conditions. FLAT WORLD GLOBAL SOLUTIONS is not liable for losses, late delivery or non-delivery caused by the acts of God, perils of the air, public enemies, public authorities, acts or omissions of Customs or quarantine officials, war, riots, strikes, labor disputes, weather conditions or mechanical delay or failure of aircraft or other equipment. FLAT WORLD GLOBAL SOLUTIONS is not liable for failure to comply with delivery or other instructions from the Customer or for the acts or omissions of any person other than employees of FLAT WORLD GLOBAL SOLUTIONS.
		</p>

		<p>
			Subject to the limitations of liability contained in the Bill of Lading and the carrier's General Rules Tariff, FLAT WORLD GLOBAL SOLUTIONS shall only be liable for loss, damage, mis-delivery or non-delivery caused by FLAT WORLD GLOBAL SOLUTIONS's own gross negligence. FLAT WORLD GLOBAL SOLUTIONS's liability therefore shall be limited to the fees that FLAT WORLD GLOBAL SOLUTIONS has earned with respect to the subject shipment.
		</p>

		<p>
			FLAT WORLD GLOBAL SOLUTIONS MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE, WITH REGARD TO DELIVERIES OR WITH REGARD TO THIS WEBSITE, INFORMATION PROVIDED ON THIS WEBSITE OR SERVICES RELATED TO TRANSACTIONS CONDUCTED ON THIS WEBSITE. FLAT WORLD GLOBAL SOLUTIONS CANNOT GUARANTEE DELIVERY BY ANY SPECIFIC TIME OR DATE. IN ANY EVENT, FLAT WORLD GLOBAL SOLUTIONS SHALL NOT BE LIABLE FOR ANY SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES, INCLUDING BUT NOT LIMITED TO LOSS OF PROFITS OR INCOME, WHETHER OR NOT FLAT WORLD GLOBAL SOLUTIONS HAD KNOWLEDGE THAT SUCH DAMAGES MIGHT BE INCURRED.
		</p>

		<p>
			Flat World Global Solutions is a transportation management company. Flat World Global Solutions is not a freight carrier as it does not transport cargo. These TERMS AND CONDITIONS, however, shall not serve to affect or limit the liability of the freight carrier performing the transportation services for the Customer. Instead, the terms and conditions of the freight carrier shall control the rights and responsibilities between the Customer and the Freight Carrier. If you have any questions regarding carrier insurance or carrier liability, please contact FLAT WORLD GLOBAL SOLUTIONS for more details.
		</p>

		<h3>Forum Selection and Choice of Law</h3>

		<p>
			Any claim, dispute or litigation relating to these Terms and Conditions, any shipment scheduled or tendered hereunder or through FLAT WORLD GLOBAL SOLUTIONS's website, or relating to any and all disputes between FLAT WORLD GLOBAL SOLUTIONS and the enrolled Customer, Shipper and/or Consignee and/or Brokers for any enrolled Customer, Shipper and/or Consignee, shall be filed in the District Court of St. Charles, MO or in the United States District Court of St. Louis and shall be subject to Missouri law.
		</p>

		<p>
			Changes to Terms & Conditions – Customer agrees to be bound by all of the terms, conditions contained in this application. Flat World Global Solutions may modify the terms and conditions of this application from time to time, upon mailing notice of such change to Customer at the address shown on Flat World Global Solutions’s records or by posting the most up to date terms and conditions on www.flatworldsc.com. Such changes shall be effective for all transactions between Flat World Global Solutions and customer after the date of the notice / posting.
		</p>

		<h3>Website Access</h3>

		<p>
			Customer agrees that all user I.D’s, passwords, and information viewed on the web site shall be kept in strict confidence by all persons receiving access, and Customer warrants that no person shall in any way attempt to view information other than that permitted by the limited access granted, or attempt to modify any aspect of the web site. Customer also agrees that it shall not knowingly populate the web site with data that is inaccurate, or in any way corrupted so as to cause damage to the web site or any of the other data situated on the web site. Customer further agrees to indemnify and hold Flat World Global Solutions harmless from any and all damages, costs, actions, causes of action, regardless of nature, including but not limited to court costs and attorney’s fees, which may arise from, out of or in connection with any act or omission of any person (whether or not an employee of agent of Customer) who gains access to, alters, or adds any data or information on the web site as a direct or indirect result of the access granted by Flat World Global Solutions. Customer acknowledges that Flat World Global Solutions reserves the right to terminate any and all access to the web site granted to any person pursuant to this or any other application, which termination of access may occur at any time, with or without notice, and for any reason or for no reason, in Flat World Global Solutions’s unfettered discretion.
		</p>

		<p>
			APPLICANT’S SIGNATURE ON THE FLAT WORLD GLOBAL SOLUTIONS CREDIT APPLICATION ATTESTS SOLVENCY, ABILITY AND WILLINGNESS TO PAY OUR INVOICES IN ACCORDANCE WITH THE TERMS ESTABLISHED. APPLICANT’S REPRESENTATIVE BY AGREEING TO THESE TERMS AND CONDITIONS BY SUBMITTING SHIPMENTS ON FLATWORLDSC.COM, REQUESTING SERVICES THROUGH FLATWORLD CUSTOMER SERVICE DEPARTMENT, OR BY SIGNING FLATWORLD’S CREDIT APPLICATION REPRESENTS AND WARRANTS THAT SHE/HE HAS BEEN DULY AUTHORIZED TO MAKE THE STATEMENTS CONTAINED HEREIN AND TO BIND THE APPLICANT TO THE TERMS AND CONDITIONS SET FORTH HEREIN AND FURTHER REPRESENTS AND WARRANTS THAT THE INFORMATION SET FORTH ABOVE (INCLUDING, WITHOUT LIMITATION, ANY ADDITIONAL SHEETS ATTACHED HERETO) AND IN THE FINANCIAL STATEMENTS DELIVERED IN CONNECTION HEREWITH ARE TRUE, CORRECT AND COMPLETE.
		</p>
	</div>

@endsection
