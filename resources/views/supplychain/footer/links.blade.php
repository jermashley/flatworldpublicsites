@php
$links = [
	[
		'text' => 'Tracking',
		'destination' => 'https://pipeline.flatworldsc.com/app.php?r=tracking',
		'target' => '_blank',
	],
	[
		'text' => 'Blog',
		'destination' => route('blog.index')
	],
	[
		'text' => 'Careers',
		'destination' => route('careers.index')
	],
	[
		'text' => 'Terms & Conditions',
		'destination' => route(config('app.prefix').'.terms')
	],
]
@endphp

@unless (empty($links))
	@foreach ($links as $link)
	<a href="{{ $link['destination'] }}" @unless(empty($link['target'])) target="{{ $link['target'] }}" @endunless class="footer-link">
		{{ $link['text'] }}
		@unless (empty($link['icon']))
		<fw-font-awesome-icon icon="{{ $link['icon'] }}"></fw-font-awesome-icon>
		@endunless
	</a>
	@endforeach
@endunless
