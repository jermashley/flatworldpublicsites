@php
$solutionLinks = [
    [
        'title' => 'Less-than-truckload (LTL)',
        'route' => route(config('app.prefix') . '.solutions.ltl')
    ],
    [
        'title' => 'Truckload (TL)',
        'route' => route(config('app.prefix') . '.solutions.tl')
    ],
    [
        'title' => 'Expedited',
        'route' => route(config('app.prefix') . '.solutions.expedited')
    ],
    [
        'title' => 'Hospitality Services',
        'route' => route(config('app.prefix') . '.solutions.hospitality')
    ],
    [
        'title' => 'International Services',
        'route' => route(config('app.prefix') . '.solutions.international')
    ],
    [
        'title' => 'Return Goods Management',
        'route' => route(config('app.prefix') . '.solutions.returns')
    ],
];

$aboutLinks = [
    [
        'title' => 'Our Company',
        'route' => route(config('app.prefix') . '.about')
    ],
    [
        'title' => 'Careers',
        'route' => route('careers.index')
    ],
];

if (Route::is('home')) {
    $theme = 'light';
} else {
    $theme = 'dark';
}
@endphp

<fw-nav-link title="Solutions" theme="{{ $theme }}" dropdown>
    @foreach ($solutionLinks as $link)
        <a href={{ $link['route'] }}>{{ $link['title'] }}</a>
    @endforeach
</fw-nav-link>

<fw-nav-link title="Technology" route="{{ route(config('app.prefix') . '.technology') }}" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="about" theme="{{ $theme }}" dropdown>
    @foreach ($aboutLinks as $link)
        <a href={{ $link['route'] }}>{{ $link['title'] }}</a>
    @endforeach
</fw-nav-link>

<fw-nav-link title="Blog" route="{{ route('blog.index') }}" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="Tracking" route="https://pipeline.flatworldsc.com/app.php?r=tracking" target="_blank" theme="{{ $theme }}"></fw-nav-link>

<fw-nav-link title="Pipeline Login" route="https://pipeline.flatworldsc.com/login.php" target="_blank" theme="{{ $theme }}"></fw-nav-link>
