<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>You Need To Log In</title>
</head>

<body style="width: 100%; min-height: 100vh; margin: 0; padding: 0; display: flex; flex-direction: column; justify-content: center; align-items: center; font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto, Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';">

    <h1 style="margin: 0 8px 64px; font-weight: 400;"><a href="{{ route('login') }}">Go to Login &gt;</a></h1>

    <img src="https://media.giphy.com/media/12qGrlnKHjEInK/200w.gif" alt="" style="width: 100%; max-width: 600px; height: auto;">

</body>
</html>