@extends('app.updated-layout')

@section('title', 'Change Password | ')

@section('content')

	<h1 class="page__title my-1">Change Password</h1>

	<div style="margin: 24px 0 0;">
		<form action="{{ route('changePassword.changePassword') }}" method="POST" style="max-width: 360px; margin: 0 auto;">
			{{-- @method('PATCH') --}}
			@csrf

			<div class="form-group">
				<label for="current_password" @if ($errors->has('current_password')) class="error" @endif>
					<span class="label-text">Current Password</span>
					<input type="password" name="current_password" id="current_password" required>
				</label>
			</div>

			<div class="form-group">
				<label for="new_password" @if ($errors->has('new_password')) class="error" @endif>
					<span class="label-text">New Password</span>
					<input type="password" name="new_password" id="new_password" required>
				</label>
			</div>

			<div class="form-group">
				<label for="new_password_confirmation" @if ($errors->has('new_password_confirmation')) class="error" @endif>
					<span class="label-text">Confirm New Password</span>
					<input type="password" name="new_password_confirmation" id="new_password_confirmation" required>
				</label>
			</div>

			<div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
				<fw-link href="{{ route('userProfile.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
						Cancel
				</fw-link>

				<fw-button design="solid" state="info" size="small" icon="save" type="submit" role="button">
					Save
				</fw-button>
			</div>
		</form>
	</div>

@endsection