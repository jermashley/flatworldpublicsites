@extends('app.updated-layout')

@section('title', 'Reset Password | ')

@section('content')

    <h1 class="page__title mb-5">Reset Password</h1>

    <form method="POST" action="{{ route('password.request') }}" style="max-width: 360px; margin: 0 auto;">
        @csrf

        <input type="hidden" name="token" value="{{ $token }}">

        <div class="form-group">
            <label for="email" @if ($errors->has('email')) class="error" @endif>
                <span class="label-text">E-mail</span>
                <input type="email" name="email" id="firstName" value="{{ old('email') }}" placeholder="john.doe@email.com" autofocus required>
            </label>
        </div>

        <div class="form-group">
            <label for="password" @if ($errors->has('password')) class="error" @endif>
                <span class="label-text">Password</span>
                <input type="password" name="password" id="password" placeholder="******" required>
            </label>

            <label for="password_confirmation" @if ($errors->has('password_confirmation')) class="error" @endif>
                <span class="label-text">Confirm Password</span>
                <input type="password" name="password_confirmation" id="password_confirmation" placeholder="******" required>
            </label>
        </div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-button design="solid" state="info" size="small" icon="key" type="submit" role="button"></fw-button>
        </div>

    </form>
@endsection
