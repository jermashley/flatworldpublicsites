@extends('app.updated-layout')

@section('title', 'Forgot Password | ')

@section('content')

    @if (Session::has('status'))
    @component ('components.alerts.banner', [
        'state' => 'info',
        'message' => Session::get('status'),
    ])
    @endcomponent
    @endif

    <h1 class="page__title mb-5">Forgot Password</h1>

    <form method="POST" action="{{ route('password.email') }}" style="max-width: 360px; margin: 0 auto;">
        @csrf

        <div class="form-group">
            <label for="email" @if ($errors->has('email')) class="error" @endif>
                <div class="label-text">E-mail</div>
                <input type="email" name="email" id="email" value="{{ old('email') }}" placeholder="john.doe@email.com" autofocus required>
            </label>
        </div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-button design="solid" state="info" size="small" icon="envelope" type="submit" role="button">
                Send Reset Link
            </fw-button>
        </div>
    </form>
@endsection
