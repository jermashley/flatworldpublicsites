@extends('app.updated-layout')

@section('title', 'Log In | ')

@section('content')

    <h1 class="page__title mb-5">Log In</h1>

    <form method="POST" action="{{ route('login') }}" style="max-width: 360px; margin: 0 auto;">
        @csrf

        <div class="form-group">
            <label for="email" @if ($errors->has('email')) class="error" @endif>
                <span class="label-text">E-mail</span>
                <input type="email" name="email" id="email" value="{{ old('email') }}" placeholder="john.doe@email.com" autofocus required>
            </label>
        </div>

        <div class="form-group">
            <label for="password" @if ($errors->has('password')) class="error" @endif>
                <span class="label-text">Password</span>
                <input type="password" name="password" id="password" placeholder="******" required>
            </label>
        </div>

        <div class="form-group">
            <label class="checkbox" for="remember" style="width: fit-content;">
                <input type="checkbox" name="remember" id="remember">
                <span class="label-text">Remember Me</span>
            </label>
        </div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-link href="{{ route('password.request') }}" design="text" state="dark" size="small" icon="lock" style="margin: 0 16px 0 0;">
                Forgot Password
            </fw-link>

            <fw-button design="solid" state="info" size="small" icon="sign-in" type="submit" role="button">
                Log In
            </fw-button>
        </div>
    </form>
@endsection
