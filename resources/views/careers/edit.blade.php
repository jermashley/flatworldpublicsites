@extends('app.updated-layout')

@section('title', '[New] Career Listing | ')

@section('content')

    @if ($errors->any())
    <div class="message-flash critical">
        <span class="close" data-toggle="hide-parent">
            <fw-font-awesome-icon icon="times"></fw-font-awesome-icon>
            {{-- <i class="far fa-times fa-fw"></i> --}}
        </span>
        <div>
            @foreach ($errors->all() as $error)
            <span class="response__heading" style="display: block; text-align: left;">{{ $error }}</span>
            @endforeach
        </div>
    </div>
    @endif

    <h1 class="blog__title">Edit {{ $career->title }}</h1>

    <form action="{{ route('careers.update', ['career' => $career->id]) }}" method="POST" enctype="multipart/form-data">
        @method('PATCH')
        @csrf

        <div class="form-group">
            <label for="title">
                <span class="label-text">Career Title</span>
                <input type="text" name="title" id="title" value="{{ $career->title }}" placeholder="Post Title" required>
            </label>
        </div>

        <div class="form-group">
            <label for="short_description">
                <span class="label-text label-text--textarea">Short Description</span>
                <textarea name="short_description" id="short_description" rows="3" placeholder="Drop in a short description about this post." required>{{ $career->short_description }}</textarea>
            </label>
        </div>

        <div class="form-group">
            <label for="long_description">
                <span class="label-text label-text--textarea">Long Description</span>
                <textarea name="long_description" id="long_description" rows="10" placeholder="Please be as descriptive as possible." required>{{ $career->long_description }}</textarea>
            </label>
        </div>

        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">What company does this need to post on?</h2>

        <div class="form-group">
            <fieldset @if ($errors->has('company')) class="error" @endif>
                @foreach($companies as $company)
                <label class="radio" for="{{ $company->shortname }}">
                    @if($company->id == $career->company_id)
                    <input type="radio" name="{{ $company->shortname }}" id="{{ $company->shortname }}" value="{{ $company->id }}" checked required>
                    @else
                    <input type="radio" name="{{ $company->shortname }}" id="{{ $company->shortname }}" value="{{ $company->id }}" required>
                    @endif
                    <span class="label-text">{{ $company->name }}</span>
                </label>
                @endforeach
            </fieldset>
        </div>

        <h2 class="mt-5 mb-1" style="padding: 0 0 0 12px;">Do you want this to post now?</h2>

        <div class="form-group">
            <fieldset @if ($errors->has('published')) class="error" @endif>
                <label class="inputGroup radio" for="published">
                    @if ($career->published === 1)
                    <input type="radio" name="published" id="published" value="1" checked required>
                    @else
                    <input type="radio" name="published" id="published" value="1" required>
                    @endif
                    <span class="label-text">Published</span>
                </label>

                <label class="inputGroup radio" for="draft">
                    @if ($career->published === 0)
                    <input type="radio" name="published" id="draft" value="0" checked required>
                    @else
                    <input type="radio" name="published" id="draft" value="0" required>
                    @endif
                    <span class="label-text">Draft</span>
                </label>
            </fieldset>
        </div>

        <div style="display: flex; flex-flow: row nowrap; justify-content: flex-end; margin: 12px 0 0; padding: 0 12px;">
            <fw-link href="{{ route('careers.index') }}" design="text" state="critical" size="small" icon="times" style="margin: 0 16px 0 0;">
                Cancel
            </fw-link>

            <fw-button design="solid" state="info" size="small" icon="paper-plane" type="submit" role="button">
                Update Career Listing
            </fw-button>
        </div>

    </form>

@endsection