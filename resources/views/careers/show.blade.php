@extends('app.updated-layout')

@section('title', $career->title.' | Careers | ')

@section('meta')
<meta property="og:title" content="{{ $career->title }}">
<meta property="og:description" content="{{ $career->short_description }}">
<meta property="og:site_name" content="{{ config('app.name') }}">
@endsection

@section('content')
<div class="blog__content" style="margin: 3rem 0 0;">
  <h1 class="blog__title">{{ $career->title }}</h1>

  <h3 class="blog__summary">{{ $career->short_description }}</h3>

  <div class="blog__meta">
      Posted on
      <span>{{ date('F d, Y', strtotime($career->created_at)) }}</span>
  </div>

  <div class="addthis_inline_share_toolbox" style="margin: 0 0 0 -7px;"></div>

  <hr
      style="
          border-color: rgba(0,0,0,.1);
          border-width: 1px;
          border-bottom: none;
      "
  >

  <div class="blog__body markdown">
      @markdown($career->long_description)
  </div>
</div>
@endsection

@section('footer-scripts')
<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c4a2f08a740f59b"></script>
@endsection
