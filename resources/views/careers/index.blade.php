@extends('app.updated-layout')
@section('title', 'Careers | ')
@section('content')

<h1 class="page__title my-1">Careers</h1>

<h3 class="page__subTitle my-1">We're looking for enthusiastic, motivated people to join our team at one of St. Louis’ fastest growing companies.</h3>

<div class="d-flex flex-column">
    <div class="splashImage" id="pipeline">
        <h3>Excellent people are at the heart of what we can accomplish</h3>
        <h3 class="em">enthusiastic, motivated people</h3>

        <img src="{{ asset('images/happy-business-meeting.jpg') }}" class="imageBg cover top" alt="">
    </div>
</div>

<div style="width: 100%; display: flex; flex-flow: row nowrap; justify-content: center; align-items: center; margin: 0 auto; padding: 0 0 48px;">
    <a href="/blog/116">
        <img src="{{ asset('images/topWorkplace2019.jpg') }}" alt="" style="max-width: 200px; width: 100%;">
    </a>

    <div style="padding: 12px 16px;">
        <a href="/blog/116" class="link" style="font-size: 16px; font-weight: 600; color: rgba(0,0,0,.65);">Learn about how we were ranked in the top 150 companies to work for in Saint Louis.</a>
    </div>
</div>

<ul class="generalList styled">
    <li>Industry competitive salaries</li>
    <li>New business incentives for sales positions</li>
    <li>Generous 401K matching</li>
    <li>100% paid premiums on health insurance</li>
    <li>Profit Sharing</li>
</ul>

<p class="text dark left paragraph general">
    Fast growing means opportunity, recognition and advancement. We're building on our culture of excellence and we're driven to exceed our customer's expectations as well as our own. Our customers aren't just clients, they're our partners.
</p>

<div class="d-flex flex-row flex-wrap justify-content-around mt-5">
    @if ($careers->isEmpty())
    <h1 style="text-align: center; color: hsla(205, 10%, 35%, 1);">Sorry! We don't currently have any positions available.</h1>

    @else

        @foreach($careers as $career)
        <fw-card-blog title="{{ $career->title }}" summary="{{ $career->short_description }}" company="{{ $career->company->name }}"
            created-at="{{ $career->created_at }}" href="{{ route('careers.show', ['career' => $career->id]) }}">
        </fw-card-blog>
        @endforeach
    @endif
</div>
@endsection
