<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Flat World Protected Site</title>

    <link rel="stylesheet" href="https://indestructibletype.com/fonts/Jost.css" type="text/css" charset="utf-8" />

    <style>
        /* http://meyerweb.com/eric/tools/css/reset/
        v2.0 | 20110126
        License: none (public domain)
        */

        html,
        body,
        div,
        span,
        applet,
        object,
        iframe,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p,
        blockquote,
        pre,
        a,
        abbr,
        acronym,
        address,
        big,
        cite,
        code,
        del,
        dfn,
        em,
        img,
        ins,
        kbd,
        q,
        s,
        samp,
        small,
        strike,
        strong,
        sub,
        sup,
        tt,
        var,
        b,
        u,
        i,
        center,
        dl,
        dt,
        dd,
        ol,
        ul,
        li,
        fieldset,
        form,
        label,
        legend,
        table,
        caption,
        tbody,
        tfoot,
        thead,
        tr,
        th,
        td,
        article,
        aside,
        canvas,
        details,
        embed,
        figure,
        figcaption,
        footer,
        header,
        hgroup,
        menu,
        nav,
        output,
        ruby,
        section,
        summary,
        time,
        mark,
        audio,
        video {
            margin: 0;
            padding: 0;
            border: 0;
            font-size: 100%;
            font: inherit;
            vertical-align: baseline;
        }

        /* HTML5 display-role reset for older browsers */
        article,
        aside,
        details,
        figcaption,
        figure,
        footer,
        header,
        hgroup,
        menu,
        nav,
        section {
            display: block;
        }

        body {
            line-height: 1;
        }

        ol,
        ul {
            list-style: none;
        }

        blockquote,
        q {
            quotes: none;
        }

        blockquote:before,
        blockquote:after,
        q:before,
        q:after {
            content: '';
            content: none;
        }

        table {
            border-collapse: collapse;
            border-spacing: 0;
        }

        :root {
            font-family: Jost, sans-serif;
            font-size: 16px;
            color: hsla(215, 10%, 30%, 1);
        }

        body {
            width: 100%;
            height: 100%;
            min-height: 100vh;
        }

        main {
            display: flex;
            flex-flow: column nowrap;
            justify-content: center;
            align-items: center;

            width: 100%;
            height: 100%;
            min-height: 100vh;
        }

        section {
            width: 100%;

            padding: 0 1.5rem;
        }

        h1 {
            font-size: 3rem;
            font-weight: 600;

            margin: 0 0 1.25rem;
        }

        h4 {
            font-size: 1.25rem;
            font-weight: 400;
        }

        h1, h4 {
            text-align: center;
        }

        .heading-container {
            margin: 0 0 2rem;
        }

        .text-danger {
            font-size: .75rem;
            font-weight: 600;

            color: hsla(355, 75%, 60%, 1);
        }

        form {
            width: 100%;
            max-width: 512px;

            margin: 0 auto;
        }

        .form-group {
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: center;
        }

        input[type=password] {
            flex-grow: 1;

            width: 100%;

            font-size: 1rem;

            margin: 0 0 .25rem;
            padding: 0.75rem 1rem;

            border: 1px solid hsla(215, 3%, 80%, 1);
            border-radius: 0.25rem 0 0 0.25rem;

            outline: none;
        }

        input::placeholder {
            color: hsla(215, 3%, 70%, 1);
            font-weight: 300;
        }

        button[type=submit] {
            width: auto;

            white-space: nowrap;

            color: hsla(215, 3%, 40%, 1);
            font-size: 1rem;

            margin: 0 0 .25rem;
            padding: 0.75rem 1rem;

            border: 1px solid hsla(215, 3%, 80%, 1);
            border-left: none;
            border-radius: 0 0.25rem 0.25rem 0;

            outline: none;
        }
    </style>
</head>

<body>
    <main>
        <section class="heading-container">
            <h1>Flat World Site</h1>
            <h4>Please, enter the password to continue</h4>
        </section>

        <section class="form-container">
            <form method="GET">
                {{ csrf_field() }}

                <div class="form-group">
                    <input type="password" name="site-password-protected" placeholder="Please enter password"
                        class="form-control" tabindex="1" autofocus />
                        <button type="submit">Enter site</button>

                </div>
                @if (Request::get('site-password-protected'))
                <div class="text-danger">Password is wrong</div>
                @endif

            </form>
        </section>
    </main>
</body>

</html>
