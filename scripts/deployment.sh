#!/bin/sh
BRANCH=$1

php artisan down

git checkout .
git fetch
git checkout $BRANCH
git pull origin $BRANCH

composer install --optimize-autoloader --no-dev

php artisan config:clear
php artisan cache:clear
php artisan route:cache
php artisan migrate

rm -rf node_modules
npm install
cd node_modules/vue-markdown
npm install
cd ../..
npm run prod

php artisan up
