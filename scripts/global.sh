#!/bin/sh
ENVIRONMENT=$1
BRANCH=$2

STAGING_SITES=(
  staging.flatworldsc.com
  staging.flatworldhospitality.com
  staging.ram-intl.com
  staging.ramcrating.com
)

PRODUCTION_SITES=(
  flatworldsc.com
  flatworldhospitality.com
  ram-intl.com
  ramcrating.com
)

if [ $ENVIRONMENT="staging" ]; then
  for SITE in ${!STAGING_SITES[@]}
  do
    cd /var/www/html/${STAGING_SITES[$SITE]}
    ./scripts/deployment.sh $BRANCH
  done
elif [ $ENVIRONMENT="production" ]; then
  for SITE in ${!PRODUCTION_SITES[@]}
  do
    cd /var/www/html/${PRODUCTION_SITES[$SITE]}
    ./scripts/deployment.sh $BRANCH
  done
else
  for SITE in ${!STAGING_SITES[@]}
  do
    cd /var/www/html/${STAGING_SITES[$SITE]}
    ./scripts/deployment.sh development
  done

  for SITE in ${!PRODUCTION_SITES[@]}
  do
    cd /var/www/html/${PRODUCTION_SITES[$SITE]}
    ./scripts/deployment.sh master
  done
fi
