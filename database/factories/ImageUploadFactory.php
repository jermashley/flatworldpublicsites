<?php

use Faker\Generator as Faker;

$factory->define(\App\ImageUpload::class, function (Faker $faker) {

    return [
      'url' => 'https://picsum.photos/640/480?' . rand(0, 2500),
      'name' => $faker->word,
      'description' => $faker->sentence($nbWords = 12, $variableNbWords = true)
    ];
});
