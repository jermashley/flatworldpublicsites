<?php

use Faker\Generator as Faker;

$factory->define(\App\Page::class, function (Faker $faker) {

    $time_start = $faker->dateTime();
    $time_end = $faker->dateTimeBetween($time_start, '5 days');

    return [
        'company_shortname' => \App\Company::all(['shortname'])->random()->shortname,
        'published' => $faker->boolean($chanceOfGettingTrue = 75),
        'title' => $faker->realText($maxNbChars = 50),
        'summary' => $faker->realText($maxNbChars = 200),
        'content' => $faker->realText($maxNbChars = 750),
        'featured_image' => $faker->imageUrl($width = 640, $height = 480),
    ];
});
