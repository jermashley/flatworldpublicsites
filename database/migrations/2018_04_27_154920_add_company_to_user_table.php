<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCompanyToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('id');
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->nullable()->after('id');
            $table->foreign('company_id')->references('id')->on('companies');
        });

        Schema::table('blogs', function (Blueprint $table) {
            $table->dropForeign('blogs_added_by_foreign');
            $table->renameColumn('added_by', 'created_by');
            $table->foreign('created_by')->references('id')->on('users');

            $table->dropForeign('blogs_updated_last_by_foreign');
            $table->renameColumn('updated_last_by', 'updated_by');
            $table->foreign('updated_by')->references('id')->on('users');

            $table->unsignedInteger('company_id')->nullable()->after('company_shortname');
            $table->foreign('company_id')->references('id')->on('companies');

            $table->removeColumn('user_id');
        });

        Schema::table('careers', function (Blueprint $table) {
            $table->unsignedInteger('company_id')->change();

            $table->unsignedInteger('created_by')->nullable()->after('deleted_at');
            $table->foreign('created_by')->references('id')->on('users');

            $table->unsignedInteger('updated_by')->nullable()->after('created_by');
            $table->foreign('updated_by')->references('id')->on('users');
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->unsignedInteger('created_by')->nullable()->after('deleted_at');
            $table->foreign('created_by')->references('id')->on('users');

            $table->unsignedInteger('updated_by')->nullable()->after('created_by');
            $table->foreign('updated_by')->references('id')->on('users');

            $table->removeColumn('user_id');
        });


        $blogSql = <<<END_SQL
UPDATE blogs b
LEFT JOIN companies c ON b.company_shortname = c.shortname
SET b.company_id = c.id
END_SQL;

        \Illuminate\Support\Facades\DB::statement($blogSql);

        $pageSql = <<<END_SQL
UPDATE pages b
LEFT JOIN companies c ON b.company_shortname = c.shortname
SET b.company_id = c.id
END_SQL;

        \Illuminate\Support\Facades\DB::statement($pageSql);

        Schema::table('blogs', function (Blueprint $table) {
            $table->dropColumn('company_shortname');
        });

        Schema::table('pages', function (Blueprint $table) {
            $table->dropColumn('company_shortname');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
