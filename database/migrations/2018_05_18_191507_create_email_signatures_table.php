<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailSignaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('companies', function (Blueprint $table) {
            $table->string('address_1')->after('name')->nullable();
            $table->string('address_2')->after('address_1')->nullable();
            $table->string('city')->after('address_2')->nullable();
            $table->string('state')->after('city')->nullable();
            $table->string('zip')->after('state')->nullable();
            $table->string('phone')->after('zip')->nullable();
            $table->string('fax')->after('phone')->nullable();
            $table->string('website')->after('fax')->nullable();
        });

        // Schema::create('email_signatures', function (Blueprint $table) {
        //     $table->increments('id');
        //     $table->unsignedInteger('user_id');
        //     $table->foreign('user_id')->references('id')->on('users');
        //     $table->unsignedInteger('company_id');
        //     $table->foreign('company_id')->references('id')->on('companies');
        //     $table->string('address_1')->nullable();
        //     $table->string('address_2')->nullable();
        //     $table->string('city')->nullable();
        //     $table->string('state')->nullable();
        //     $table->string('zip')->nullable();
        //     $table->string('phone')->nullable();
        //     $table->unsignedInteger('has_phone')->default('0');
        //     $table->string('cell')->nullable();
        //     $table->unsignedInteger('has_cell')->default('0');
        //     $table->string('fax')->nullable();
        //     $table->unsignedInteger('has_fax')->default('0');
        //     $table->string('email')->nullable();
        //     $table->unsignedInteger('has_email')->default('0');
        //     $table->timestamps();
        //     $table->softDeletes();
        // });

        Schema::table('users', function (Blueprint $table) {
            $table->string('title')->after('last_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('email_signatures');
    }
}
