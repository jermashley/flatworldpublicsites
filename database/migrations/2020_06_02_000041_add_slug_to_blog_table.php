<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Str;

class AddSlugToBlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blogs', function (Blueprint $table) {
            $table->string('slug')->after('title')->unique()->nullable();
        });

        $rows = DB::table('blogs')->get(['id', 'title']);

        foreach ($rows as $row) {
            DB::table('blogs')
                ->where('id', $row->id)
                ->update(['slug' => Str::slug(Str::limit($row->title, 191), '-')]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blogs', function (Blueprint $table) {
            $table->dropColumn('slug');
        });
    }
}
