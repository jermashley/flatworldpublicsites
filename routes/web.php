<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Helpers\Brand;
use Illuminate\Http\Request;

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/login-failed', function () {
    return view('auth.fail');
});

Route::get('/', function () {
    return view(config('app.prefix') . '.home.index');
})->name('home');

// Password Reset Routes
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// User Management Routes
Route::resource('/user', 'UserManagementController');

// User Profile Routes
Route::get('/profile', 'UserProfileController@index')->name('userProfile.index');
Route::get('/profile/edit', 'UserProfileController@edit')->name('userProfile.edit');
Route::patch('/profile/update', 'UserProfileController@update')->name('userProfile.update');

Route::get('/profile/password', 'ChangePasswordController@index')->name('changePassword.index');
Route::post('/changePassword', 'ChangePasswordController@changePassword')->name('changePassword.changePassword');

// Contact
Route::resource('/contact', 'ContactController');
Route::post('/air2PostContact', 'ContactController@a2PStoreContact')->name('air2PostContact');
// Route::get('/contact', function () {
//     return view(config('app.prefix').'.contact.index');
// })->name('contact');

Route::post('/generalContact', 'GeneralContactController@sendEmail')->name('generalContact');

// Careers
Route::resource('/careers', 'CareerController');
Route::get('/careers/delete/{id}', 'CareerController@delete')->name('careers.delete');

// Blog Public
Route::get('/blog', 'BlogController@index')->name('blog.index');
Route::get('/blog/{blog}', 'BlogController@show')->name('blog.show');

// Pages
Route::get('/pages/{pages}/restore', 'PageController@restore')->name('pages.restore');
Route::resource('/pages', 'PageController');
Route::get('/pages/delete/{pages}', 'PageController@delete')->name('pages.delete');

// Image Uploads
Route::get('/api/images', 'ImageUploadController@index');
Route::post('/api/images/store', 'ImageUploadController@store');

// Services
Route::get('/services', function () {
    return view(config('app.prefix') . '.services.index');
})->name(config('app.prefix') . '.services');

// Software
Route::get('/software', function () {
    return view(config('app.prefix') . '.software.index');
})->name(config('app.prefix') . '.software');

// Services
Route::get('/process', function () {
    return view(config('app.prefix') . '.process.index');
})->name(config('app.prefix') . '.process');

// Technology
Route::get('/technology', function () {
    return view(config('app.prefix') . '.technology.index');
})->name(config('app.prefix') . '.technology');

// Process
Route::get('/process', function () {
    return view(config('app.prefix') . '.process.index');
})->name(config('app.prefix') . '.process');

// About
Route::get('/about', function () {
    return view(config('app.prefix') . '.about.index');
})->name(config('app.prefix') . '.about');

// Supply Chain

Route::get('/tracking', function () {
    return redirect('https://pipeline.flatworldsc.com/app.php?r=tracking');
});
Route::post('/getTracking', 'ShipmentTrackingController@getTracking')->name('getTracking');

// Solutions
Route::get('/solutions', function () {
    return view(config('app.prefix') . '.solutions.index');
})->name(config('app.prefix') . '.solutions');

Route::get('/solutions/ltl', function () {
    return view(config('app.prefix') . '.solutions.ltl');
})->name(config('app.prefix') . '.solutions.ltl');

Route::get('/solutions/tl', function () {
    return view(config('app.prefix') . '.solutions.tl');
})->name(config('app.prefix') . '.solutions.tl');

Route::get('/solutions/hospitality', function () {
    return view(config('app.prefix') . '.solutions.hospitality');
})->name(config('app.prefix') . '.solutions.hospitality');

Route::get('/solutions/expedited', function () {
    return view(config('app.prefix') . '.solutions.expedited');
})->name(config('app.prefix') . '.solutions.expedited');

Route::get('/solutions/international', function () {
    return view(config('app.prefix') . '.solutions.international');
})->name(config('app.prefix') . '.solutions.international');

Route::get('/solutions/returns', function () {
    return view(config('app.prefix') . '.solutions.returns');
})->name(config('app.prefix') . '.solutions.returns');


// Flat World Global Solutions
Route::get('/solutions/air2post', function () {
    return view(config('app.prefix') . '.solutions.air2post');
})->name(config('app.prefix') . '.solutions.air2post');

Route::get('/solutions/airFreight', function () {
    return view(config('app.prefix') . '.solutions.airFreight');
})->name(config('app.prefix') . '.solutions.airFreight');

Route::get('/solutions/oceanFreight', function () {
    return view(config('app.prefix') . '.solutions.oceanFreight');
})->name(config('app.prefix') . '.solutions.oceanFreight');

Route::get('/solutions/compliance', function () {
    return view(config('app.prefix') . '.solutions.compliance');
})->name(config('app.prefix') . '.solutions.compliance');

Route::get('/solutions/tempControlledCargo', function () {
    return view(config('app.prefix') . '.solutions.tempControlledCargo');
})->name(config('app.prefix') . '.solutions.tempControlledCargo');

Route::get('/solutions/specialtyCargo', function () {
    return view(config('app.prefix') . '.solutions.specialtyCargo');
})->name(config('app.prefix') . '.solutions.specialtyCargo');

Route::get('/solutions/warehousing', function () {
    return view(config('app.prefix') . '.solutions.warehousing');
})->name(config('app.prefix') . '.solutions.warehousing');

Route::get('/solutions/domesticTransportation', function () {
    return view(config('app.prefix') . '.solutions.domesticTransportation');
})->name(config('app.prefix') . '.solutions.domesticTransportation');

Route::get('/solutions/shipmentTracking', function () {
    return view(config('app.prefix') . '.solutions.shipmentTracking');
})->name(config('app.prefix') . '.solutions.shipmentTracking');

Route::get('/terms', function () {
    return view(config('app.prefix') . '.termsConditions.index');
})->name(config('app.prefix') . '.terms');

Route::get('/resources', function () {
    return view(config('app.prefix') . '.resources.index');
})->name(config('app.prefix') . '.resources');

// E-Commerce Routes
Route::get('/ecommerce', function () {
    return view(config('app.prefix') . '.solutions.ecommerce');
})->name(config('app.prefix') . '.ecommerce');

Route::get('/solutions/ecommerceQuote/lineItem', function () {
    return view(config('app.prefix') . '.solutions.ecommerceQuote.lineItem');
})->name(config('app.prefix') . '.ecommerce.lineItem');

Route::get('/solutions/ecommerceQuote/cartonCrate', function () {
    return view(config('app.prefix') . '.solutions.ecommerceQuote.cartonCrate');
})->name(config('app.prefix') . '.ecommerce.cartonCrate');

Route::get('/solutions/ecommerceQuote/cartonPallet', function () {
    return view(config('app.prefix') . '.solutions.ecommerceQuote.cartonPallet');
})->name(config('app.prefix') . '.ecommerce.cartonPallet');

Route::post('/eCommerceForm', 'ECommerceFormController@sendEmail')->name('eCommerceForm');

Route::get('/profile/signatures/download/{signature}', 'EmailSignatureController@renderHtml')->name('signatures.download');
Route::resource('/profile/signatures', 'EmailSignatureController');

Auth::routes();

Route::get('/playground', function () {
    return view('playground');
})->name('playground');

Route::get('/api/companies', 'CompanyController@index');

// Route::get('/', 'HomeController@index')->name('home');

// Temp routes
Route::get('/alert', function () {
    return view(config('app.prefix') . '.alerts.indiaAlert');
});

// Api
Route::namespace('Api')->name('api.')->prefix('api')->group(function () {
    // Blog Routes
    Route::get('blog/{blog}/restore', 'BlogApiController@restore')->name('blog.restore');
    Route::get('blog/{blog}/delete', 'BlogApiController@delete')->name('blog.delete');
    Route::resource('blog', 'BlogApiController');

    // Career Routes
    Route::resource('career', 'CareerController');

    // Tracking
    Route::post('tracking/{trackingNumber}', 'TrackingApiController@getTracking')->name('tracking');
});

Route::namespace('Admin')->name('admin.')->prefix('admin')->group(function () {
    // Blog Admin
    Route::resource('blog', 'BlogAdminController');

    // Career Admin
    Route::resource('career', 'CareerController');
});

// Temp route to test emails
Route::get('mailable', function (Request $request) {
    return new App\Mail\EcommerceResponderForm($request);
})->name('mailable');

// Temp routes
Route::get('/review-map', function () {
    return view('temp.review');
});
