<?php

return [
    'name' => env('APP_PREFIX', 'flatworldholdings'),
    'contacts' => env('APP_CONTACTS', 'info@flatworldholdings.com'),
];
