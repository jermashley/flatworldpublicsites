let mix = require("laravel-mix");

require("laravel-mix-polyfill");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .js("resources/assets/js/app.js", "public/js")
  .js("resources/assets/js/_eCommerceQuoteForm.js", "public/js")
  .sass("resources/assets/sass/app.scss", "public/css")
  .polyfill({
    enabled: true,
    useBuiltIns: "usage",
    targets: { firefox: "50", ie: 11 },
  })
  .version();

// mix.browserSync('http://localhost:8085/');

mix
  .copy("resources/assets/fonts/", "public/fonts", false)
  .copy("resources/assets/images/", "public/images", false)
  .copy("node_modules/clipboard/dist/clipboard.min.js", "public/js", false);
